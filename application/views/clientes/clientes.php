<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'aCliente')){
		echo '
			<a href="'.base_url().'index.php/clientes/adicionar" class="btn btn-success">
				<i class="icon-plus icon-white"></i> Agregar Cliente
			</a>
		';
		}
?>
<?php
	if(!$results){
?>
	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-user"></i></span>
				<h5>Clientes</h5>
		</div>
		<div class="widget-content nopadding">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th>Cedula/Rif</th>
						<th>Telefono</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5">Ningun Cliente Registrado</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

<?php
	}else{
?>
	<div class="widget-box">
		<div class="widget-title">
			<span class="icon">
					<i class="icon-user"></i>
			 </span>
			<h5>Clientes</h5>
		</div>

		<div class="widget-content nopadding">
			<table class="table table-bordered ">
				<thead>
					<tr>
						<th>#</th>
							<th>Nombre</th>
							<th>Cedula / Rif</th>
							<th>Telefono</th>
							<th>Celular</th>
							<th>Email</th>
							<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($results as $r) {
							echo '<tr>';
							echo '<td>'.$r->idClientes.'</td>';
							echo '<td>'.$r->nomCliente.'</td>';
							echo '<td>'.$r->documento.'</td>';
							echo '<td>'.$r->telefono.'</td>';
							echo '<td>'.$r->celular.'</td>';
							echo '<td>'.$r->email.'</td>';
							echo '<td>';
							if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vCliente')){
							echo '<a href="'.base_url().'index.php/clientes/visualizar/'.$r->idClientes.'" style="margin-right: 1%" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>';
							}
							if($this->permission->checkPermission($this->session->userdata('permisos_id'),'eCliente')){
							echo '<a href="'.base_url().'index.php/clientes/editar/'.$r->idClientes.'" style="margin-right: 1%" class="btn btn-info tip-top" title="Editar Cliente"><i class="icon-pencil icon-white"></i></a>';
							}
							if($this->permission->checkPermission($this->session->userdata('permisos_id'),'dCliente')){
							echo '<a href="#modal-excluir" role="button" data-toggle="modal" cliente="'.$r->idClientes.'" style="margin-right: 1%" class="btn btn-danger tip-top" title="Excluir Cliente"><i class="icon-remove icon-white"></i></a>';
							}
							echo '</td>';
							echo '</tr>';
						}
					?>
					<tr>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php echo $this->pagination->create_links();}?>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form action="<?php echo base_url() ?>index.php/clientes/excluir" method="post" >
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h5 id="myModalLabel">Borrar Cliente</h5>
	</div>
	<div class="modal-body">
		<input type="hidden" id="idCliente" name="id" value="" />
		<h5 style="text-align: center">Desea realmente borrar este cliente y los datos asociados a el (OC, Ventas, Facturas)?</h5>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button class="btn btn-danger">Borrar</button>
	</div>
	</form>
</div>

<script>
	$(document).ready(function(){
		$(document).on('click', 'a', function(event) {
			var cliente = $(this).attr('cliente');
			$('#idCliente').val(cliente);
		});
	});
</script>
