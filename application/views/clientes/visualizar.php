<div class="widget-box">
	<div class="widget-title">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#tab1">Datos de Cliente</a></li>
			<li><a data-toggle="tab" href="#tab2">Ordenes de Compra</a></li>
			<div class="buttons">
				<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'eCliente')){
					echo '<a title="Icon Title" class="btn btn-mini btn-info" href="'.base_url().'index.php/clientes/editar/'.$result->idClientes.'"><i class="icon-pencil icon-white"></i> Editar</a>';
				} ?>
			</div>
		</ul>
	</div>
	<div class="widget-content tab-content">
		<div id="tab1" class="tab-pane active" style="min-height: 300px">
			<div class="accordion" id="collapse-group">
				<div class="accordion-group widget-box">
					<div class="accordion-heading">
						<div class="widget-title">
							<a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse">
								<span class="icon"><i class="icon-list"></i></span><h5>Datos Personales</h5>
							</a>
						</div>
					</div>
					<div class="collapse in accordion-body" id="collapseGOne">
						<div class="widget-content">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<td style="text-align: right; width: 30%"><strong>Nombre</strong></td>
										<td><?php echo $result->nomCliente ?></td>
									</tr>
									<tr>
										<td style="text-align: right"><strong>Documento</strong></td>
										<td><?php echo $result->documento ?></td>
									</tr>
									<tr>
										<td style="text-align: right"><strong>Datos de Registro</strong></td>
										<td><?php echo date('d-m-Y',  strtotime($result->fechacli)) ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="accordion-group widget-box">
					<div class="accordion-heading">
						<div class="widget-title">
							<a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse">
								<span class="icon"><i class="icon-list"></i></span><h5>Contactos</h5>
							</a>
						</div>
					</div>
					<div class="collapse accordion-body" id="collapseGTwo">
						<div class="widget-content">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<td style="text-align: right; width: 30%"><strong>Telefono</strong></td>
										<td><?php echo $result->telefono ?></td>
									</tr>
									<tr>
										<td style="text-align: right"><strong>Celular</strong></td>
										<td><?php echo $result->celular ?></td>
									</tr>
									<tr>
										<td style="text-align: right"><strong>Email</strong></td>
										<td><?php echo $result->email ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="accordion-group widget-box">
					<div class="accordion-heading">
						<div class="widget-title">
							<a data-parent="#collapse-group" href="#collapseGThree" data-toggle="collapse">
								<span class="icon"><i class="icon-list"></i></span><h5>Direccion</h5>
							</a>
						</div>
					</div>
					<div class="collapse accordion-body" id="collapseGThree">
						<div class="widget-content">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<td style="text-align: right; width: 30%"><strong>Calle</strong></td>
										<td><?php echo $result->calle ?></td>
									</tr>
									<tr>
										<td style="text-align: right"><strong>Número</strong></td>
										<td><?php echo $result->numero ?></td>
									</tr>
									<tr>
										<td style="text-align: right"><strong>Sector</strong></td>
										<td><?php echo $result->barrio ?></td>
									</tr>
									<tr>
										<td style="text-align: right"><strong>Cidade</strong></td>
										<td><?php echo $result->ciudad ?> - <?php echo $result->estado ?></td>
									</tr>
									<!--<tr>
										<td style="text-align: right"><strong>CEP</strong></td>
										<td><?php //echo $result->cep ?></td>
									</tr>-->
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--Tab 2-->
		<div id="tab2" class="tab-pane" style="min-height: 300px">
			<?php if (!$results) { ?>
			<table class="table table-bordered ">
				<thead>
					<tr style="backgroud-color: #2D335B">
						<th>#</th>
						<th>Fecha Inicial</th>
						<th>Fecha Final</th>
						<th>Descripcion</th>
						<th>Defecto</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="6">Ninguna Orden de Compra Registrada</td>
					</tr>
				</tbody>
			</table>
			<?php } else { ?>
			<table class="table table-bordered ">
				<thead>
					<tr style="backgroud-color: #2D335B">
						<th>#</th>
						<th>Fecha Inicial</th>
						<th>Fecha Final</th>
						<th>Descripcion</th>
						<th>Defecto</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($results as $r) {
						$fechaInicial = date(('d-m-Y'), strtotime($r->fechaInicial));
						$fechaFinal = date(('d-m-Y'), strtotime($r->fechaFinal));
						echo '<tr>';
						echo '<td>' . $r->idOs . '</td>';
						echo '<td>' . $fechaInicial . '</td>';
						echo '<td>' . $fechaFinal . '</td>';
						echo '<td>' . $r->descriProducto . '</td>';
						echo '<td>' . $r->defecto . '</td>';
						echo '<td>';
							if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vOs')){
						echo '<a href="' . base_url() . 'index.php/os/visualizar/' . $r->idOs . '" style="margin-right: 1%" class="btn tip-top" title="Ver mas detalles"><i class="icon-eye-open"></i></a>';
										}
							if($this->permission->checkPermission($this->session->userdata('permisos_id'),'eOs')){
						echo '<a href="' . base_url() . 'index.php/os/editar/' . $r->idOs . '" class="btn btn-info tip-top" title="Editar OC"><i class="icon-pencil icon-white"></i></a>';
										}

						echo  '</td>';
						echo '</tr>';
						} ?>
					<tr>
					</tr>
				</tbody>
			</table>
			<?php  } ?>
		</div>
	</div>
</div>