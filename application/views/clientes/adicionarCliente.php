<div class="row-fluid" style="margin-top:0">
	<div class="span12">
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon">
						<i class="icon-user"></i>
				</span>
				<h5>Registro de Cliente</h5>
			</div>
			<div class="widget-content nopadding">
				<?php if ($custom_error != '') {
						echo '<div class="alert alert-danger">' . $custom_error . '</div>';
				} ?>
				<form action="<?php echo current_url(); ?>" id="formCliente" method="post" class="form-horizontal" >
					<div class="control-group">
						<label for="nomCliente" class="control-label">Nombre<span class="required">*</span></label>
						<div class="controls">
								<input id="nomCliente" type="text" name="nomCliente" value="<?php echo set_value('nomCliente'); ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="documento" class="control-label">Cedula / Rif<span class="required">*</span></label>
						<div class="controls">
								<input id="documento" type="text" name="documento" value="<?php echo set_value('documento'); ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="telefono" class="control-label">Telefono<span class="required">*</span></label>
						<div class="controls">
							<input id="telefono" type="text" name="telefono" value="<?php echo set_value('telefono'); ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="celular" class="control-label">Celular</label>
						<div class="controls">
							<input id="celular" type="text" name="celular" value="<?php echo set_value('celular'); ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="email" class="control-label">Email<span class="required">*</span></label>
						<div class="controls">
							<input id="email" type="text" name="email" value="<?php echo set_value('email'); ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="calle" class="control-label">Calle<span class="required">*</span></label>
						<div class="controls">
							<input id="calle" type="text" name="calle" value="<?php echo set_value('calle'); ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="numero" class="control-label">Número<span class="required">*</span></label>
						<div class="controls">
							<input id="numero" type="text" name="numero" value="<?php echo set_value('numero'); ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="barrio" class="control-label">Sector<span class="required">*</span></label>
						<div class="controls">
							<input id="barrio" type="text" name="barrio" value="<?php echo set_value('barrio'); ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="ciudad" class="control-label">Ciudad<span class="required">*</span></label>
						<div class="controls">
							<input id="ciudad" type="text" name="ciudad" value="<?php echo set_value('ciudad'); ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="estado" class="control-label">Estado<span class="required">*</span></label>
						<div class="controls">
							<input id="estado" type="text" name="estado" value="<?php echo set_value('estado'); ?>"  />
						</div>
					</div>

					<!--<div class="control-group" class="control-label">
							<label for="cep" class="control-label">CEP<span class="required">*</span></label>
							<div class="controls">
									<input id="cep" type="text" name="cep" value="<?php //echo set_value('cep'); ?>"  />
							</div>
					</div>-->

					<div class="form-actions">
						<div class="span12">
							<div class="span6 offset3">
								<button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Agregar</button>
								<a href="<?php echo base_url() ?>index.php/clientes" id="" class="btn"><i class="icon-arrow-left"></i> Volver</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script>
	$(document).ready(function(){
		 $('#formCliente').validate({
			rules :{
				nomCliente:{ required: true},
				documento:{ required: true},
				telefono:{ required: true},
				email:{ required: true},
				calle:{ required: true},
				numero:{ required: true},
				barrio:{ required: true},
				ciudad:{ required: true},
				estado:{ required: true}
				//cep:{ required: true}
			},
			messages:{
				nomCliente :{ required: 'Campo Requerido.'},
				documento :{ required: 'Campo Requerido.'},
				telefono:{ required: 'Campo Requerido.'},
				email:{ required: 'Campo Requerido.'},
				calle:{ required: 'Campo Requerido.'},
				numero:{ required: 'Campo Requerido.'},
				barrio:{ required: 'Campo Requerido.'},
				ciudad:{ required: 'Campo Requerido.'},
				estado:{ required: 'Campo Requerido.'}
				//cep:{ required: 'Campo Requerido.'}
			},

			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
				$(element).parents('.control-group').addClass('success');
			}
		 });
	});
</script>




