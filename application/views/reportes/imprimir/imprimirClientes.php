  <head>
    <title>Textil OC</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/blue.css" class="skin-color" />
    <script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

  <body style="background-color: transparent">



      <div class="container-fluid">

          <div class="row-fluid">
              <div class="span12">

                  <div class="widget-box">
                      <div class="widget-title">
                          <h4 style="text-align: center">Clientes</h4>
                      </div>
                      <div class="widget-content nopadding">

                  <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th style="font-size: 1.2em; padding: 5px;">Nombre</th>
                              <th style="font-size: 1.2em; padding: 5px;">Cedula / Rif</th>
                              <th style="font-size: 1.2em; padding: 5px;">Telefono</th>
                              <th style="font-size: 1.2em; padding: 5px;">Email</th>
                              <th style="font-size: 1.2em; padding: 5px;">Registrado Desde</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                          foreach ($clientes as $c) {
                              $fechacli = date('d-m-Y', strtotime($c->fechacli));
                              echo '<tr>';
                              echo '<td>' . $c->nomCliente . '</td>';
                              echo '<td>' . $c->documento . '</td>';
                              echo '<td>' . $c->telefono . '</td>';
                              echo '<td>' . $c->email . '</td>';
                              echo '<td>' . $fechacli . '</td>';
                              echo '</tr>';
                          }
                          ?>
                      </tbody>
                  </table>

                  </div>

              </div>
                  <h5 style="text-align: right">Fecha del Reporte: <?php echo date('d-m-Y');?></h5>

          </div>



      </div>
</div>




            <!-- Arquivos js-->

            <script src="<?php echo base_url();?>assets/js/excanvas.min.js"></script>
            <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url();?>assets/js/jquery.flot.min.js"></script>
            <script src="<?php echo base_url();?>assets/js/jquery.flot.resize.min.js"></script>
            <script src="<?php echo base_url();?>assets/js/jquery.peity.min.js"></script>
            <script src="<?php echo base_url();?>assets/js/fullcalendar.min.js"></script>
            <script src="<?php echo base_url();?>assets/js/sosmc.js"></script>
            <script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
  </body>
</html>







