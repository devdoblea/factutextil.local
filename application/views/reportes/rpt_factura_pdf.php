<?php
	// (c) Xavier Nicolay
	// Exemple de g�n�ration de devis/facture PDF

	// captura de los posteos
	if (!$this->input->post("repopdf")) {

		$nrotmp = $this->uri->segment(3);
		// funcion para invertir las fechas
		function cambfecha($fecha) {
			$fech = explode('-',$fecha);
			$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];
			return $fnac;
		}

	 } else {

		$nrotmp = $this->input->post("repopdf");
		// funcion para invertir las fechas
		function cambfecha($fecha) {
			$fech = explode('-',$fecha);
			$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];
			return $fnac;
		}
	}

	//$nrotmp = $this->input->post("repopdf");

	// para sacar datos del colegio
	$empresa = $this->Servicios_model->getEmpresa();
	foreach ( $empresa as $r ){
		$nome = $r->nombrempresa;
		$rife = $r->rif;
		$impu = $r->iva;
		$dire = $r->direccion;
		$nroe = $r->numero;
		$sete = $r->sector;
		$city = $r->ciudad;
		$mail = $r->email;
		$tlfn = $r->telefonoemp;
	}

	$fact = $this->Servicios_model->listar_encabezado_nrotemp($nrotmp);
	foreach ($fact as $r) {
		$idvt = sprintf("%05d",$r->idVentas);
		$fven = cambfecha($r->fechaVenta);
		$vtot = $r->valorTotal;
		$faid = $r->nrotemp_id;
		$clid = $r->clientes_id;
		$nomc = $r->nomCliente;
		$docc = $r->documento;
		$dirc = $r->direccion;
		$tlfc = $r->telefono;
		$celc = $r->celular;
		$maic = $r->email;
		$calc = $r->calle;
		$numc = $r->numero;
		$setc = $r->barrio;
		$cddc = $r->ciudad;
		$estc = $r->estado;
	}

	// Creacion del PDF

	/*
	 * Se crea un objeto de la clase Pdf, recuerda que la clase Pdf
	 * hered� todos las variables y m�todos de fpdf
	 */
	$this->pdf = new Invoice( 'P', 'mm', 'A4' );
	$this->pdf->AddPage();
	$this->pdf->addSociete( $nome,
									$dire."\n".
									$sete."\n".
									$city."\n".
									$mail."\n".
									$tlfn);
	$this->pdf->fact_dev( "Factura", $idvt );
	$this->pdf->temporaire( "VAER UNIFORMES" );
	$this->pdf->addDate( $fven);
	$this->pdf->addClient($clid);
	$this->pdf->addPageNumber("1");
	$this->pdf->addClientAdresse(
									"Cliente: ".$nomc."\n".
									"Direccion Cliente: ".$dirc."\n".
									"Calle: ".$calc."\n".
									"Casa Nro: ".$numc."\n".
									"Sector: ".$setc."\n".
									"Ciudad: ".$cddc."\n".
									"Estado: ".$estc);
	$this->pdf->addReglement("Efectivo / Transferencia");
	$this->pdf->addEcheance($fven);
	$this->pdf->addNumTVA($docc);
	$this->pdf->addReference("");
	$cols=array( "COD_PROD"     => 25,
						 "DESCRIPCION"  => 78,
						 "CANT"             => 12,
						 "P.U."             => 24,
						 "MONTO"                => 30,
						 "UND"                  => 21 );
	$this->pdf->addCols( $cols);
	$cols=array( "COD_PROD"   => "L",
						 "DESCRIPCION"  => "L",
						 "CANT"             => "C",
						 "P.U."             => "R",
						 "MONTO"                => "R",
						 "UND"            => "C" );
	$this->pdf->addLineFormat( $cols);
	$this->pdf->addLineFormat($cols);

	$productos = $this->Servicios_model->listar_art_nrotemp($nrotmp);

	$y    = 109;

		foreach ($productos as $r) {
			$monto = number_format($r->precio * $r->cant,2,',','.');
			$precio = number_format($r->precio,2,',','.');

			$line = array( "COD_PROD"           => $r->codprod,
										 "DESCRIPCION"  => $r->descriProd,
										 "CANT"             => $r->cant,
										 "P.U."             => $precio,
										 "MONTO"                => $monto,
										 "UND"            => $r->unidad );
			$size = $this->pdf->addLine( $y, $line );
			$y   += $size + 2;
		}




	$this->pdf->addCadreTVAs();

	// invoice = array( "px_unit" => value,
	//                  "qte"     => qte,
	//                  "tva"     => code_tva );
	// tab_tva = array( "1"       => 19.6,
	//                  "2"       => 5.5, ... );
	// params  = array( "RemiseGlobale" => [0|1],
	//                      "remise_tva"     => [1|2...],  // {la remise s'applique sur ce code TVA}
	//                      "remise"         => value,     // {montant de la remise}
	//                      "remise_percent" => percent,   // {pourcentage de remise sur ce montant de TVA}
	//                  "FraisPort"     => [0|1],
	//                      "portTTC"        => value,     // montant des frais de ports TTC
	//                                                     // par defaut la TVA = 19.6 %
	//                      "portHT"         => value,     // montant des frais de ports HT
	//                      "portTVA"        => tva_value, // valeur de la TVA a appliquer sur le montant HT
	//                  "AccompteExige" => [0|1],
	//                      "accompte"         => value    // montant de l'acompte (TTC)
	//                      "accompte_percent" => percent  // pourcentage d'acompte (TTC)
	//                  "Remarque" => "texte"              // texte
	$tot_prods = array( array ( "px_unit" => $vtot, "qte" => 1, "tva" => 1 ),
											array ( "px_unit" =>     0, "qte" => 1, "tva" => 1 ));
	$tab_tva = array( "1"       => $impu,
										"2"         => 0);
	$params  = array( "RemiseGlobale"       => 0,
											"remise_tva"        => 1,     // {la remise s'applique sur ce code TVA}
											"remise"            => 1,     // {montant de la remise}
											"remise_percent"    => 0,     // {pourcentage de remise sur ce montant de TVA}
											"FraisPort"         => 0,
											"portTTC"           => 1,           // montant des frais de ports TTC
																									// par defaut la TVA = 19.6 %
											"portHT"            => 1,       // montant des frais de ports HT
											"portTVA"           => 1,           // valeur de la TVA a appliquer sur le montant HT
											"AccompteExige"     => 1,
											"accompte"        => ($vtot*$impu)/100,         // montant de l'acompte (TTC)
											"accompte_percent"=> 1,         // pourcentage d'acompte (TTC)
											"Nota" => "Esta es una prueba" );

	$this->pdf->addTVAs( $params, $tab_tva, $tot_prods);
	$this->pdf->addCadreEurosFrancs();
	//$this->pdf->Output();
	$this->pdf->Output('FacturaNro-'."-".date('d-m-Y H:m:s').".pdf", 'I');
?>
