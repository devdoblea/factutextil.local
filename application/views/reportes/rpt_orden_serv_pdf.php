<?php
	// funcion para invertir las fechas
	/*function cambfecha($fecha) {
		$fech = explode('-',$fecha);
		$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];
		return $fnac;
	}*/
	$varbusqueda = $this->security->xss_clean($this->uri->segment(3));
	// Recibiendo lo posteado
	if ($varbusqueda) {

		// funcion para invertir las fechas
		function cambfecha($fecha) {
			$fech = explode('-',$fecha);
			$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];
			return $fnac;
		}

		$codserv = $this->uri->segment(3);
		// se obtienen todos los datos de la orden de servicio
		$encabezado = $this->Servicios_model->listar_encabezado_ord_serv($codserv);
		// se obtienen todos los datos de la descripcion de la orden de servicio
		$descripcion = $this->Servicios_model->listar_art_ord_serv($codserv);

	}elseif(!$varbusqueda){

		$nrotemp = $this->input->post('nrotemp');
		// se obtienen todos los datos de la orden de servicio
		$encabezado = $this->Servicios_model->listar_encabezado_nrotemp($nrotemp);
		// se obtienen todos los datos de la descripcion de la orden de servicio
		$descripcion = $this->Servicios_model->listar_art_nrotemp($nrotemp);

	} else {
		echo '
			<script>
				alertify.log("Faltan Parametros.!","", 0);
			</script>
			';
	}

	// busco los datos para el encabezado
	foreach ($encabezado as $orden) {
		$va   	= $orden->codserv;
		$csol 	= $orden->cedsolicit;
		$nsol 	= $orden->apenombre;
		$dirr 	= $orden->dirref;
		$dir  	= $orden->direccion;
		$tlfcas 	= $orden->tlfnocasa;
		$tlfcel 	= $orden->tlfnocel;
		$correo = $orden->ecorreo;
		$fsol 	= cambfecha($orden->fservicio);
	}

	// saco los datos que debo colocar en el encabezado
	$datosH = array(
		'Titulo' 	=>	'FACTURA',
		'nrorden' =>	$va,
		'nombcli' =>	utf8_encode($nsol),
		'fserv'	 	=>	$fsol,
		'dirref'	=>	utf8_encode($dirr),
		'dircli'	=>	utf8_encode($dir),
		'tlfnoca'	=>	$tlfcas,
		'tlfnoce'	=>	$tlfcel,
		'mailcli'	=>	$correo
	);
	//echo 'Subtitulo: '.$datosH['Subtitulo'];

	// Creacion del PDF

	/*
	 * Se crea un objeto de la clase Pdf, recuerda que la clase Pdf
	 * heredó todos las variables y métodos de fpdf
	 */
	$this->pdf = new Pdf('P','mm','letter');

	/* Se define el titulo, márgenes izquierdo, derecho y
	 * el color de relleno predeterminado
	 */
	$this->pdf->SetTitle(iconv("UTF-8","ISO-8859-1","SOLUTEC - Orden Servicio"));
	$this->pdf->SetTopMargin(2);
	$this->pdf->SetLeftMargin(10);
	$this->pdf->SetRightMargin(10);
	$this->pdf->SetFillColor(200,200,200);

	// asigno el valor a los encabezados
	$this->pdf->SetDatosHeader($datosH);
	// cargar un encabezado diferente
	$this->pdf->header = 1;
	// cargar un footer diferente
	$this->pdf->footer = 0;
	// Agregamos una página
	$this->pdf->AddPage();
	// Define el alias para el número de página que se imprimirá en el pie
	$this->pdf->AliasNbPages();

	/*
	 * TITULOS DE COLUMNAS
	 *
	 * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
	 */
	/* los datos mostrados aqui se extraen del modelo pero dependen de donde se haga
	    la solicitud. desde la tablaserv o desde la orden de servicio*/
	// Se define el formato de fuente: Arial, negritas, tamaño 9
	$x = 1;
	$tot = 0;

	foreach ($descripcion as $r) {
		// Se imprimen los datos de cada desc
		$this->pdf->SetFont('Arial', 'B', 8);
		$this->pdf->Cell(12,8,$r->cantpedido,1,0,'C','0');
		$this->pdf->Cell(90,8,$r->descripserv,1,0,'L','0');
		$this->pdf->Cell(16,8,cambfecha($r->fechacitaserv),1,0,'C','0');
		$this->pdf->Cell(16,8,$r->tipoart,1,0,'C','0');
		$this->pdf->Cell(16,8,number_format($r->preciounit,2,',','.'),1,0,'C','0');
		$this->pdf->SetFont('Arial', 'B', 4);
		$this->pdf->MultiCell(40,8,$r->observacion,1,'J',0);

		//Se agrega un salto de linea
		//$this->pdf->Ln(8);
		$x++;
		$tot += $r->preciounit;
	}

	while ($x <= 5) {
		$x++;
		$this->pdf->Cell(12,8,'',1,0,'C','0');
		$this->pdf->Cell(90,8,'',1,0,'C','0');
		$this->pdf->Cell(16,8,'',1,0,'C','0');
		$this->pdf->Cell(16,8,'',1,0,'C','0');
		$this->pdf->Cell(16,8,'',1,0,'C','0');
		$this->pdf->Cell(40,8,'',1,0,'C','0');
		$this->pdf->Ln(8);//Se agrega un salto de linea
	}

		// linea para los totales
		$iva = 0;
		$this->pdf->Ln(8);
		$this->pdf->SetFont('Arial','B',10);
		$this->pdf->Cell(110,8,'RECIBIDO POR:','TLR',0,'L','0');
		$this->pdf->Cell(20,8,'',0,0,'R','0');
		$this->pdf->Cell(20,8,'SubTotal:',0,0,'R','0');
		$this->pdf->Cell(40,8,number_format($tot,2,',','.'),1,0,'C','0');
		$this->pdf->Ln(8);
		$this->pdf->Cell(110,8,'','LR',0,'R','0');
		$this->pdf->Cell(20,8,'',0,0,'R','0');
		$this->pdf->Cell(20,8,'IVA:',0,0,'R','0');
		$this->pdf->Cell(40,8,number_format($iva,2,',','.'),1,0,'C','0');
		$this->pdf->Ln(8);
		$this->pdf->Cell(110,8,'','BLR',0,'R','0');
		$this->pdf->Cell(20,8,'',0,0,'R','0');
		$this->pdf->Cell(20,8,'TOTAL:',0,0,'R','0');
		$this->pdf->Cell(40,8,number_format(($tot+$iva),2,',','.'),1,0,'C','0');
		$this->pdf->Ln(8);

		//Pie de pagina en media pagina
		$this->pdf->SetFont('Arial','B',6);
		$this->pdf->Cell(190,4,'Original',0,0,'R','0');

	// genero un salto de pagina
	$this->pdf->AcceptPageBreak();
	/*
	 * Se manda el pdf al navegador
	 *
	 * $this->pdf->Output(nombredelarchivo, destino);
	 *
	 * I = Muestra el pdf en el navegador
	 * D = Envia el pdf para descarga
	 *
	 */
	$this->pdf->Output('$va'."-".date('d-m-Y H:m:s').".pdf", 'I');
?>