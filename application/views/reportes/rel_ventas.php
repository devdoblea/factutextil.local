<link href="<?php echo base_url();?>assets/js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<div class="row-fluid" style="margin-top: 0">
    <div class="span4">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Reportes Rápidos</h5>
            </div>
            <div class="widget-content">
                <ul class="site-stats">
                    <li><a href="<?php echo base_url()?>index.php/reportes/ventasRapid"><i class="icon-tags"></i> <small>Todas las Ventas</small></a></li>

                </ul>
            </div>
        </div>
    </div>

    <div class="span8">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Reportes Personalizables</h5>
            </div>
            <div class="widget-content">
                <div class="span12 well">

                    <form action="<?php echo base_url() ?>index.php/reportes/ventasCustom" method="get">
                        <div class="span12 well">
                            <div class="span6">
                                <label for="">Datos desde:</label>
                                <input type="text" id="dataInicial" name="dataInicial" class="datepicker span12" autocomplete="off" />
                            </div>
                            <div class="span6">
                                <label for="">Hasta:</label>
                                <input type="text" id="dataFinal" name="dataFinal" class="datepicker span12" autocomplete="off" />
                            </div>
                        </div>
                        <div class="span12 well" style="margin-left: 0">
                            <div class="span6">
                                <label for="">Cliente:</label>
                                <input type="text"  id="cliente" class="span12" />
                                <input type="hidden" name="cliente" id="clienteHide" />

                            </div>
                            <div class="span6">
                                <label for="">Vendedor:</label>
                                <input type="text" id="tecnico"   class="span12" />
                                <input type="hidden" name="responsavel" id="responsavelHide" />
                            </div>
                        </div>


                        <div class="span12" style="margin-left: 0; text-align: center">
                            <input type="reset" class="btn" value="Limpiar" />
                            <button class="btn btn-inverse"><i class="icon-print icon-white"></i> Imprimir</button>
                        </div>
                    </form>
                </div>
                .
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.maskMoney.js"></script>
<script >
    $(document).ready(function(){
        $(".money").maskMoney();

        $("#cliente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteCliente",
            minLength: 2,
            select: function( event, ui ) {

                 $("#clienteHide").val(ui.item.id);


            }
      });

      $("#tecnico").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteUsuario",
            minLength: 2,
            select: function( event, ui ) {

                 $("#responsavelHide").val(ui.item.id);


            }
      });

    });
</script>
<script>
    $("#dataInicial").datepicker({ dateFormat: 'dd-mm-yy' });
    $("#dataFinal").datepicker({ dateFormat: 'dd-mm-yy' });
</script>