<link href="<?php echo base_url()?>assets/js/FormValidation/formValidation.css" rel="stylesheet" type="text/css" />
<div class="row-fluid" style="margin-top:0">
	<div class="span12">
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"><i class="icon-align-justify"></i></span>
					<h5>Editar Producto</h5>
			</div>
			<div class="widget-content nopadding">
				<?php
					// este es para mostrar los mensajes q vienen del controlador despues de efectuar operaciones en las vistas
					// mensaje de alerta en caso de error mostrado con alertify.js
					if (!$this->session->flashdata('mensaje')) {
						echo '';
					} else {
						echo $this->session->flashdata('mensaje');
					}
				?>
				<form action="<?php echo current_url(); ?>" id="formProduto" method="post" class="form-horizontal" >

					<div class="control-group">
						<label for="codprod" class="control-label">Codigo del Producto:</label>
							<div class="controls">
								<input type="text" id="codprod" name="codprod" maxlength="10" value="<?php echo $result->codprod; ?>" readonly />
								<?php echo form_error('codprod', '<span style="color:red">', '</span>'); ?>
							</div>
					</div>

					<div class="control-group">

						<?php echo form_hidden('idProductos',$result->idProductos) ?>
						<label for="descriProd" class="control-label">Descripcion</label>
						<div class="controls">
							<input id="descriProd" type="text" name="descriProd" value="<?php echo $result->descriProd; ?>"  />
							<?php echo form_error('descriProd', '<span style="color:red">', '</span>'); ?>
						</div>

						<label for="unidad" class="control-label">Unidad</label>
						<div class="controls">
							<select id="unidad" name="unidad" >
									<option value="<?php echo $result->unidad;?>"><?php echo $result->unidad;?></option>
									<option value="UNIDAD"> UNIDAD </option>
									<option value="DOCENA"> DOCENA </option>
									<option value="LOTE"	> LOTE 	 </option>
									<option value="CAJA"	> CAJA 	 </option>
							</select>
							<?php echo form_error('unidad', '<span style="color:red">', '</span>'); ?>
						</div>

						<label for="categoria" class="control-label">Categoria: </label>
						<div class="controls">
							<select id="categoria" name="categoria">
								<option value="<?php echo $result->categoria;?>"><?php echo $result->categoria;?></option>
								<option value="ESCOLAR"		> ESCOLAR 	</option>
								<option value="DEPORTIVA"	> DEPORTIVA </option>
								<option value="EJECUTIVA"	> EJECUTIVA </option>
								<option value="INFORMAL"	> INFORMAL	</option>
							</select>
							<?php echo form_error('categoria', '<span style="color:red">', '</span>'); ?>
						</div>

						<label for="estilo" class="control-label">Estilo: </label>
						<div class="controls">
							<select id="estilo" name="estilo">
								<option value="<?php echo $result->estilo;?>"><?php echo $result->estilo;?></option>
								<option value="CHEMISE"	> CHEMISE </option>
								<option value="CAMISA"	> CAMISA 	</option>
								<option value="LINO"		> LINO 		</option>
								<option value="JEANS"		> JEANS		</option>
							</select>
							<?php echo form_error('estilo', '<span style="color:red">', '</span>'); ?>
						</div>

						<label for="tipo" class="control-label">Tipo: </label>
						<div class="controls">
							<select id="tipo" name="tipo">
								<option value="<?php echo $result->tipo;?>"><?php echo $result->tipo;?></option>
								<option value="COMBO"			> COMBO 		</option>
								<option value="INDIVIDUAL"> INDIVIDUAL</option>
							</select>
							<?php echo form_error('tipo', '<span style="color:red">', '</span>'); ?>
						</div>

						<label for="talla" class="control-label">Talla: </label>
						<div class="controls">
							<select id="talla" name="talla">
								<option value="<?php echo $result->talla;?>"><?php echo $result->talla;?></option>
								<option value="S"			> S 		</option>
								<option value="M"			> M 		</option>
								<option value="L"			> L 		</option>
								<option value="XL"		> XL		</option>
								<option value="XXL"		> XXL		</option>
								<option value="4-6"		> 4-6		</option>
								<option value="8-10"	> 8-10	</option>
								<option value="12-14"	> 12-14	</option>
								<option value="16"		> 16		</option>
							</select>
							<?php echo form_error('talla', '<span style="color:red">', '</span>'); ?>
						</div>

						<label for="color" class="control-label">Color: </label>
						<div class="controls">
							<select id="color" name="color">
								<option value="<?php echo $result->color;?>"><?php echo $result->color;?></option>
								<option value="BLANCO"> BLANCO	</option>
								<option value="AZUL"	> AZUL		</option>
								<option value="BEIGE"	> BEIGE		</option>
							</select>
							<?php echo form_error('color', '<span style="color:red">', '</span>'); ?>
						</div>

						<label for="caractprod" class="control-label">Caracteristicas</label>
						<div class="controls">
							<textarea style="text-align:justify; margin-bottom:0.3cm;" name="caractprod" id="caractprod" rows="3" cols="32" spellcheck="true" onkeyup="if(this.value.length >= 200){ alert('Has superado el tama&ntilde;o m&aacute;ximo de caracteres permitidos'); return false; }" onchange="javascript:this.value=this.value.toUpperCase();"><?php echo $result->caractprod;?></textarea>
							<?php echo form_error('caractprod', '<span style="color:red">', '</span>'); ?>
						</div>
					</div>

					<div class="control-group">

						<label for="precioCompra" class="control-label">Precio de Compra</label>
						<div class="controls">
							<input id="precioCompra" type="text" name="precioCompra" value="<?php echo $result->precioCompra; ?>"  />
							<?php echo form_error('precioCompra', '<span style="color:red">', '</span>'); ?>
						</div>

						<label for="precioVenta" class="control-label">Precio de Venta</label>
						<div class="controls">
							<input id="precioVenta" type="text" name="precioVenta" value="<?php echo $result->precioVenta; ?>"  />
							<?php echo form_error('precioVenta', '<span style="color:red">', '</span>'); ?>
						</div>

						<label for="precioVentaMay" class="control-label">Precio de Venta al Mayor</label>
							<div class="controls">
								<input id="precioVentaMay" type="text" name="precioVentaMay" value="<?php echo $result->precioVentaMay;?>" />
								<?php echo form_error('precioVentaMay', '<span style="color:red">', '</span>'); ?>
							</div>
					</div>

					<div class="control-group">
						<label for="existencia" class="control-label">Existencia<span class="required">*</span></label>
						<div class="controls">
							<input id="existencia" type="text" name="existencia" value="<?php echo $result->existencia; ?>"  />
						</div>
					</div>

					<div class="control-group">
						<label for="existMinimo" class="control-label">Existencia Mínima</label>
						<div class="controls">
							<input id="existMinimo" type="text" name="existMinimo" value="<?php echo $result->existMinimo; ?>"  />
						</div>
					</div>

					<div class="form-actions">
						<div class="span12">
							<div class="span6 offset3">
								<button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Editar</button>
								<a href="<?php echo base_url() ?>index.php/productos" id="" class="btn"><i class="icon-arrow-left"></i> Volver</a>
							</div>
						</div>
					</div>

				</form>
			</div>
		 </div>
	</div>
</div>


<script src="<?php echo base_url()?>assets/js/jquery.price_format.2.0.js"></script>
<script src="<?php echo base_url()?>assets/js/FormValidation/formValidation.js"></script>
<script src="<?php echo base_url()?>assets/js/FormValidation/formValidation_bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/FormValidation/formValidation_language_es_ES.js"></script>
<script>

	$("#precioCompra").priceFormat({
		prefix: '',
		//prefix: 'Bs. ',
		centsSeparator: ',',
		thousandsSeparator: '.',
		clearPrefix: true
	});

	$("#precioVenta").priceFormat({
		prefix: '',
		//prefix: 'Bs. ',
		centsSeparator: ',',
		thousandsSeparator: '.',
		clearPrefix: true
	});

	$("#precioVentaMay").priceFormat({
		prefix: '',
		//prefix: 'Bs. ',
		centsSeparator: ',',
		thousandsSeparator: '.',
		clearPrefix: true
	});

	$("#existencia").priceFormat({
		limit: 11,
		centsLimit: 0,
		prefix: '',
		centsSeparator: '',
		thousandsSeparator: '',
		clearPrefix: true
	});

	$("#existMinimo").priceFormat({
		limit: 2,
		centsLimit: 0,
		prefix: '',
		centsSeparator: '',
		thousandsSeparator: '',
		clearPrefix: true
	});

	// validacion de campos
	$('#form-registro').formValidation({
		 framework: 'bootstrap',
					icon: {
							valid: 'glyphicon glyphicon-ok',
							invalid: 'glyphicon glyphicon-remove',
							validating: 'glyphicon glyphicon-refresh'
					},
					err: {
							container: 'tooltip'
					},
		 excluded: ':disabled',
		 fields: {
			 codprod: {
				 validators: {
					 notEmpty: {
						 message: 'El Codigo es requerida'
					 },
					 regexp: {
						 regexp: /^[0-9a-zA-ZáéíóúñÁÉÍÓÚÑ\.\s]+$/i,
						 message: 'Solo puede contener Letras y Numeros'
					 },
					 stringLength: {
						min: 5,
						max: 10,
						message: 'Verifique, La Cedula Escolar debe tener minimo 10 Digitos'
					 },
					 remote: {
						message: 'Esta Codigo YA EXISTE',
						url: 'consultcod',
						data: {
							type: 'codprod'
						},
						type: 'POST',
						delay: 1000
					 }
				 }
			 },
			 descriProd: {
				 validators: {
					 notEmpty: {
						 message: 'Descripción es requerida'
					 },
					 regexp: {
						 regexp: /^[0-9a-zA-ZáéíóúñÁÉÍÓÚÑ\.\s]+$/i,
						 message: 'Solo puede contener Letras y Numeros'
					 }
				 }
			 },
			 unidad: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 }
				 }
			 },
			 categoria: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 }
				 }
			 },
			 estilo: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 }
				 }
			 },
			 tipo: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 }
				 }
			 },
			 talla: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 }
				 }
			 },
			 color: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 }
				 }
			 },
			 caractprod: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 },
					 regexp: {
						 regexp: /^[0-9a-zA-ZáéíóúñÁÉÍÓÚÑ\.\s]+$/i,
						 message: 'Solo puede contener Letras y Numeros'
					 }
				 }
			 },
			 precioCompra: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 }
				 }
			 },
			 precioVenta: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 }
				 }
			 },
			 precioVentaMay: {
				 validators: {
					 notEmpty: {
						 message: 'Requerido. No puede ir vacio'
					 }
				 }
			 },
			 tlfnocasrep: {
				 message: 'El Teléfono Principal no es valido',
				 validators: {
					 notEmpty: {
						 message: 'El teléfono Principal es requerido y no puede ir vacio'
					 },
					 regexp: {
						 regexp: /^\b\d{4}[ ]?\d{3}[-]?\d{2}[-]?\d{2}\b/,
						 message: 'El Teléfono Principal solo puede contener números'
					 }
				 }
			 },
		 }
	});

		//calcular precios de venta en caliente
	/*function calcular(){
		var pcompra = parseFloat($("#precioCompra").val());
		var pventa  = (pcompra * 0.3) + parseFloat($("#precioCompra").val());
		var pventM  = (pcompra * 0.15) + parseFloat($("#precioCompra").val());

			// cambio los valores en los inputs involucrados

			$("#precioVenta").val(parseFloat(pventa));
			$("#precioVentaMay").val(parseFloat(pventM));
	};*/
</script>



