<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'aProduto')){
	echo '
		<a href="'.base_url().'index.php/productos/adicionar" class="btn btn-success">
			<i class="icon-plus icon-white"></i> Agregar Producto
		</a>
		';
	}
?>

<?php

if(!$results){?>
	<div class="widget-box">
		 <div class="widget-title">
				<span class="icon"><i class="icon-barcode"></i></span>
				<h5>Productos</h5>
		 </div>
		 <?php
			// este es para mostrar los mensajes q vienen del controlador despues de efectuar operaciones en las vistas
			// mensaje de alerta en caso de error mostrado con alertify.js
			if (!$this->session->flashdata('mensaje')) {
				echo '';
			} else {
				echo $this->session->flashdata('mensaje');
			}
		?>
		<div class="widget-content nopadding">
			<table class="table table-bordered ">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre Producto</th>
						<th>Existencia</th>
						<th>Precio</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5">Ningun Producto Registrado</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

<?php } else{?>

	<div class="widget-box">
		 <div class="widget-title">
			<span class="icon"><i class="icon-barcode"></i></span>
				<h5>Productos</h5>

		 </div>
		<div class="widget-content nopadding">
			<table class="table table-bordered ">
				<thead>
					<tr style="backgroud-color: #2D335B">
						<th>#</th>
						<th>Codigo Producto</th>
						<th>Nombre Producto</th>
						<th>Existencia</th>
						<th>Precio Venta</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($results as $r) {
							echo '<tr>';
							echo '<td>'.$r->idProductos.'</td>';
							echo '<td style="text-align:center">'.$r->codprod.'</td>';
							echo '<td>'.$r->descriProd.'</td>';
							echo '<td style="text-align:center">'.$r->existencia.'</td>';
							echo '<td style="text-align:right">'.number_format($r->precioVenta,2,',','.').'</td>';

							echo '<td>';
							if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vProduto')){
									echo '<a style="margin-right: 1%" href="'.base_url().'index.php/productos/visualizar/'.$r->idProductos.'" class="btn tip-top" title="Visualizar Producto"><i class="icon-eye-open"></i></a>  ';
							}
							if($this->permission->checkPermission($this->session->userdata('permisos_id'),'eProduto')){
									echo '<a style="margin-right: 1%" href="'.base_url().'index.php/productos/editar/'.$r->idProductos.'" class="btn btn-info tip-top" title="Editar Producto"><i class="icon-pencil icon-white"></i></a>';
							}
							if($this->permission->checkPermission($this->session->userdata('permisos_id'),'dProduto')){
									echo '<a href="#modal-excluir" role="button" data-toggle="modal" producto="'.$r->idProductos.'" class="btn btn-danger tip-top" title="Excluir Producto"><i class="icon-remove icon-white"></i></a>';
							}

							echo '</td>';
							echo '</tr>';
						}
					?>
					<tr>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

<?php echo $this->pagination->create_links();}?>



<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form action="<?php echo base_url() ?>index.php/productos/excluir" method="post" >
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h5 id="myModalLabel">Excluir Producto</h5>
	</div>
	<div class="modal-body">
		<input type="hidden" id="idProducto" name="id" value="" />
		<h5 style="text-align: center">Desea realmente excluir este producto?</h5>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button class="btn btn-danger">Excluir</button>
	</div>
	</form>
</div>



<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', 'a', function(event) {
			var producto = $(this).attr('producto');
			$('#idProducto').val(producto);

		});

});

</script>