<div class="accordion" id="collapse-group">
    <div class="accordion-group widget-box">
        <div class="accordion-heading">
            <div class="widget-title">
                <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse">
                    <span class="icon"><i class="icon-list"></i></span><h5>Datos del Producto</h5>
                </a>
            </div>
        </div>
        <div class="collapse in accordion-body">
            <div class="widget-content">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td style="text-align: right; width: 30%"><strong>Descripcion</strong></td>
                            <td><?php echo $result->descriProd ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Unidad</strong></td>
                            <td><?php echo $result->unidad ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Precio de Compra</strong></td>
                            <td>Bs. <?php echo $result->precioCompra; ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Precio de Venta</strong></td>
                            <td>Bs. <?php echo $result->precioVenta; ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Existencia</strong></td>
                            <td><?php echo $result->existencia; ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Existencia Mínimo</strong></td>
                            <td><?php echo $result->existMinimo; ?></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

