<?php
	$idart = $this->input->post('idart');

	// consultar cual articulo
	$this->db->where('idserv', $idart);
	$consulta = $this->db->get('tbservicios');
		foreach ($consulta->result() as $r) {
			$desc = $r->descripserv;
			$prec = $r->preciounit;
			$tipa = $r->tipoart;
		}

?>
<form id="form-modificar" action="<?php echo base_url();?>servicios/borrar_articulo" method="post" role="form">
	<div class="row">
		<div class="panel-header text-center">
			<div class="row">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="box-title" id="myModalLabel">Borrar Articulo</h3>
			</div>
		</div><!-- /.box-header -->
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12 text-center">
					<p><h4>Desea Borrar este Articulo</h4></p>
					<p><h4><?php echo $desc.' - '.$prec.', '.$tipa?></h4></p>
						<p><h4>
							<input type="hidden" name="idart" id="idart" value="<?php echo $idart;?>" />
						</h4></p>
				</div>
			</div>
		</div><!-- final del modal-body -->
		<div class="panel-body"><!--Boton para enviar la info-->
			<div class="col-md-12">
				<?php
					//el botón submit de nuestro formulario
					$submit = array(
						'name'  => 'Borrar',
						'class' => 'btn btn-danger',
						'value' => 'BORRAR',
						'title' => 'Borrar datos'
					);
					echo form_submit($submit);
			 	?>
			 	<button type="button" class="btn btn-default pull-right" data-dismiss="modal">No Borrar</button>
			</div>
		</div>
	</div>
</form>
