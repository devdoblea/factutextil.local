<?php

		// funcion para invertir las fechas
		function cambfecha($fecha) {
			$fech = explode('-',$fecha);
			$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];
			return $fnac;
		}

		$idcomp = $this->uri->segment(3);
		$results = $this->Compras_model->listar_encabezado_id($idcomp);
		foreach ($results as $r) {
			$fcomp 	 = cambfecha($r->fechaCompra);
			$nrotemp = $r->nrotemp_id;
			$nrofact = $r->nrofactcompra;
			$nombprv = $r->nomProv;
			$rif 		 = $r->documento;
			$celu 	 = $r->celular;
			$idprov  = $r->idProv;
		}
?>
<link href="<?php echo base_url();?>assets/js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<div class="row-fluid" style="margin-top:0">
		<div class="span12">
				<div class="widget-box">
						<div class="widget-title">
								<span class="icon">
										<i class="icon-tags"></i>
								</span>
								<h5>Registro de Compras</h5>
						</div>
						<div class="widget-content nopadding">

								<div class="span12" id="divProdutosServicos" style=" margin-left: 0">
										<ul class="nav nav-tabs">
												<li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalles de la Compra</a></li>
										</ul>
										<div class="tab-content">
												<div class="tab-pane active" id="tab1">

													<form action="<?php echo current_url(); ?>" method="post" id="form_Compras" >
														<div class="span5" style="font-size:10px;" >
																<?php if($custom_error == true){ ?>
																	<div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;">Datos incompletos, verifique los campos con asterisco y seleccione correctamente Proveedor</div>
																<?php } ?>

																	<div class="span11" style="padding-left: 3%">
																		<label for="fechaCompra">Fecha de Compra:<span class="required">*</span>
																			<input id="fechaCompra" class="datepicker span4" type="text" name="fechaCompra" value="<?php echo $fcomp; ?>"  />
																			<input type="hidden" name="nrotemp" id="nrotemp" value="<?php echo $nrotemp;?>">
																		</label>
																	</div>

																	<div class="span11">
																		<label for="nrofactcomp">Nro de Factura de Compra:<span class="required">*</span>
																			<input id="nrofactcomp" class="span4" maxlength="10" type="text" name="nrofactcomp" value="<?php echo $nrofact;?>"  />
																		</label>
																	</div>

																	<div class="span11" style="padding: 1%">
																		<div class="span9">
																				<label for="proveedor">Proveedor<span class="required">*</span></label>
																				<input id="proveedor" class="span12" type="text" name="proveedor" value="<?php echo $nombprv?>" />
																				<input id="proveedor_id" class="span12" type="hidden" name="proveedor_id" value="<?php echo $idprov?>"  />
																		</div>
																		<div class="span5">
																				<label for="rif">Rif<span class="required">*</span></label>
																				<input id="rif" class="span12" type="text"  name="rif" value="<?php echo $rif ?>"  />
																		</div>
																		<div class="span5">
																				<label for="celu">Celular<span class="required">*</span></label>
																				<input id="celu" class="span12" type="text" name="celu" value="<?php echo $celu?>" />
																		</div>
																	</div>

																	<div class="span11" style="padding: 1">
																		<div class="span12 ui-widget">
																			<label>Nombre del Producto</label>
																				<input type="text" name="descriProd" id="descriProd" class="span12" maxlength="80" onchange="javascript:this.value=this.value.toUpperCase();"/>
																				<input id="codprod" class="span12" type="hidden" name="codprod" value="" />
																				<input id="unidad" class="span12" type="hidden" name="unidad" value="" />
																		</div>
																		<div class="span3">
																			<label>P. Compra</label>
																				<input type="text" name="precioCompra" id="precioCompra" class="span12 text-center" maxlength="16" autocomplete="off" />
																		</div>
																		<div class="span3">
																			<label>Precio Venta</label>
																				<input type="text" name="precioVenta" id="precioVenta" class="span12 text-center" maxlength="16" autocomplete="off"/>
																		</div>
																		<div class="span3">
																			<label>Cant.</label>
																				<input type="text" name="cantpedido" id="cantpedido" class="span12 text-center" maxlength="6" onchange="javascript:this.value=this.value.toUpperCase();" autocomplete="off"/>
																		</div>

																		<div class="span1" style="padding-top:24px;">
																				<button type="button" id="agregar_art" class="btn btn-success" title="Agrega el articulo que seleccionaste a la Compra"><i class="fa fa-plus"></i> Agregar</button>
																		</div>
																	</div>
														</div>

														<div class="span7" >

																	<?php echo $this->session->flashdata('mensaje');?>

																	<div class="span11" style="padding: 1%;">
																		<div class="span6">

																			<button type="submit" name="cancelar" id="cancelar" class="btn btn-danger pull-left" value="Cancelar Orden" dir="<?php echo base_url()?>index.php/compras/cancelar_compra" title="Haz Clic aqui si ya agregaste Articulos para Eliminarlos y salir de esta Factura"> Borrar Factura</button>
																		</div>
																		<div class="span6">

																			<button type="submit" name="registrar" id="registrar" class="btn btn-primary pull-right" value="Registrar Datos" dir="<?php echo base_url()?>index.php/compras/editar_compra" title="Haz Clic aqui para Registrar esta Factura y Salir">&nbsp;&nbsp; Cargar Factura &nbsp;&nbsp;</a>
																		</div>
																	</div>

																	<div class="span11" style="padding: 1%;">
																		<div id="msgerrores"></div>
																	</div>

																	<div class="span11">
																		<table id="grilla" class="table table-striped table-bordered table-condensed" role="grid">
																			<thead>
																				<tr class="bg-success">
																					<th class="text-center">  Item                </th>
																					<th class="text-center">  Descripción del Articulo Comprado</th>
																					<th class="text-center">  Cantidad             </th>
																					<th class="text-center">  Precio Compra        </th>
																					<th class="text-center">  Precio Venta         </th>
																					<th style='text-align:center'><i class='icon icon-minus-sign'></i></th>
																				</tr>
																			</thead>
																			<tbody id="tbody_listar" style="vertical-align:middle; text-align:center;"> </tbody>
																		</table>
																	</div>
														</div>

													</form>

												</div>

										</div>

								</div>.
						</div>
				</div>
		</div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div id="ventanaBorrar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="ct text-center">
				<h2> Borrando..! <?php echo $this->input->post('idFactcomp');?></h2>
			</div>
		</div>
	</div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div id="ventanaEnviando" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="ct text-center">
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.AreYouSure/jquery.are-you-sure.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.AreYouSure/ays-beforeunload-shim.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script>

		$("#proveedor").autocomplete({
			source: "<?php echo base_url(); ?>index.php/compras/autoCompleteCliente",
			minLength: 1,
			select: function( event, ui ) {
			 	$("#proveedor_id").val(ui.item.id);
			 	$("#rif").val(ui.item.tlfn);
			 	$("#celu").val(ui.item.celu);
			}
		});

		$("#descriProd").autocomplete({
			source: "<?php echo base_url(); ?>index.php/compras/autoCompleteProducto",
			minLength: 1,
			select: function( event, ui ) {
				$("#descriProd").val(ui.item.descriProd);
				$("#precioCompra").val(ui.item.precioc);
				$("#precioVenta").val(ui.item.preciov);
				$("#cantpedido").val('1');
				$("#unidad").val(ui.item.unidad);
				$("#codprod").val(ui.item.codprod);
			}
		});

		$("#form_Compras").validate({
				rules:{
					 nrofactcomp: {required:true},
					 proveedor: {required:true},
					 rif: {required:true},
					 fechaCompra: {required:true}
				},
				messages:{
					 nrofactcomp: {required: 'Campo Requerido.'},
					 proveedor: {required: 'Campo Requerido.'},
					 rif: {required: 'Campo Requerido.'},
					 fechaCompra: {required: 'Campo Requerido.'}
				},

					errorClass: "help-inline",
					errorElement: "span",
					highlight:function(element, errorClass, validClass) {
							$(element).parents('.control-group').addClass('error');
					},
					unhighlight: function(element, errorClass, validClass) {
							$(element).parents('.control-group').removeClass('error');
							$(element).parents('.control-group').addClass('success');
					}
		});

		$("#fechaCompra" ).datepicker({ dateFormat: 'dd-mm-yy' });
</script>
<script>
	// para agregar articulo por articulo a la compra
	$('#agregar_art').on('click', function(event) {
		// compruebo que se envien datos para grabar via ajax
		if($("#nrofactcomp").val().length < 1) {
			alert("Nro de Factura no puede estar vacio\n");
			$("#nrofactcomp").focus();
			return;
		}
		// compruebo que se envien datos para grabar via ajax
		if($("#proveedor").val().length < 1) {
			alert("proveedor no puede estar vacio\n");
			$("#proveedor").focus();
			return;
		}
		if($("#rif").val().length < 1) {
			alert("RIF no puede estar vacio\n");
			$("#rif").focus();
			return;
		}
		if($("#celu").val().length < 1) {
			alert("Celular no puede estar vacio\n");
			$("#celu").focus();
			return;
		}


		if($("#descriProd").val().length < 1) {
			alert("Descripción no puede estar vacio\n");
			$("#descriProd").focus();
			return;
		}
		if($("#cantpedido").val().length < 1) {
			alert("Cantidad no puede estar vacio\n");
			$("#cantpedido").focus();
			return;
		}
		if($("#precioCompra").val().length < 1) {
			alert("Precio Venta no puede estar vacio\n");
			$("#precioCompra").focus();
			return;
		}
		if($("#precioVenta").val().length < 1) {
			alert("Precio Venta no puede estar vacio\n");
			$("#precioVenta").focus();
			return;
		}

		// recibo los datos y los convierto en variables para pasarlos a php
		var idtmp = $('#nrotemp').val();
		var codpr = $('#codprod').val();
		var cantp = $('#cantpedido').val();
		var prcic = $('#precioCompra').val();
		var prciv = $('#precioVenta').val();
		var unidd = $('#unidad').val();
		var dataString = { idtmp:idtmp, codpr:codpr, cantp:cantp, prcic:prcic, prciv:prciv, unidd:unidd };
		$.ajax({
			url: "<?php echo base_url()?>index.php/compras/agregar_art",
			type: "POST",
			data:dataString,
				success: function(data) {
					//limpio los campos de donde envie los datos a ser guardados
					$('#codprod').val('');
					$('#descriProd').val('');
					$('#cantpedido').val('');
					$('#precioCompra').val('');
					$('#precioVenta').val('');
					$('#unidad').val('');
					// muestro otra vez la lista de datos
					fn_buscar();
					$("#descriProd").focus();
				},
				error: function(err) {
					alert('agregar_art: '+err);
				}
		});
	});
</script>
<script>
	// para ejecutar la accion de mostrar los articuloas a listar
	function fn_buscar(){
		var idtmp = $('#nrotemp').val();
		var dataString =  { idtmp:idtmp };

		$.ajax({
			url: '<?php echo base_url()?>index.php/compras/listar_articulos',
			type: 'POST',
			data: dataString,
			success: function(data){
				$("#tbody_listar").html(data);
				$("#descriProd").focus();
			},
			error: function(err) {
				alert('fn_buscar'+err);
			}
		});
	}
</script>
<script>
	// cuando haga clic en el boton borrar
	$('#ventanaBorrar').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idart = button.data('borrar');
		var dataBorra = { idart:idart };

		$.ajax({
			url: "<?php echo base_url()?>index.php/compras/borrar_articulo",
			type: "POST",
			data: dataBorra,
			success: function (data) {
				$("#ventanaBorrar").modal("hide");
				fn_buscar();
			},
			error: function(err) {
				alert(err);
			}
		});
	});
</script>
<script>
	// para cargar el div_listar cuando cargue la pagina
	if (document.addEventListener){
		window.addEventListener('load',fn_buscar,false);
	} else {
		window.attachEvent('onload',fn_buscar);
	}
</script>
<script>
	$(document).bind('keydown',function(e){
		if ( e.which == 27 ) {
			alert('Si deseas salir de la Factura, debes presionar el Boton "Borrar Factura"');
		};
	});
</script>
<script>
	// para enviar el formulario segun el boton que al que se le haga clic
	$("button[type=submit]").click(function() {
		// compruebo que se envien datos para grabar via ajax
		if($("#nrofactcomp").val().length < 1) {
			alert("Nro de Factura no puede estar vacio\n");
			$("#nrofactcomp").focus();
			return;
		}
		// compruebo que se envien datos para grabar via ajax
		if($("#proveedor").val().length < 1) {
			alert("proveedor no puede estar vacio\n");
			$("#proveedor").focus();
			return;
		}
		if($("#rif").val().length < 1) {
			alert("RIF no puede estar vacio\n");
			$("#rif").focus();
			return;
		}
		if($("#celu").val().length < 1) {
			alert("Celular no puede estar vacio\n");
			$("#celu").focus();
			return;
		}

		var accion = $(this).attr('dir');
			$("#ventanaEnviando").delay(200).fadeOut(300, function(){
				$(".ct").append("Espere, por favor..!");
			});
			$('form').attr('action', accion);
			$('form').submit();
	});
</script>
<script>
	//$('#form_Compras').areYouSure( {
	//	'message':'Si quieres salir y ya agregaste un producto, debes presionar el boton "Borrar Factura".!'
	//});
</script>