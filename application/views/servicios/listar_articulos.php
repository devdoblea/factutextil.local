<?php
	$nrotemp = $this->input->post('idtmp');
	$stotal = 0.00;
	$totalpre = 0.00;

	// para saber el iva
	$dataemp = $this->Servicios_model->datos();
	foreach ($dataemp as $r) {
		$alicuota = $r->iva;
	}

	// si hay registros
	$listar = $this->Servicios_model->listar_articulos($nrotemp);
	if($listar->num_rows() > 0) {
		$contador = 0;
		foreach ($listar->result() as $r) {
			$contador = $contador + 1;
			$btn_borrar = array (
				'name' 		=> 'borrar_art',
				'id' 			=> 'borrar_art',
				'type' 		=> 'button',
				'content' => '<i class="icon icon-minus-sign"></i>',
				'class' 	=> 'btn btn-xs btn-danger',
				'data-borrar' => $r->idFacturas,
				'title' 	=> 'Borrar este Articulo',
				'data-toggle' => 'modal',
				'data-target' =>'#ventanaBorrar'
			);

			$cantidad = $r->cant;
			$totalpre = $cantidad * $r->precio;

			echo "
				<tr>
					<td style='text-align:center'>".$contador."</td>
					<td>".$r->codprod." - ".$r->descriProd."</td>
					<td style='text-align:center'>".$r->cant."</td>
					<td style='text-align:center'>".$r->unidad."</td>
					<td style='text-align:right'>".$r->precio."</td>
					<td style='text-align:right'>".number_format($totalpre, 2, ',', '.')."</td>
					<td>
						".form_button($btn_borrar)."
					</td>
				</tr>
			";
			$stotal = $totalpre + $stotal;
		}// fin del foreach
		// para completar la tabla si hay menos de 4 filas
		while ($contador<=4) {
			echo "
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class='text-center'>
						<button class='btn btn-xs btn-disable disabled' ><i class='icon icon-minus-sign'></i></button>
					</td>
				</tr>
			";
				$contador++;
		}
	 } else {
	 	// si no hay registros
	 	$contador = 0;
		while ($contador<=4) {
			echo "
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class='text-center'>
						<button class='btn btn-xs btn-disable disabled' ><i class='icon icon-minus-sign'></i></button>
					</td>
				</tr>
			";
				$contador++;
		}
	}
?>
	<tr>
		<td colspan="5" style="text-align:right">
			<label>Sub-Total:</label>
		</td>
		<td colspan="1" style="text-align:right">
			<?php echo number_format($stotal,2,',','.');?>
		</td>
	</tr>
	<tr>
		<td colspan="5" style="text-align:right">
			<label>IVA <?php echo ($alicuota).'%'?></label>
		</td>
		<td colspan="1" style="text-align:right">
			<?php $iv = $stotal * ($alicuota/100); echo number_format($iv,2,',','.');?>
		</td>
	</tr>
	<tr>
		<td colspan="5" style="text-align:right">
			<label>Precio Total:</label>
		</td>
		<td colspan="1" style="text-align:right">
			<?php $tot = $stotal + $iv; echo number_format($tot,2,',','.');?>
		</td>
	</tr>
