<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/dist/excanvas.min.js"></script><![endif]-->
<script src="<?php echo base_url();?>assets/js/dist/jquery.jqplot.min.js"></script>
<link href="<?php echo base_url();?>assets/js/dist/jquery.jqplot.min.css" rel="stylesheet" type="text/css"/>

<script src="<?php echo base_url();?>assets/js/dist/plugins/jqplot.pieRenderer.js"></script>
<script src="<?php echo base_url();?>assets/js/dist/plugins/jqplot.donutRenderer.js"></script>

<!--Action boxes-->
	<div class="container-fluid">
		<div class="quick-actions_homepage">
			<ul class="quick-actions">
				<?php

					if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vOs')){
						echo '<li class="bg_lv"> <a href="'.base_url().'index.php/compras"> <i class="icon icon-plus-sign"></i> Compras </a> </li>';
					}

					if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vCliente')){
						echo '<li class="bg_lb"> <a href="'.base_url().'index.php/clientes"> <i class="icon-group"></i> Clientes</a> </li>';
					}

					if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vProduto')){
						echo '<li class="bg_lg"> <a href="'.base_url().'index.php/productos"> <i class="icon-barcode"></i> Productos</a> </li>';
					}

					if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vVenda')){
						echo '<li class="bg_ls"> <a href="'.base_url().'index.php/servicios"><i class="icon-shopping-cart"></i> Ventas</a></li>';
					}

					if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vOs')){
						echo '<li class="bg_lo"> <a href="'.base_url().'"> <i class="icon-tags"></i> Orden Compra</a> </li>';
					}

					/*if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vServico')){
						echo '<li class="bg_ly"> <a href="<?php echo base_url()?>index.php/facturas"> <i class="icon-wrench"></i> Facturas</a> </li>';
					}*/
				?>
			</ul>
		</div>
	</div>
<!--End-Action boxes-->



<div class="row-fluid" style="margin-top: 0">

		<div class="span12">

				<div class="widget-box">
						<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Productos Con Existencia Mínima</h5></div>
						<div class="widget-content">
								<table class="table table-bordered">
										<thead>
												<tr>
														<th>#</th>
														<th>Producto</th>
														<th>Precio de Venta</th>
														<th>Existencia</th>
														<th>Existencia Mínima</th>
														<th></th>
												</tr>
										</thead>
										<tbody>
												<?php
												if($productos != null){
														foreach ($productos as $p) {
																echo '<tr>';
																echo '<td>'.$p->idProductos.'</td>';
																echo '<td>'.$p->descriProd.'</td>';
																echo '<td>'.$p->precioVenta.'</td>';
																echo '<td>'.$p->existencia.'</td>';
																echo '<td>'.$p->existMinimo.'</td>';
																echo '<td>';
																if($this->permission->checkPermission($this->session->userdata('permisos_id'),'eProduto')){
																		echo '<a href="'.base_url().'index.php/productos/editar/'.$p->idProductos.'" class="btn btn-info"> <i class="icon-pencil" ></i> </a>  ';
																}
																echo '</td>';
																echo '</tr>';
														}
												}
												else{
														echo '<tr><td colspan="3">Ningun Producto con existencia baja.</td></tr>';
												}

												?>
										</tbody>
								</table>
						</div>
				</div>
		</div>

		<div class="span12" style="margin-left: 0">

				<div class="widget-box">
						<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Ordenes de Compra Abiertas</h5></div>
						<div class="widget-content">
								<table class="table table-bordered">
										<thead>
												<tr>
														<th>#</th>
														<th>Fecha Inicial</th>
														<th>Fecha Final</th>
														<th>Cliente</th>
														<th></th>
												</tr>
										</thead>
										<tbody>
												<?php
												if($ordenes != null){
														foreach ($ordenes as $o) {
																echo '<tr>';
																echo '<td>'.$o->idOs.'</td>';
																echo '<td>'.date('d-m-Y' ,strtotime($o->fechaInicial)).'</td>';
																echo '<td>'.date('d-m-Y' ,strtotime($o->fechaFinal)).'</td>';
																echo '<td>'.$o->nomCliente.'</td>';
																echo '<td>';
																if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vOs')){
																		echo '<a href="'.base_url().'index.php/os/visualizar/'.$o->idOs.'" class="btn"> <i class="icon-eye-open" ></i> </a> ';
																}
																echo '</td>';
																echo '</tr>';
														}
												}
												else{
														echo '<tr><td colspan="3">Ninguna Orden de Compra abierta.</td></tr>';
												}

												?>
										</tbody>
								</table>
						</div>
				</div>
		</div>

</div>



<?php if($estatisticas_financiero != null){
			if($estatisticas_financiero->total_receita != null || $estatisticas_financiero->total_despesa != null || $estatisticas_financiero->total_receita_pendente != null || $estatisticas_financiero->total_despesa_pendente != null){  ?>
	<div class="row-fluid" style="margin-top: 0">

			<div class="span4">

					<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas financieras - Procesadas</h5></div>
							<div class="widget-content">
									<div class="row-fluid">
											<div class="span12">
												<div id="chart-financeiro" style=""></div>
											</div>

									</div>
							</div>
					</div>
			</div>

			<div class="span4">

					<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas financieras - Pendentes</h5></div>
							<div class="widget-content">
									<div class="row-fluid">
											<div class="span12">
												<div id="chart-financeiro2" style=""></div>
											</div>

									</div>
							</div>
					</div>
			</div>


			<div class="span4">

					<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Total en caja / Previsto</h5></div>
							<div class="widget-content">
									<div class="row-fluid">
											<div class="span12">
												<div id="chart-financeiro-caixa" style=""></div>
											</div>

									</div>
							</div>
					</div>
			</div>

	</div>
<?php } } ?>

<?php if($os != null){ ?>
	<div class="row-fluid" style="margin-top: 0">

			<div class="span12">

					<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas de OS</h5></div>
							<div class="widget-content">
									<div class="row-fluid">
											<div class="span12">
												<div id="chart-os" style=""></div>
											</div>

									</div>
							</div>
					</div>
			</div>
	</div>
<?php } ?>


<div class="row-fluid" style="margin-top: 0">
	<div class="span12">
		<div class="widget-box">
			<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas del Sistema</h5></div>
			<div class="widget-content">
				<div class="row-fluid">
					<div class="span12">
						<ul class="site-stats">
							<li class="bg_lh"><i class="icon-group"></i> <strong><?php echo $this->db->count_all('clientes');?></strong> <small>Clientes</small></li>
							<li class="bg_lh"><i class="icon-barcode"></i> <strong><?php echo $this->db->count_all('productos');?></strong> <small>Productos </small></li>
							<li class="bg_lh"><i class="icon-tags"></i> <strong><?php echo $this->db->count_all('os');?></strong> <small>Ordenes de Compra</small></li>
							<li class="bg_lh"><i class="icon-wrench"></i> <strong><?php echo $this->db->count_all('ventas');?></strong> <small>Ventas</small></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>


<?php if($os != null) {?>
	<script >

			$(document).ready(function(){
				var data = [
					<?php foreach ($os as $o) {
							echo "['".$o->status."', ".$o->total."],";
					} ?>

				];
				var plot1 = jQuery.jqplot ('chart-os', [data],
					{
						seriesDefaults: {
							// Make this a pie chart.
							renderer: jQuery.jqplot.PieRenderer,
							rendererOptions: {
								// Put data labels on the pie slices.
								// By default, labels show the percentage of the slice.
								showDataLabels: true
							}
						},
						legend: { show:true, location: 'e' }
					}
				);

			});

	</script>

<?php } ?>



<?php
	if(isset($estatisticas_financiero) && $estatisticas_financiero != null) {
		if($estatisticas_financiero->total_receita != null || $estatisticas_financiero->total_despesa != null || $estatisticas_financiero->total_receita_pendente != null || $estatisticas_financiero->total_despesa_pendente != null){
?>
<script type="text/javascript">

		$(document).ready(function(){

			var data2 = [['Total Receitas',<?php echo ($estatisticas_financiero->total_receita != null ) ?  $estatisticas_financiero->total_receita : '0.00'; ?>],['Total Despesas', <?php echo ($estatisticas_financiero->total_despesa != null ) ?  $estatisticas_financiero->total_despesa : '0.00'; ?>]];
			var plot2 = jQuery.jqplot ('chart-financeiro', [data2],
				{

					seriesColors: [ "#9ACD32", "#FF8C00", "#EAA228", "#579575", "#839557", "#958c12","#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],
					seriesDefaults: {
						// Make this a pie chart.
						renderer: jQuery.jqplot.PieRenderer,
						rendererOptions: {
							// Put data labels on the pie slices.
							// By default, labels show the percentage of the slice.
							dataLabels: 'value',
							showDataLabels: true
						}
					},
					legend: { show:true, location: 'e' }
				}
			);


			var data3 = [['Total Receitas',<?php echo ($estatisticas_financiero->total_receita_pendente != null ) ?  $estatisticas_financiero->total_receita_pendente : '0.00'; ?>],['Total Despesas', <?php echo ($estatisticas_financiero->total_despesa_pendente != null ) ?  $estatisticas_financiero->total_despesa_pendente : '0.00'; ?>]];
			var plot3 = jQuery.jqplot ('chart-financeiro2', [data3],
				{

					seriesColors: [ "#90EE90", "#FF0000", "#EAA228", "#579575", "#839557", "#958c12","#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],
					seriesDefaults: {
						// Make this a pie chart.
						renderer: jQuery.jqplot.PieRenderer,
						rendererOptions: {
							// Put data labels on the pie slices.
							// By default, labels show the percentage of the slice.
							dataLabels: 'value',
							showDataLabels: true
						}
					},
					legend: { show:true, location: 'e' }
				}

			);


			var data4 = [['Total em Caixa',<?php echo ($estatisticas_financiero->total_receita - $estatisticas_financiero->total_despesa); ?>],['Total a Entrar', <?php echo ($estatisticas_financiero->total_receita_pendente - $estatisticas_financiero->total_despesa_pendente); ?>]];
			var plot4 = jQuery.jqplot ('chart-financeiro-caixa', [data4],
				{

					seriesColors: ["#839557","#d8b83f", "#d8b83f", "#ff5800", "#0085cc"],
					seriesDefaults: {
						// Make this a pie chart.
						renderer: jQuery.jqplot.PieRenderer,
						rendererOptions: {
							// Put data labels on the pie slices.
							// By default, labels show the percentage of the slice.
							dataLabels: 'value',
							showDataLabels: true
						}
					},
					legend: { show:true, location: 'e' }
				}

			);


		});

</script>

<?php } } ?>