<?php if(!isset($datos[0]) || $datos[0] == null) { ?>
	<div class="row-fluid" style="margin-top:0">
			<div class="span12">
					<div class="widget-box">
							<div class="widget-title">
									<span class="icon">
											<i class="icon-align-justify"></i>
									</span>
									<h5>Datos de la Empresa</h5>
							</div>
							<div class="widget-content ">
									<div class="alert alert-danger">
										No se ha registrado niguna Empresa. Estos datos seran usados en los encabezados de la facturacion y otros.
									</div>
									<a href="#modalCadastrar" data-toggle="modal" role="button" class="btn btn-success">Registrar Datos</a>
							</div>
					</div>

			</div>
	</div>

	<div id="modalCadastrar" class="modal hide fade" style="width:600px;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form id="form_Registrar" method="post" class="form-horizontal" action="" enctype="multipart/form-data" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Textil OC - Registrar Datos de la Empresa</h3>
			</div>
			<div class="modal-body">

				<div class="control-group">
						<label for="nome" class="control-label">Razòn Social<span class="required">*</span></label>
						<div class="controls">
								<input id="nombrempresa" type="text" name="nombrempresa" value=""  />
						</div>
				</div>
				<div class="control-group">
						<label for="rif" class="control-label"><span class="required">Rif*</span></label>
						<div class="controls">
								<input class="" type="text" name="rif" value=""  />
						</div>
				</div>
				<div class="control-group">
						<label for="ramo" class="control-label"><span class="required">Ramo / Dedicado a:</span></label>
						<div class="controls">
								<input type="text" name="ramo" value=""  />
						</div>
				</div>
				<div class="control-group">
						<label for="iva" class="control-label"><span class="required">Impuesto al Valor Agregado (IVA):</span></label>
						<div class="controls">
								<input type="text" name="iva" value=""  />
						</div>
				</div>
				<div class="control-group">
						<label for="direccion" class="control-label"><span class="required">Direcciòn*</span></label>
						<div class="controls">
								<input type="text" name="direccion" value=""  />
						</div>
				</div>
				<div class="control-group">
						<label for="numero" class="control-label"><span class="required">Número*</span></label>
						<div class="controls">
								<input type="text" name="numero" value=""  />
						</div>
				</div>
				<div class="control-group">
						<label for="sector" class="control-label"><span class="required">Sector*</span></label>
						<div class="controls">
								<input type="text" name="sector" value=""  />
						</div>
				</div>
				<div class="control-group">
						<label for="ciudad" class="control-label"><span class="required">Ciudad*</span></label>
						<div class="controls">
								<input type="text" name="ciudad" value=""  />
						</div>
				</div>
				<div class="control-group">
						<label for="telefono" class="control-label"><span class="required">Telefono*</span></label>
						<div class="controls">
								<input type="text" name="telefono" value=""  />
						</div>
				</div>
				<div class="control-group">
						<label for="email" class="control-label"><span class="required">E-mail*</span></label>
						<div class="controls">
								<input type="text" name="email" value="" />
						</div>
				</div>

				<div class="control-group">
						<label for="logo" class="control-label"><span class="required">Logomarca*</span></label>
						<div class="controls">
								<input type="file" name="userfile" size="20" value="" />
								<br/><br/>
						</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
				<button type="submit" dir="<?php echo base_url()?>index.php/mapos/registrarEmpresa" class="btn btn-success">Registrar</button>
			</div>
		</form>
	</div>

<?php } else { ?>

	<div class="row-fluid" style="margin-top:0">
			<div class="span12">
					<div class="widget-box">
							<?php 	echo $this->session->flashdata('mensaje');?>
							<div class="widget-title">
									<span class="icon">
											<i class="icon-align-justify"></i>
									</span>
									<h5>Datos de la Empresa</h5>
							</div>
							<div class="widget-content ">
							<div class="alert alert-info">Los datos mostrados acà seran utilizados en los encabezados de la facturaciòn.</div>
								<table class="table table-bordered">
									<tbody>
										<tr>
											<td style="width: 25%">
												<img src="<?php echo $datos[0]->url_logo?>">
											</td>
											<td>
												<span style="font-size: 20px; "> <?php echo $datos[0]->nombrempresa; ?> </span> </br>
												<span>
													<?php echo $datos[0]->rif; ?> </br>
													<?php echo $datos[0]->direccion.', nº:'.$datos[0]->numero.', '.$datos[0]->sector.' - '.$datos[0]->ciudad; ?>
												</span> </br>
												<span>
													E-mail: <?php echo $datos[0]->email.' - Tlfno: '.$datos[0]->telefonoemp; ?>
												</span>
											</td>
										</tr>
									</tbody>
								</table>

									<a href="#modalAlterar" data-toggle="modal" role="button" class="btn btn-primary">Actualizar Datos</a>
									<a href="#modalLogo" data-toggle="modal" role="button" class="btn btn-inverse">Actualizar Logo</a>
							</div>
					</div>
			</div>
	</div>


	<div id="modalAlterar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form id="form_Actualizar" method="post" class="form-horizontal" action="" enctype="multipart/form-data" >

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="">Textil OC - Editar Datos de la Empresa</h3>
			</div>
			<div class="modal-body">

					<div class="control-group">
							<label for="nombrempresa" class="control-label">Razòn Social<span class="required">*</span></label>
							<div class="controls">
									<input id="nombrempresa" type="text" name="nombrempresa" value="<?php echo $datos[0]->nombrempresa; ?>"  />
									<input id="nombrempresa" type="hidden" name="id" value="<?php echo $datos[0]->id; ?>"  />
							</div>
					</div>
					<div class="control-group">
							<label for="rif" class="control-label"><span class="required">Rif*</span></label>
							<div class="controls">
									<input class="" type="text" name="rif" value="<?php echo $datos[0]->rif; ?>"  />
							</div>
					</div>
					<div class="control-group">
							<label for="ramo" class="control-label"><span class="required">Ramo / Dedicado a:*</span></label>
							<div class="controls">
									<input type="text" name="ramo" value="<?php echo $datos[0]->ramo; ?>"  />
							</div>
					</div>
					<div class="control-group">
							<label for="iva" class="control-label"><span class="required">Impuesto al Valor Agregado (IVA):</span></label>
							<div class="controls">
									<input type="text" name="iva" value="<?php echo $datos[0]->iva; ?>" />
							</div>
					</div>
					<div class="control-group">
							<label for="direccion" class="control-label"><span class="required">Direcciòn*</span></label>
							<div class="controls">
									<input type="text" name="direccion" value="<?php echo $datos[0]->direccion; ?>"  />
							</div>
					</div>
					<div class="control-group">
							<label for="numero" class="control-label"><span class="required">Número*</span></label>
							<div class="controls">
									<input type="text" name="numero" value="<?php echo $datos[0]->numero; ?>"  />
							</div>
					</div>
					<div class="control-group">
							<label for="sector" class="control-label"><span class="required">Sector*</span></label>
							<div class="controls">
									<input type="text" name="sector" value="<?php echo $datos[0]->sector; ?>"  />
							</div>
					</div>
					<div class="control-group">
							<label for="ciudad" class="control-label"><span class="required">Ciudad*</span></label>
							<div class="controls">
									<input type="text" name="ciudad" value="<?php echo $datos[0]->ciudad; ?>"  />
							</div>
					</div>
					<div class="control-group">
							<label for="telefono" class="control-label"><span class="required">Telefono*</span></label>
							<div class="controls">
									<input type="text" name="telefono" value="<?php echo $datos[0]->telefonoemp; ?>"  />
							</div>
					</div>
					<div class="control-group">
							<label for="email" class="control-label"><span class="required">E-mail*</span></label>
							<div class="controls">
									<input type="text" name="email" value="<?php echo $datos[0]->email; ?>" />
							</div>
					</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
				<button type="submit" dir="<?php echo base_url()?>index.php/mapos/editarEmpresa" class="btn btn-primary">Actualizar</button>
			</div>
		</form>
	</div>


	<div id="modalLogo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form id="formLogo" method="post" class="form-horizontal" action="" enctype="multipart/form-data" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="">Textil OC - Actualizar Logomarca</h3>
			</div>
			<div class="modal-body">
				<div class="span12 alert alert-info">Selecione una nueva imagen de logomarca. Tamaño indicado (130px X 130px).</div>
				<div class="control-group">
					<label for="logo" class="control-label"><span class="required">Logomarca*</span></label>
					<div class="controls">
						<input type="file" name="userfile" />
						<input type="hidden" name="id" value="<?php echo $datos[0]->id; ?>"  />
						<br/><br/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
				<button type="submit" dir="<?php echo base_url()?>index.php/mapos/editarLogo" class="btn btn-primary">Actualizar</button>
			</div>
		</form>
	</div>

<?php } ?>

<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script>
	$(document).ready(function(){

		$("#formLogo").validate({
				rules:{
					 userfile: {required:true}
				},
				messages:{
					 userfile: {required: 'Campo Requerido.'}
				},

					errorClass: "help-inline",
					errorElement: "span",
					highlight:function(element, errorClass, validClass) {
							$(element).parents('.control-group').addClass('error');
							$(element).parents('.control-group').removeClass('success');
					},
					unhighlight: function(element, errorClass, validClass) {
							$(element).parents('.control-group').removeClass('error');
							$(element).parents('.control-group').addClass('success');
					}
		});


		$("#form_Registrar").validate({
				rules:{
					 nombrempresa: {required:true},
					 rif: {required:true},
					 ramo: {required:true},
					 iva: {required:true},
					 direccion: {required:true},
					 numero: {required:true},
					 sector: {required:true},
					 ciudad: {required:true},
					 telefono: {required:true},
					 email: {required:true}
				},
				messages:{
					 nombrempresa: {required: 'Campo Requerido.'},
					 rif: {required: 'Campo Requerido.'},
					 ramo: {required: 'Campo Requerido.'},
					 iva: {required: 'Campo Requerido.'},
					 direccion: {required: 'Campo Requerido.'},
					 numero: {required:'Campo Requerido.'},
					 sector: {required:'Campo Requerido.'},
					 ciudad: {required:'Campo Requerido.'},
					 telefono: {required:'Campo Requerido.'},
					 email: {required:'Campo Requerido.'}
				},

					errorClass: "help-inline",
					errorElement: "span",
					highlight:function(element, errorClass, validClass) {
							$(element).parents('.control-group').addClass('error');
							$(element).parents('.control-group').removeClass('success');
					},
					unhighlight: function(element, errorClass, validClass) {
							$(element).parents('.control-group').removeClass('error');
							$(element).parents('.control-group').addClass('success');
					}
		});


		$("#form_Actualizar").validate({
				rules:{
					 nombrempresa: {required:true},
					 rif: {required:true},
					 ramo: {required:true},
					 iva: {required:true},
					 direccion: {required:true},
					 numero: {required:true},
					 sector: {required:true},
					 ciudad: {required:true},
					 telefono: {required:true},
					 email: {required:true}
				},
				messages:{
					 nombrempresa: {required: 'Campo Requerido.'},
					 rif: {required: 'Campo Requerido.'},
					 ramo: {required: 'Campo Requerido.'},
					 iva: {required: 'Campo Requerido.'},
					 direccion: {required: 'Campo Requerido.'},
					 numero: {required:'Campo Requerido.'},
					 sector: {required:'Campo Requerido.'},
					 ciudad: {required:'Campo Requerido.'},
					 telefono: {required:'Campo Requerido.'},
					 email: {required:'Campo Requerido.'}
				},

					errorClass: "help-inline",
					errorElement: "span",
					highlight:function(element, errorClass, validClass) {
							$(element).parents('.control-group').addClass('error');
							$(element).parents('.control-group').removeClass('success');
					},
					unhighlight: function(element, errorClass, validClass) {
							$(element).parents('.control-group').removeClass('error');
							$(element).parents('.control-group').addClass('success');
					}
		});
	});
</script>
<script>
	// para enviar el formulario segun el boton que al que se le haga clic
	$("button[type=submit]").click(function() {
		var accion = $(this).attr('dir');
			$("#ventanaEnviando").delay(200).fadeOut(300, function(){
				$(".ct").append("Espere, por favor..!");
			});
			$('form').attr('action', accion);
			$('form').submit();
	});
</script>