<!DOCTYPE html>
<html lang="es">

<head>
	<title>Textil OC</title><meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-responsive.min.css" />
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/matrix-login.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
	<script src="<?php echo base_url()?>assets/js/jquery-1.10.2.min.js"></script>
</head>
<body>
	<div id="loginbox">
		<form  class="form-vertical" id="formLogin" method="post" action="<?php echo base_url()?>index.php/mapos/verificarLogin">
			<?php if($this->session->flashdata('error') != null){?>
				<div id="aviso" class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<?php echo $this->session->flashdata('error');?>
				 </div>
				 <script>$("#aviso").delay(1500).fadeOut(600);  </script>
			<?php }?>
			<div class="control-group normal_text"> <h3><img src="<?php echo base_url()?>assets/img/logo.png" alt="Logo" /></h3></div>
			<div class="control-group">
				<div class="controls">
					<div class="main_input_box">
						<span class="add-on bg_lg"><i class="icon-user"></i></span>
						<input id="email" name="email" type="text" placeholder="Email" /><br>
						<?php echo form_error('email', '<span class="error">', '</span>'); ?>
					</div>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<div class="main_input_box">
						<span class="add-on bg_ly"><i class="icon-lock"></i></span>
						<input name="clave" type="password" placeholder="Clave" /><br>
						<?php echo form_error('clave', '<span class="error">', '</span>'); ?>
					</div>
				</div>
			</div>
			<div class="form-actions" style="text-align: center">
				<button type="submit"  class="btn btn-info btn-large"/> Login</button>
			</div>
		</form>
	</div>
	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/validate.js"></script>
</body>
</html>