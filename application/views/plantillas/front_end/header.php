<!DOCTYPE html>
<html lang="es-ES">
<head>
<title>Textil OC</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
<link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel='stylesheet' type='text/css'/>
<link href="<?php echo base_url();?>assets/css/matrix-style.css" rel='stylesheet' type='text/css'/>
<link href="<?php echo base_url();?>assets/css/matrix-media.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel='stylesheet' type='text/css'/>
<link href="<?php echo base_url();?>assets/css/fullcalendar.css" rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<script src="<?php echo base_url();?>assets/js/jQuery-2.1.4.js"></script>
</head>
<body>

<!--Header-part-->
<div id="header">
	<h1><a href="">Textil OC</a></h1>
</div>
<!--close-Header-part-->

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
	<ul class="nav">
		<li class=""><a title="" href="<?php echo base_url();?>index.php/mapos/miCuenta"><i class="icon icon-star"></i> <span class="text">Mi Cuenta</span></a></li>
		<li class=""><a title="" href="<?php echo base_url();?>index.php/mapos/salir"><i class="icon icon-share-alt"></i> <span class="text">Salir del Sistema</span></a></li>
	</ul>
</div>

<!--start-top-serch-->
<div id="search">
	<form action="<?php echo base_url()?>index.php/mapos/buscar">
		<input type="text" name="termo" placeholder="Buscar..."/>
		<button type="submit"  class="tip-bottom" title="Buscar"><i class="icon-search icon-white"></i></button>
	</form>
</div>
<!--close-top-serch-->

<!--sidebar-menu-->

<div id="sidebar"> <a href="#" class="visible-phone"><i class="icon icon-list"></i> Menu</a>
	<ul>
		<li class="<?php if(isset($menuPanel)){echo 'active';};?>">
			<a href="<?php echo base_url()?>"><i class="icon icon-home"></i>
			<span>Dashboard</span></a>
		</li>

		<?php
			$actC = '';
			$actP = '';
			$actV = '';
			$actO = '';
			$actS = '';
			$actK = '';
			$actR = '';
			if(isset($menuClientes)) {
				$actC = 'active';
			} elseif (isset($menuProductos)) {
				$actP = 'active';
			} elseif (isset($menuVentas)) {
				$actV = 'active';
			} elseif (isset($menuOs)) {
				$actO = 'active';
			} elseif (isset($menuServicios)) {
				$actS = 'active';
			} elseif (isset($menuConfiguraciones)){
				$actK = 'active open';
			} elseif (isset($menuProveedores)){
				$actR = 'active';
			}

			if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vCliente')) {
				echo '<li class="'.$actC.'"><a href="'.base_url().'index.php/clientes"><i class="icon icon-group"></i> <span>Clientes</span></a></li>';
			}

			if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vProduto')) {
				echo '<li class="'.$actP.'"><a href="'.base_url().'index.php/productos"><i class="icon icon-barcode"></i> <span>Productos</span></a></li>';
			}

			if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vVenda')) {
				echo '<li class="'.$actV.'"><a href="'.base_url().'index.php/servicios"><i class="icon icon-shopping-cart"></i> <span>Ventas</span></a></li>';
			}

			/*if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vOs')) {
				echo '<li class="'.$actO.'"><a href="'.base_url().'"><i class="icon icon-tags"></i> <span>Ordenes de Compra</span></a></li>';
			}*/

			if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vProduto')) {
				echo '<li class="'.$actO.'"><a href="'.base_url().'index.php/compras"><i class="icon icon-plus-sign"></i> <span>Compras</span></a></li>';
			}
		?>

		<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'rCliente') || $this->permission->checkPermission($this->session->userdata('permisos_id'),'rProduto') || $this->permission->checkPermission($this->session->userdata('permisos_id'),'rServico') || $this->permission->checkPermission($this->session->userdata('permisos_id'),'rOs') || $this->permission->checkPermission($this->session->userdata('permisos_id'),'rFinanceiro') || $this->permission->checkPermission($this->session->userdata('permisos_id'),'rVenda')){ ?>

				<li class="submenu <?php if(isset($menuReportes)){echo 'active open';};?>" >
					<a href="#"><i class="icon icon-list-alt"></i> <span>Reportes</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
					<ul>

						<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'rCliente')){ ?>
								<li><a href="<?php echo base_url()?>index.php/reportes/clientes">Clientes</a></li>
						<?php } ?>
						<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'rProduto')){ ?>
								<li><a href="<?php echo base_url()?>index.php/reportes/productos">Productos</a></li>
						<?php } ?>
						<?php //if($this->permission->checkPermission($this->session->userdata('permisos_id'),'rServico')){ ?>
								<!--<li><a href="<?php //echo base_url()?>index.php/servicios">Facturas</a></li>-->
						<?php //} ?>
						<?php //if($this->permission->checkPermission($this->session->userdata('permisos_id'),'rOs')){ ?>
								 <!--<li><a href="<?php //echo base_url()?>index.php/reportes/os">Ordenes de Compra</a></li>-->
						<?php //} ?>
						<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'rVenda')){ ?>
								<li><a href="<?php echo base_url()?>index.php/reportes/ventas">Ventas</a></li>
						<?php } ?>
					</ul>
				</li>

		<?php } ?>

		<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'cUsuario')  || $this->permission->checkPermission($this->session->userdata('permisos_id'),'cEmitente') || $this->permission->checkPermission($this->session->userdata('permisos_id'),'cpermisos_id') || $this->permission->checkPermission($this->session->userdata('permisos_id'),'cBackup')){ ?>
				<li class="submenu <?php echo $actK; ?>">
					<a href="#"><i class="icon icon-cog"></i> <span>Configuraciones</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
					<ul>
						<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'cUsuario')){ ?>
								<li><a href="<?php echo base_url()?>index.php/usuarios">Usuarios</a></li>
						<?php } ?>
						<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'cEmitente')){ ?>
								<li><a href="<?php echo base_url()?>index.php/mapos/empresa">Empresa</a></li>
						<?php } ?>
						<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'cEmitente')){ ?>
								<li><a href="<?php echo base_url()?>index.php/proveedores">Proveedores</a></li>
						<?php } ?>
						<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'cpermisos_id')){ ?>
								<li><a href="<?php echo base_url()?>index.php/permissoes">Permissões</a></li>
						<?php } ?>
						<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'cBackup')){ ?>
								<li><a href="<?php echo base_url()?>index.php/mapos/backup">Backup</a></li>
						<?php } ?>

					</ul>
				</li>
		<?php } ?>


	</ul>
</div>
<div id="content">
	<div id="content-header">
		<div id="breadcrumb">
			<a href="<?php echo base_url()?>" title="Dashboard" class="tip-bottom">
				<i class="icon-home"></i> Dashboard
			</a>
			<?php
				if($this->uri->segment(1) != null){
					echo '
					<a href="'.base_url().'index.php/'.$this->uri->segment(1).'" class="tip-bottom" title="'.ucfirst($this->uri->segment(1)).'">'.ucfirst($this->uri->segment(1)).'
					</a>
					';
				}
			?>
			<?php
				if($this->uri->segment(2) != null){
					echo '
					<a href="'.base_url().'index.php/'.$this->uri->segment(1).'/'.$this->uri->segment(2).' class="current tip-bottom" title="'.ucfirst($this->uri->segment(2)).'">'.ucfirst($this->uri->segment(2)).'
					</a>
					';
				}
			?>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12">
				<?php
					if($this->session->flashdata('error') != null){
						echo '
							<div id="saveAlert" class="alert alert-success hide" >
								<a class="close" data-dismiss="alert" href="#">&times;</a>
							</div>
						';
					} elseif ($this->session->flashdata('success')) {
						echo '
							<div id="saveAlert" class="alert alert-success" >
								<a class="close" data-dismiss="alert" href="#">&times;</a>
								<p>'. $this->session->flashdata('success').'</p>
							</div>
						';
					} elseif ($this->session->flashdata('error')) {
						echo '
							<div id="saveAlert" class="alert alert-danger" >
								<a class="close" data-dismiss="alert" href="#">&times;</a>
								<p>'. $this->session->flashdata('error').'</p>
							</div>
						';
					}
				?>

