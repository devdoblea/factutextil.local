<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicios extends CI_Controller {

	public function __construct() {
		parent::__construct();
		 $this->load->model('Servicios_model');
		 $this->load->model('Ventas_model');
		 $this->encryption->initialize(array('driver' => 'openssl'));
		/* Se carga la libreria fpdf q se carga en repo tambien pero
		   se carga aqui para q se use cuando se imprima la orden de servicio directo desde su creacion*/
		$this->load->library('pdf');
		$this->load->library('invoice');
	}

	//con esta función validamos y protegemos el buscador
	public function validar() {
		/*if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
			redirect('main/login');
		}*/
		$this->form_validation->set_rules('buscando', 'buscador', 'required|min_length[2]|max_length[20]|trim|xss_clean');
		$this->form_validation->set_message('required', 'El %s no puede ir vacío!');
		$this->form_validation->set_message('min_length', 'El %s debe tener al menos %s carácteres');
		$this->form_validation->set_message('max_length', 'El %s no puede tener más de %s carácteres');
		if ($this->form_validation->run() == TRUE) {

			$buscador = $this->input->post('buscando');
			$this->session->set_userdata('buscando', $buscador);
			redirect('servicios/index');
		} else {

			//cargamos la vista y el array data
			$this->session->unset_userdata('buscando');
			redirect('servicios/index');
		}
	}

	function index(){

		$this->gerenciar();
	}

	function gerenciar(){
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'vVenda')){
			 $this->session->set_flashdata('error','Ud. No tiene permiso para visualizar Ventas.');
			 redirect(base_url());
		}

		$por_pagina = 10; //Número de registros mostrados por páginas
		$config['base_url'] = base_url() . 'servicios/index'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
		$config['total_rows'] = $this->Servicios_model->count('os'); //calcula el número de filas
		$config['per_page'] = $por_pagina; //Número de registros mostrados por páginas
		$config["uri_segment"] = 3;
		$config['num_links'] = 5; //Número de links mostrados en la paginación
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = 'Primera';
		$config['prev_tag_open'] = '<li class="paginate_button previous">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Siguiente';
		$config['next_tag_open'] = '<li class="paginate_button next">';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="paginate_button active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="paginate_button ">';
		$config['num_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="paginate_button">';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="paginate_button">';
		$config['last_tag_close'] = '</li>';
		$config['first_link'] = 'Inicio';
		$config['last_link'] = 'Ultima';

		$this->pagination->initialize($config);
		//el array con los datos a paginar ya preparados
		$datos["results"] = $this->Servicios_model->get('ventas','*','',$config['per_page'],$this->uri->segment(3));

		/*$sections = array(
			'config'  => TRUE,
			'queries' => TRUE,
			'controller_info' => TRUE,
			'uri_strig' => TRUE,
			'session_data' => TRUE

			);
		$this->output->set_profiler_sections($sections);
		$this->output->enable_profiler(TRUE);*/

		// cargamos la vista principal
		$datos['menuServicios'] = 'Servicios';
		$this->load->view('plantillas/front_end/header',$datos);
		$this->load->view('servicios/ventas',$datos);
		$this->load->view('plantillas/front_end/footer');
	}

	//con esta función registramos estudiantes
	function adicionar(){

			if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'aVenda')){
				$this->session->set_flashdata('error','Ud. No tiene permiso para realizar Ventas.');
				redirect(base_url());
			}

			$data['custom_error'] = '';
			$nrotemp = $this->Servicios_model->nro_temporal();
			$data['nrotemp'] = $nrotemp;
			$this->load->view('plantillas/front_end/header', $data);
			$this->load->view('servicios/adicionarVenta', $data);
			$this->load->view('plantillas/front_end/footer');
	}

	// para buscar los datos del cliente
	public function autoCompleteCliente(){
		$nome = $this->input->get('term',TRUE);
		if ($nome){
			$valores = $this->Servicios_model->consultar_cliente($nome);
			return $valores;
		}
	}

	// para buscar servicios o material en el tabulador
	public function autoCompleteProducto(){
		$descrip = $this->input->get('term',TRUE);
		if ($descrip){
			$valores = $this->Servicios_model->consultar_prod($descrip);
			return $valores;
		}
	}
	// listar los articulos creados
	public function listar_articulos() {
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'vVenda')){
			 $this->session->set_flashdata('error','Ud. No tiene permiso para agregar Productos.');
			 redirect(base_url());
		}

		$nrotemp = $this->input->post('idtmp');
		$datos['resultados'] = $this->Servicios_model->listar_articulos($nrotemp);
		$this->load->view('servicios/listar_articulos', $datos);
	}

	// para registrar los articulos de forma temporal
	public function agregar_art() {

		$idtmp = $this->input->post('idtmp');
		$codpr = $this->input->post('codpr');
		$cantp = $this->input->post('cantp');
		$prcio = $this->input->post('prcio');
		$unidd = $this->input->post('unidd');


		//conseguimos la hora de nuestro país,
		date_default_timezone_set("America/New_York");
		$fec = date('Y-m-d');
		$ope = $this->session->userdata('usuario');

		// inserta los datos arreglados
		$insert  = $this->Servicios_model->agregar_art($idtmp,$codpr,$cantp,$prcio,$unidd,$fec,$ope);

		//$this->output->enable_profiler(TRUE);

		if ($insert == 'error') {

			$this->session->set_flashdata('mensaje', '
				<div id="saveAlert" class="alert alert-danger" >
					<a class="close" data-dismiss="alert" href="#">&times;</a>
					<p>No se puede Grabar el dato..!</p>
				</div>
				<script> $("#saveAlert").delay(2000).fadeOut(600);</script>
			');
			redirect(base_url().'index.php/servicios/adicionar');

		} elseif ($insert == 'correcto') {

			$this->session->set_flashdata('mensaje', '
				<div id="saveAlert" class="alert alert-success" >
					<a class="close" data-dismiss="alert" href="#">&times;</a>
					<p>El registro se hizo correctamente!</p>
				</div>
				<script> $("#saveAlert").delay(2000).fadeOut(600);</script>
			');
			redirect(base_url().'index.php/servicios/adicionar');
		}
	}

	public function pregunta_borrar(){

		$this->load->view('compras/borrararticulo');
	}

	// para borrar articulo por articulo de la orden
	public function borrar_articulo(){

		// recibo el post
		$idart = $this->input->post('idart');

		$result = $this->Servicios_model->borra_art($idart);

			if( $result == false) {
				$this->session->set_flashdata('mensaje', '
					<div id="saveAlert" class="alert alert-danger" >
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<p>No se puede borrar el registro..!</p>
					</div>
					<script> $("#saveAlert").delay(2000).fadeOut(600);</script>
				');
				redirect(base_url().'index.php/servicios/adicionar');

			} else {

				$this->session->set_flashdata('mensaje', '
					<div id="saveAlert" class="alert alert-success" >
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<p>Se borró correctamente el registro!</p>
					</div>
					<script> $("#saveAlert").delay(2000).fadeOut(600);</script>
				');
				redirect(base_url().'index.php/servicios/adicionar');
			}
	}

	// imprimir factura
	public function imprime_fact($nrotemp) {

		// debo buscar la manera de abrir este reporte en otra pestaña
		$data['nrotemp'] = $nrotemp;
		$direccion = 'reportes/rpt_factura_pdf';
		$this->load->view($direccion,$data);
	}

	// para crear la orden de servicio
	public function insertar_servicio() {

		// recibo las variables por post
		$nrot = $this->input->post('nrotemp');
		$ncli = $this->input->post('cliente');
		$ccli = $this->input->post('clientes_id');
		$fven = $this->input->post('fechaVenta');
		$tlfn = $this->input->post('tlfno');
		$celu = $this->input->post('celu');
		$accion = $this->input->post('registrar');

		//conseguimos la hora de nuestro país,
		date_default_timezone_set("America/New_York");
		$fec = date('Y-m-d');
		$ope = $this->session->userdata('usuario');

		// defino si es una cancelacion de la orden o una insercion en la bd
		if($accion == 'Registrar Datos') {

			$insertar = $this->Servicios_model->insertaRegistro($nrot,$ncli,$ccli,$tlfn,$celu,$fven);

			if ($insertar == 'error') {

				$this->session->set_flashdata('mensaje', '
					<div id="saveAlert" class="alert alert-danger" >
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<p>No se puede Grabar la Factura..!</p>
					</div>
					<script> $("#saveAlert").delay(2000).fadeOut(600);</script>
				');
				redirect(base_url().'index.php/servicios');

			 } elseif ($insertar == 'correcto') {

				$this->session->set_flashdata('mensaje', '
					<div id="saveAlert" class="alert alert-success" >
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<p>El registro se hizo correctamente!</p>
					</div>
					<script> $("#saveAlert").delay(2000).fadeOut(600);</script>
				');
				// redirijo a la pagina de ventas luego de generar la factura en pdf
				//$this->imprime_fact($nrot);
				// y luego que vaya a la pagina principal de ventas
				redirect(base_url().'index.php/servicios');

			}
		}
	}

	// para cancelar la orden de servicio
	public function cancelar_servicio(){

		// recibo las variables por post
		$nrot = $this->input->post('nrotemp');
		$accion = $this->input->post('cancelar');

		if ($accion == 'Cancelar Orden') {

			$cancelar = $this->Servicios_model->cancelarOrden($nrot);

			if ($cancelar == 'error') {

				$this->session->set_flashdata('mensaje', '
					<div id="saveAlert" class="alert alert-danger" >
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<p>No se puede Cancelar esta Orden..!</p>
					</div>
					<script> $("#saveAlert").delay(2000).fadeOut(600);</script>
				');
				redirect(base_url().'index.php/servicios');

			} elseif ($cancelar == 'correcto') {

				$this->session->set_flashdata('mensaje', '
					<div id="saveAlert" class="alert alert-warning" >
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<p>Se canceló la orden de Servicio!</p>
					</div>
					<script> $("#saveAlert").delay(2000).fadeOut(600);</script>
				');
				redirect(base_url().'index.php/servicios');
			}
		}
	}

	function editar() {
			if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'eVenda')){
				$this->session->set_flashdata('error','Ud. no tiene permiso para editar ventas');
				redirect(base_url());
			}

			$this->load->library('form_validation');
			$this->data['custom_error'] = '';

			if ($this->form_validation->run('vendas') == false) {
					$this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
			} else {

					$fechaVenta = $this->input->post('fechaVenta');

					try {

							$fechaVenta = explode('/', $fechaVenta);
							$fechaVenta = $fechaVenta[2].'-'.$fechaVenta[1].'-'.$fechaVenta[0];


					} catch (Exception $e) {
						 $fechaVenta = date('Y-m-d');
					}

					$data = array(
							'fechaVenta' => $fechaVenta,
							'usuarios_id' => $this->input->post('usuarios_id'),
							'clientes_id' => $this->input->post('clientes_id')
					);

					if ($this->Ventas_model->edit('ventas', $data, 'idVentas', $this->input->post('idVentas')) == TRUE) {
							$this->session->set_flashdata('success','Venta editada con exito!');
							redirect(base_url() . 'index.php/ventas/editar/'.$this->input->post('idVentas'));
					} else {
							$this->data['custom_error'] = '<div class="form_error"><p> Ocurrio un error </p></div>';
					}
			}

			$this->data['result'] = $this->Ventas_model->getById($this->uri->segment(3));
			$this->data['productos'] = $this->Ventas_model->getProductos($this->uri->segment(3));
			$this->data['view'] = 'ventas/editarVenta';
			$this->load->view('tema/topo', $this->data);
	}
}