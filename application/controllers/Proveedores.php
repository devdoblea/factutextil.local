<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proveedores extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
			redirect('mapos/login');
		}

		$this->load->model('Proveedores_model','',TRUE);
	}

	function index(){
		$this->gerenciar();
	}

	function gerenciar(){
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'vCliente')){
			 $this->session->set_flashdata('error','Ud. No tiene permiso para visualizar Proveedores.');
				 redirect(base_url());
		}

		$config['base_url'] = base_url().'index.php/proveedores/gerenciar/';
		$config['total_rows'] = $this->Proveedores_model->count('proveedores');
		$config['per_page'] = 10;
		$config['next_link'] = 'Próxima';
		$config['prev_link'] = 'Anterior';
		$config['full_tag_open'] = '<div class="pagination alternate"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
		$config['cur_tag_close'] = '</b></a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['first_link'] = 'Primeira';
		$config['last_link'] = 'Última';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$data['results'] = $this->Proveedores_model->get('proveedores','idProv,nomProv,documento,telefono,celular,email,calle,numero,barrio,ciudad,estado','',$config['per_page'],$this->uri->segment(3));

		$data['menuProveedores'] = 'Proveedores';
		$this->load->view('plantillas/front_end/header',$data);
		$this->load->view('proveedores/proveedores',$data);
		$this->load->view('plantillas/front_end/footer');
	}

	function adicionar() {
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'aCliente')){
			 $this->session->set_flashdata('error','Ud. No tiene permiso para agregar Proveedores.');
				 redirect(base_url());
		}


		$data['custom_error'] = '';
		$this->form_validation->set_rules('nomProv', 'Nombre Proveedor', 'required|trim|xss_clean');
		$this->form_validation->set_rules('documento', 'documento', 'required|trim|xss_clean');
		$this->form_validation->set_rules('celular', 'celular', 'required|trim|xss_clean');
		$this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean');
		$this->form_validation->set_rules('calle', 'calle', 'required|trim|xss_clean');
		$this->form_validation->set_rules('numero', 'numero', 'required|trim|xss_clean');
		$this->form_validation->set_rules('barrio', 'barrio', 'required|trim|xss_clean');
		$this->form_validation->set_rules('estado', 'ciudad', 'required|trim|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$this->data['custom_error'] = validation_errors();
			$this->load->view('plantillas/front_end/header');
			$this->load->view('proveedores/adicionarProveedor',  $data);
			$this->load->view('plantillas/front_end/footer');

		} else {
			$data = array(
				'nomProv' => set_value('nomProv'),
				'documento' => set_value('documento'),
				'telefono' => set_value('telefono'),
				'celular' => $this->input->post('celular'),
				'email' => set_value('email'),
				'calle' => set_value('calle'),
				'numero' => set_value('numero'),
				'barrio' => set_value('barrio'),
				'ciudad' => set_value('ciudad'),
				'estado' => set_value('estado'),
				//'cep' => set_value('cep'),
				'fechacli' => date('Y-m-d')
			);

			if ($this->Proveedores_model->add('proveedores', $data) == TRUE) {
				$this->session->set_flashdata('success','Proveedor adicionado con exito!');
				redirect(base_url() . 'index.php/proveedores');
			} else {
				$this->data['custom_error'] = '<div class="form_error"><p>Ocurrio un error.</p></div>';
				redirect(base_url() . 'index.php/proveedores/adicionar');
			}
		}
	}

	function editar() {
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'eCliente')){
			$this->session->set_flashdata('error','Ud. No tiene permiso para editar Proveedores.');
			redirect(base_url());
		}
		//$this->load->library('form_validation');
		$this->data['custom_error'] = '';
		$this->form_validation->set_rules('nomProv', 'Nombre Proveedor', 'required|trim|xss_clean');
		$this->form_validation->set_rules('documento', 'documento', 'required|trim|xss_clean');
		$this->form_validation->set_rules('celular', 'celular', 'required|trim|xss_clean');
		$this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean');
		$this->form_validation->set_rules('calle', 'calle', 'required|trim|xss_clean');
		$this->form_validation->set_rules('numero', 'numero', 'required|trim|xss_clean');
		$this->form_validation->set_rules('barrio', 'barrio', 'required|trim|xss_clean');
		$this->form_validation->set_rules('estado', 'ciudad', 'required|trim|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$this->data['custom_error'] = validation_errors();
			$this->data['result'] = $this->Proveedores_model->getById($this->uri->segment(3));
			$this->load->view('plantillas/front_end/header');
			$this->load->view('proveedores/editarProveedor',  $this->data);
			$this->load->view('plantillas/front_end/footer');
		} else {
			$data = array(
				'nomProv' => $this->input->post('nomProv'),
				'documento' => $this->input->post('documento'),
				'telefono' => $this->input->post('telefono'),
				'celular' => $this->input->post('celular'),
				'email' => $this->input->post('email'),
				'calle' => $this->input->post('calle'),
				'numero' => $this->input->post('numero'),
				'barrio' => $this->input->post('barrio'),
				'ciudad' => $this->input->post('ciudad'),
				'estado' => $this->input->post('estado')
				//'cep' => $this->input->post('cep')
			);

			if ($this->Proveedores_model->edit('proveedores', $data, 'idProv', $this->input->post('idProv')) == TRUE) {
				$this->session->set_flashdata('success','Proveedor editado con Exito!');
				redirect(base_url() . 'index.php/proveedores');
			} else {
				$this->data['custom_error'] = '<div class="form_error"><p>Ocurrio un error</p></div>';
				$this->data['result'] = $this->Proveedores_model->getById($this->uri->segment(3));
				redirect(base_url() . 'index.php/proveedores/editar');
			}
		}

	}

	public function visualizar(){
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'vCliente')){
			$this->session->set_flashdata('error','Ud no tiene permiso para visualizar Proveedores.');
			redirect(base_url());
		}

		$this->data['custom_error'] = '';
		$this->data['result'] = $this->Proveedores_model->getById($this->uri->segment(3));
		$this->data['results'] = $this->Proveedores_model->getOsByProv($this->uri->segment(3));
		$this->load->view('plantillas/front_end/header');
		$this->load->view('proveedores/visualizar',  $this->data);
		$this->load->view('plantillas/front_end/footer');

	}

	public function excluir(){

					if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dCliente')){
						 $this->session->set_flashdata('error','Você não tem permissão para excluir Proveedores.');
						 redirect(base_url());
					}


					$this->Proveedores_model->delete('proveedores','idProv',$id);

					$this->session->set_flashdata('success','Proveedor excluido com sucesso!');
					redirect(base_url().'index.php/proveedores/gerenciar/');
	}
}

