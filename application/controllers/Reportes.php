<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends CI_Controller{
    public function __construct() {
        parent::__construct();
        if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
            redirect('mapos/login');
        }

        $this->load->model('Reportes_model','',TRUE);
        $this->load->model('Servicios_model','',TRUE);
        $this->load->library('invoice');
        $this->data['menuReportes'] = 'Reportes';

    }

    public function clientes(){

        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rCliente')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de clientes.');
           redirect(base_url());
        }
        //$this->data['view'] = 'reportes/rel_clientes';
       	//$this->load->view('tema/topo',$this->data);
        $this->load->view('plantillas/front_end/header');
        $this->load->view('reportes/rel_clientes');
        $this->load->view('plantillas/front_end/footer');
    }

    public function productos(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rProduto')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de de productos.');
           redirect(base_url());
        }
        //$this->data['view'] = 'reportes/rel_produtos';
       	//$this->load->view('tema/topo',$this->data);
        $this->load->view('plantillas/front_end/header');
        $this->load->view('reportes/rel_productos');
        $this->load->view('plantillas/front_end/footer');

    }

    public function clientesCustom(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rCliente')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de clientes.');
           redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');

        $data['clientes'] = $this->Reportes_model->clientesCustom($dataInicial,$dataFinal);

        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirClientes', $data);
        $html = $this->load->view('reportes/imprimir/imprimirClientes', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_clientes' . date('d-m-y'), TRUE);

    }

    public function clientesRapid(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rCliente')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de clientes.');
           redirect(base_url());
        }

        $data['clientes'] = $this->Reportes_model->clientesRapid();

        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirClientes', $data);
        $html = $this->load->view('reportes/imprimir/imprimirClientes', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_clientes' . date('d-m-y'), TRUE);
    }

    public function productosRapid(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rProduto')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de productos.');
           redirect(base_url());
        }

        $data['productos'] = $this->Reportes_model->produtosRapid();

        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirProdutos', $data);
        $html = $this->load->view('reportes/imprimir/imprimirProductos', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_productos' . date('d-m-y'), TRUE);
    }

    public function productosRapidMin(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rProduto')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de produtos.');
           redirect(base_url());
        }

        $data['productos'] = $this->Reportes_model->produtosRapidMin();

        $this->load->library('mpdf');
        $html = $this->load->view('reportes/imprimir/imprimirProductos', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_productos' . date('d-m-y'), TRUE);

    }

    public function productosCustom(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rProduto')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de produtos.');
           redirect(base_url());
        }

        $precoInicial   = str_replace(array('.',','),array('','.'),$this->input->get('precoInicial'));
        $precoFinal     = str_replace(array('.',','),array('','.'),$this->input->get('precoFinal'));
        $estoqueInicial = $this->input->get('estoqueInicial');
        $estoqueFinal   = $this->input->get('estoqueFinal');

        $data['productos'] = $this->Reportes_model->productosCustom($precoInicial,$precoFinal,$estoqueInicial,$estoqueFinal);

        $this->load->library('mpdf');
        $html = $this->load->view('reportes/imprimir/imprimirProductos', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_productos' . date('d-m-y'), TRUE);
    }

    public function servicos(){

        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rServico')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de servicios.');
           redirect(base_url());
        }
        //$this->data['view'] = 'reportes/rel_servicos';
       	//$this->load->view('tema/topo',$this->data);
        $this->load->view('plantillas/front_end/header');
        $this->load->view('reportes/rel_servicos');
        $this->load->view('plantillas/front_end/footer');

    }

    public function servicosCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rServico')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de servicios.');
           redirect(base_url());
        }

        $precoInicial = $this->input->get('precoInicial');
        $precoFinal = $this->input->get('precoFinal');
        $data['servicos'] = $this->Reportes_model->servicosCustom($precoInicial,$precoFinal);
        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirServicos', $data);
        $html = $this->load->view('reportes/imprimir/imprimirServicos', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_servicios' . date('d-m-y'), TRUE);
    }

    public function servicosRapid(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rServico')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de servicios.');
           redirect(base_url());
        }

        $data['servicos'] = $this->Reportes_model->servicosRapid();

        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirServicos', $data);
        $html = $this->load->view('reportes/imprimir/imprimirServicos', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_servicios' . date('d-m-y'), TRUE);
    }

    public function os(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rOs')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de Ordenes de Compra.');
           redirect(base_url());
        }
        //$this->data['view'] = 'reportes/rel_os';
       	//$this->load->view('tema/topo',$this->data);
        $this->load->view('plantillas/front_end/header',$datos);
        $this->load->view('reportes/rel_os',$datos);
        $this->load->view('plantillas/front_end/footer');
    }

    public function osRapid(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rOs')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de Ordenes de Compra.');
           redirect(base_url());
        }

        $data['os'] = $this->Reportes_model->osRapid();

        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirOs', $data);
        $html = $this->load->view('reportes/imprimir/imprimirOs', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_ord_compras' . date('d-m-y'), TRUE);
    }

    public function osCustom(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rOs')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de Ordenes de Compra.');
           redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');
        $status = $this->input->get('status');
        $data['os'] = $this->Reportes_model->osCustom($dataInicial,$dataFinal,$cliente,$responsavel,$status);
        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirOs', $data);
        $html = $this->load->view('reportes/imprimir/imprimirOs', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_ord_compras' . date('d-m-y'), TRUE);
    }


    public function financeiro(){

        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rFinanceiro')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de financieros.');
           redirect(base_url());
        }

        //$this->data['view'] = 'reportes/rel_financeiro';
        //$this->load->view('tema/topo',$this->data);
        $this->load->view('plantillas/front_end/header');
        $this->load->view('reportes/rel_financeiro');
        $this->load->view('plantillas/front_end/footer');

    }


    public function financeiroRapid(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rFinanceiro')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de financieros.');
           redirect(base_url());
        }

        $data['lancamentos'] = $this->Reportes_model->financeiroRapid();

        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirFinanceiro', $data);
        $html = $this->load->view('reportes/imprimir/imprimirFinanceiro', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_ord_compras' . date('d-m-y'), TRUE);
    }

    public function financeiroCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rFinanceiro')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de financieros.');
           redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $tipo = $this->input->get('tipo');
        $situacao = $this->input->get('situacao');

        $data['lancamentos'] = $this->Reportes_model->financeiroCustom($dataInicial,$dataFinal,$tipo,$situacao);
        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirFinanceiro', $data);
        $html = $this->load->view('reportes/imprimir/imprimirFinanceiro', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_financiero' . date('d-m-y'), TRUE);
    }



    public function ventas(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rVenda')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de ventas.');
           redirect(base_url());
        }

        //$this->data['view'] = 'reportes/rel_vendas';
        //$this->load->view('tema/topo',$this->data);
        $this->load->view('plantillas/front_end/header');
        $this->load->view('reportes/rel_ventas');
        $this->load->view('plantillas/front_end/footer');
    }

    public function ventasRapid(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rVenda')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de ventas.');
           redirect(base_url());
        }
        $data['ventas'] = $this->Reportes_model->ventasRapid();

        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirOs', $data);
        $html = $this->load->view('reportes/imprimir/imprimirVentas', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_ventas' . date('d-m-y'), TRUE);
    }

    public function ventasCustom(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rVenda')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para imprimir reporte de ventas.');
           redirect(base_url());
        }
        $dataInicial = $this->input->get('dataInicial');
        $dataFinal   = $this->input->get('dataFinal');
        $cliente     = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');

        $data['ventas'] = $this->Reportes_model->ventasCustom($dataInicial,$dataFinal,$cliente,$responsavel);
        $this->load->library('mpdf');
        //$this->load->view('reportes/imprimir/imprimirOs', $data);
        $html = $this->load->view('reportes/imprimir/imprimirVentas', $data, true);
        $this->mpdf->pdf_create($html, 'reporte_ventas' . date('d-m-y'), TRUE);
    }

    public function rpt_factura_pdf(){
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'rVenda')){
           $this->session->set_flashdata('error','Ud. no tiene permiso para genrrar reportes de ventas.');
           redirect(base_url());
        }

        $this->load->view('reportes/rpt_factura_pdf');
    }
}
