<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
			redirect('mapos/login');
		}

		$this->load->model('Clientes_model','',TRUE);
	}

	function index(){
		$this->gerenciar();
	}

	function gerenciar(){
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'vCliente')){
			 $this->session->set_flashdata('error','Ud. No tiene permiso para visualizar clientes.');
				 redirect(base_url());
		}

		$config['base_url'] = base_url().'index.php/clientes/gerenciar/';
		$config['total_rows'] = $this->Clientes_model->count('clientes');
		$config['per_page'] = 10;
		$config['next_link'] = 'Próxima';
		$config['prev_link'] = 'Anterior';
		$config['full_tag_open'] = '<div class="pagination alternate"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
		$config['cur_tag_close'] = '</b></a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['first_link'] = 'Primeira';
		$config['last_link'] = 'Última';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$data['results'] = $this->Clientes_model->get('clientes','idClientes,nomCliente,documento,telefono,celular,email,calle,numero,barrio,ciudad,estado','',$config['per_page'],$this->uri->segment(3));

		$data['menuClientes'] = 'Clientes';
		$this->load->view('plantillas/front_end/header',$data);
		$this->load->view('clientes/clientes',$data);
		$this->load->view('plantillas/front_end/footer');
	}

	function adicionar() {
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'aCliente')){
			 $this->session->set_flashdata('error','Ud. No tiene permiso para agregar clientes.');
				 redirect(base_url());
		}


		$data['custom_error'] = '';
		$this->form_validation->set_rules('nomCliente', 'nomCliente', 'required|trim|xss_clean');
		$this->form_validation->set_rules('documento', 'documento', 'required|trim|xss_clean');
		$this->form_validation->set_rules('celular', 'celular', 'required|trim|xss_clean');
		$this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean');
		$this->form_validation->set_rules('calle', 'calle', 'required|trim|xss_clean');
		$this->form_validation->set_rules('numero', 'numero', 'required|trim|xss_clean');
		$this->form_validation->set_rules('barrio', 'barrio', 'required|trim|xss_clean');
		$this->form_validation->set_rules('estado', 'ciudad', 'required|trim|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$this->data['custom_error'] = validation_errors();
			$this->load->view('plantillas/front_end/header');
			$this->load->view('clientes/adicionarCliente',  $data);
			$this->load->view('plantillas/front_end/footer');

		} else {
			$data = array(
				'nomCliente' => set_value('nomCliente'),
				'documento' => set_value('documento'),
				'telefono' => set_value('telefono'),
				'celular' => $this->input->post('celular'),
				'email' => set_value('email'),
				'calle' => set_value('calle'),
				'numero' => set_value('numero'),
				'barrio' => set_value('barrio'),
				'ciudad' => set_value('ciudad'),
				'estado' => set_value('estado'),
				//'cep' => set_value('cep'),
				'fechacli' => date('Y-m-d')
			);

			if ($this->Clientes_model->add('clientes', $data) == TRUE) {
				$this->session->set_flashdata('success','Cliente adicionado con exito!');
				redirect(base_url() . 'index.php/clientes');
			} else {
				$this->data['custom_error'] = '<div class="form_error"><p>Ocurrio un error.</p></div>';
				redirect(base_url() . 'index.php/clientes/adicionar');
			}
		}
		//$this->data['view'] = 'clientes/adicionarCliente';
		//$this->load->view('tema/topo', $this->data);
		//$this->load->view('plantillas/front_end/header');
		//$this->load->view('clientes/adicionarCliente',  $this->data);
		//$this->load->view('plantillas/front_end/footer');
	}

	function editar() {
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'eCliente')){
			$this->session->set_flashdata('error','Ud. No tiene permiso para visualizar clientes.');
			redirect(base_url());
		}
		//$this->load->library('form_validation');
		$this->data['custom_error'] = '';
		$this->form_validation->set_rules('nomCliente', 'nomCliente', 'required|trim|xss_clean');
		$this->form_validation->set_rules('documento', 'documento', 'required|trim|xss_clean');
		$this->form_validation->set_rules('celular', 'celular', 'required|trim|xss_clean');
		$this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean');
		$this->form_validation->set_rules('calle', 'calle', 'required|trim|xss_clean');
		$this->form_validation->set_rules('numero', 'numero', 'required|trim|xss_clean');
		$this->form_validation->set_rules('barrio', 'barrio', 'required|trim|xss_clean');
		$this->form_validation->set_rules('estado', 'ciudad', 'required|trim|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$this->data['custom_error'] = validation_errors();
			$this->data['result'] = $this->Clientes_model->getById($this->uri->segment(3));
			$this->load->view('plantillas/front_end/header');
			$this->load->view('clientes/editarCliente',  $this->data);
			$this->load->view('plantillas/front_end/footer');
		} else {
			$data = array(
				'nomCliente' => $this->input->post('nomCliente'),
				'documento' => $this->input->post('documento'),
				'telefono' => $this->input->post('telefono'),
				'celular' => $this->input->post('celular'),
				'email' => $this->input->post('email'),
				'calle' => $this->input->post('calle'),
				'numero' => $this->input->post('numero'),
				'barrio' => $this->input->post('barrio'),
				'ciudad' => $this->input->post('ciudad'),
				'estado' => $this->input->post('estado')
				//'cep' => $this->input->post('cep')
			);

			if ($this->Clientes_model->edit('clientes', $data, 'idClientes', $this->input->post('idClientes')) == TRUE) {
				$this->session->set_flashdata('success','Cliente editado con Exito!');
				redirect(base_url() . 'index.php/clientes');
				//redirect(base_url() . 'index.php/clientes/editar/'.$this->input->post('idClientes'));
			} else {
				$this->data['custom_error'] = '<div class="form_error"><p>Ocurrio un error</p></div>';
				$this->data['result'] = $this->Clientes_model->getById($this->uri->segment(3));
				redirect(base_url() . 'index.php/clientes/editar');
			}
		}


		//$this->data['result'] = $this->Clientes_model->getById($this->uri->segment(3));
		//$this->data['view'] = 'clientes/editarCliente';
		//$this->load->view('tema/topo', $this->data);
		//$this->load->view('plantillas/front_end/header');
		//$this->load->view('clientes/editarCliente',  $this->data);
		//$this->load->view('plantillas/front_end/footer');
	}

	public function visualizar(){
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'vCliente')){
			$this->session->set_flashdata('error','Ud no tiene permiso para visualizar clientes.');
			redirect(base_url());
		}

		$this->data['custom_error'] = '';
		$this->data['result'] = $this->Clientes_model->getById($this->uri->segment(3));
		$this->data['results'] = $this->Clientes_model->getOsByCliente($this->uri->segment(3));
		//$this->data['view'] = 'clientes/visualizar';
		//$this->load->view('tema/topo', $this->data);
		$this->load->view('plantillas/front_end/header');
		$this->load->view('clientes/visualizar',  $this->data);
		$this->load->view('plantillas/front_end/footer');

	}

	public function excluir(){

					if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dCliente')){
						 $this->session->set_flashdata('error','Você não tem permissão para excluir clientes.');
						 redirect(base_url());
					}


					$id =  $this->input->post('id');
					if ($id == null){

							$this->session->set_flashdata('error','Erro ao tentar excluir cliente.');
							redirect(base_url().'index.php/clientes/gerenciar/');
					}

					//$id = 2;
					// excluindo OSs vinculadas ao cliente
					$this->db->where('clientes_id', $id);
					$os = $this->db->get('os')->result();

					if($os != null){

							foreach ($os as $o) {
									$this->db->where('os_id', $o->idOs);
									$this->db->delete('servicos_os');

									$this->db->where('os_id', $o->idOs);
									$this->db->delete('produtos_os');


									$this->db->where('idOs', $o->idOs);
									$this->db->delete('os');
							}
					}

					// excluindo Vendas vinculadas ao cliente
					$this->db->where('clientes_id', $id);
					$vendas = $this->db->get('vendas')->result();

					if($vendas != null){

							foreach ($vendas as $v) {
									$this->db->where('vendas_id', $v->idVendas);
									$this->db->delete('itens_de_vendas');


									$this->db->where('idVendas', $v->idVendas);
									$this->db->delete('vendas');
							}
					}

					//excluindo receitas vinculadas ao cliente
					$this->db->where('clientes_id', $id);
					$this->db->delete('lancamentos');



					$this->Clientes_model->delete('clientes','idClientes',$id);

					$this->session->set_flashdata('success','Cliente excluido com sucesso!');
					redirect(base_url().'index.php/clientes/gerenciar/');
	}
}

