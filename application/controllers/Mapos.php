<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mapos extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Mapos_model','',TRUE);
	}

	public function index() {
		if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
			redirect('mapos/login');
		}

		$data['estatisticas_financiero'] = $this->Mapos_model->getEstatisticasFinanciero();
		$data['ordenes'] 	= $this->Mapos_model->getOsAbiertas();
		$data['productos'] 	= $this->Mapos_model->getProdutosMinimo();
		$data['os'] 		= $this->Mapos_model->getOsEstatisticas();
		$data['menuPanel'] 	= 'Panel de Control';

		// muestro el respectivo view con la data recolectada
		$this->load->view('plantillas/front_end/header',$data);
		$this->load->view('mapos/panel',$data);
		$this->load->view('plantillas/front_end/footer');

		/*$sections = array(
			'config'  => TRUE,
			'queries' => TRUE,
			'controller_info' => TRUE,
			'uri_strig' => TRUE,
			'session_data' => TRUE

			);
		$this->output->set_profiler_sections($sections);
		$this->output->enable_profiler(TRUE);*/
	}

	public function miCuenta() {
		if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
			redirect('mapos/login');
		}

		$this->data['usuario'] = $this->Mapos_model->getByEmail($this->session->userdata('usuario'));
		$this->data['view'] = 'mapos/miCuenta';
		$this->load->view('plantillas/front_end/header',  $this->data);
		$this->load->view('mapos/miCuenta',  $this->data);

		/*$sections = array(
			'config'  => TRUE,
			'queries' => TRUE,
			'controller_info' => TRUE,
			'uri_strig' => TRUE,
			'session_data' => TRUE

			);
		$this->output->set_profiler_sections($sections);
		$this->output->enable_profiler(TRUE);*/

	}

	public function alterarSenha() {
		if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
			redirect('mapos/login');
		}

		$oldSenha = $this->input->post('oldSenha');
		$senha = $this->input->post('novaSenha');
		$result = $this->Mapos_model->alterarSenha($senha,$oldSenha,$this->session->userdata('id'));
			if($result){
				$this->session->set_flashdata('success','Senha Alterada com sucesso!');
				redirect(base_url() . 'index.php/mapos/miCuenta');
			} else{
				$this->session->set_flashdata('error','Ocorreu um erro ao tentar alterar a senha!');
				redirect(base_url() . 'index.php/mapos/miCuenta');
			}
	}

	public function buscar() {

		if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
			redirect('mapos/login');
		}

				$termo = $this->input->get('termo');

				$data['results'] = $this->Mapos_model->pesquisar($termo);
				$this->data['productos'] = $data['results']['productos'];
				$this->data['servicios'] = $data['results']['servicios'];
				$this->data['os'] = $data['results']['os'];
				$this->data['clientes'] = $data['results']['clientes'];
				$this->data['view'] = 'mapos/buscar';
				$this->load->view('plantillas/front_end/header',  $this->data);
	}

	public function login(){

		$this->load->view('mapos/login');
	}

	public function salir(){
		$this->session->sess_destroy();
		redirect('mapos/login');
	}

	public function verificarLogin(){
		// para el debuggin
		if( $this->input->post('token') == $this->session->userdata('token') )  {
			$this->form_validation->set_rules('email', 'email', 'required|trim|min_length[2]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('clave', 'clave', 'required|trim|min_length[5]|max_length[50]|xss_clean');
				//lanzamos mensajes de error del formulario login si es que los hay
				if($this->form_validation->run() == FALSE) {
					// redireccionamos hacia el login para que muestre los errores
					$this->login();

				} else {
					// si no hay errores en el fomrulario login hacemos las validaciones en el back-end
					$username = $this->input->post('email');
					$password = sha1($this->input->post('clave'));
					$check_user = $this->Mapos_model->login_user($username,$password);

						// si los datos suministrados estan en la bd configuro las variables de session necesarias
						if($check_user == 'FALSE') {
							// si los datos suministrados son erroneos, redirecciono y le informo al usuario
							$this->session->set_flashdata('error','Los datos introducidos son incorrectos');
							redirect('mapos/login');

						} else {
							$sesion = array(
								'is_logued_in'	=> 'TRUE',
								'permisos_id'	=> $check_user->permisos_id,
								'status'		=> $check_user->status,
								'nivel'			=> $check_user->nivel,
								'apenom'		=> $check_user->nombre,
								'usuario'		=> $check_user->email
							);

							// actualizo el momento en el que entrò al sistema. esto es para las estadisticas
							$idUs = $check_user->idUsuarios;
							$us = $this->Mapos_model->actualiza_ultsession($idUs);
							if ($us == 'FALSE') {
								$this->session->set_flashdata('error','No tiene datos de la ultima session');
								// declaro las variables de la sesion que comienza
								$this->session->set_userdata($sesion);
								redirect('mapos/index','refresh');
							} else {
								$this->session->set_flashdata('success','Bienvenido');
								// declaro las variables de la sesion que comienza
								$this->session->set_userdata($sesion);
								redirect('mapos/index','refresh');
							}

						}
				}
		}
	}

	public function backup(){

			if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
					redirect('mapos/login');
			}

			if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'cBackup')){
				 $this->session->set_flashdata('error','Ud. no tiene permiso para realizar un backup.');
				 redirect(base_url());
			}



			$this->load->dbutil();
			$prefs = array(
							'format'      => 'zip',
							'filename'    => 'backup'.date('d-m-Y').'.sql'
						);

			$backup = $this->dbutil->backup($prefs);

			$this->load->helper('file');
			write_file(base_url().'backup/backup.zip', $backup);

			$this->load->helper('download');
			force_download('backup'.date('d-m-Y H:m:s').'.zip', $backup);
	}

	public function empresa(){

			if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
					redirect('mapos/login');
			}

			if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'cEmitente')){
				 $this->session->set_flashdata('error','Ud. no tiene permiso para configurar Empresa.');
				 redirect(base_url());
			}

			$data['menuConfiguraciones'] = 'Configuraciones';
			$data['datos'] = $this->Mapos_model->getEmpresa();

			$this->load->view('plantillas/front_end/header',$data);
			$this->load->view('mapos/empresa',$data);
			$this->load->view('plantillas/front_end/footer');
	}

	public function registrarEmpresa() {

		if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
			redirect('mapos/login');
		}

		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'cEmitente')){
			$this->session->set_flashdata('error','Ud. no tiene permiso para configurar Empresa.');
			redirect(base_url());
		}

		$nome = $this->input->post('nombrempresa');
		$rif = $this->input->post('rif');
		$ramo = $this->input->post('ramo');
		$iva = $this->input->post('iva');
		$dir = $this->input->post('direccion');
		$numero = $this->input->post('numero');
		$sector = $this->input->post('sector');
		$ciudad = $this->input->post('ciudad');
		$tlfno = $this->input->post('telefono');
		$email = $this->input->post('email');
		$logo = base_url().'assets/uploads/'.$image;


		$retorno = $this->Mapos_model->addEmpresa($nome, $rif, $ramo, $iva, $dir, $numero, $sector, $ciudad, $tlfno, $email, $logo);

		if ($retorno == 'error'){

			$this->session->set_flashdata('error','Ocurrio un error al tratar de insertar datos de la empresa.');
			redirect(base_url().'mapos/empresa');

		} elseif ($retorno == 'correcto') {

			$this->session->set_flashdata('success','Insertada empresa con Exito.');
			redirect(base_url().'mapos/empresa');
		}
	}

	public function editarEmpresa() {

		if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){

			redirect('mapos/login');
		}

		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'cEmitente')){
			 $this->session->set_flashdata('error','Ud. no tiene permiso para configurar Empresa.');
			 redirect(base_url());
		}

		$nome = $this->input->post('nombrempresa');
		$rif = $this->input->post('rif');
		$ramo = $this->input->post('ramo');
		$iva = $this->input->post('iva');
		$dir = $this->input->post('direccion');
		$numero = $this->input->post('numero');
		$sector = $this->input->post('sector');
		$ciudad = $this->input->post('ciudad');
		$tlfno = $this->input->post('telefono');
		$email = $this->input->post('email');
		$id = $this->input->post('id');


		$retorno = $this->Mapos_model->editEmpresa($id, $nome, $rif, $ramo, $iva, $dir, $numero, $sector, $ciudad, $tlfno, $email);

		if ($retorno == 'error'){

			$this->session->set_flashdata('error','Ocurrio un error al tratar de insertar datos de la empresa.');
			redirect('mapos/empresa');

		 } elseif ($retorno == 'correcto') {

			$this->session->set_flashdata('success','Insertada empresa con Exito.');
			redirect('mapos/empresa');
		}
	}

	public function editarLogo(){

			if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
					redirect('index.php/mapos/login');
			}

			if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'cEmitente')){
				 $this->session->set_flashdata('error','Ud. no tiene permiso para configurar Empresa.');
				 redirect(base_url());
			}

			$id = $this->input->post('id');
			//$im = $this->input->post('userfile');

			$this->load->helper('file');
			delete_files(FCPATH .'assets/uploads/');

			$image = $this->do_upload();
			$logo = base_url().'assets/uploads/'.$image;

			$retorno = $this->Mapos_model->editLogo($id, $logo);

			if($retorno == 'error'){

				$this->session->set_flashdata('error','Ocurrio un error al actualizar Logoempresa.');
				redirect('mapos/empresa');

			} elseif ($retorno == 'correcto') {

					$this->session->set_flashdata('success','Logo Actualizado con Exito.');
					redirect('mapos/empresa');
			}
	}

	function do_upload(){

			if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
					redirect('mapos/login');
			}

			if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'cEmitente')){
				 $this->session->set_flashdata('error','Ud. no tiene permiso para configurar Empresa.');
				 redirect(base_url());
			}

			$image_upload_folder = FCPATH . 'assets/uploads';

			if (!file_exists($image_upload_folder)) {
					mkdir($image_upload_folder, DIR_WRITE_MODE, true);
			}

				$config['upload_path']	= $image_upload_folder;
				$config['allowed_types']= 'png|jpg|jpeg|bmp';
				$config['max_size']		= 100;
				$config['max_width']	= 1024;
				$config['max_height']	= 768;

				$this->load->library('upload', $config);

			 if (!$this->upload->do_upload()) {

				$upload_error = array('error' => $this->upload->display_errors());
				//$this->load->view('upload_form', $upload_error);
				$this->session->set_flashdata('success',$upload_error);
				redirect(base_url().'index.php/mapos/empresa');

			 } else {

				$data = $this->upload->data('file_name');
				return $data;

			 }
	}


}
