<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    function __construct() {
        parent::__construct();
        if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
            redirect('mapos/login');
        }
        if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'cUsuario')){
          $this->session->set_flashdata('error','Ud,. no tiene permiso para configurar usuarios.');
          redirect(base_url());
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('Usuarios_model', '', TRUE);
    }

    function index(){
		$this->gerenciar();
	}

	function gerenciar(){

        $this->load->library('pagination');


        $config['base_url'] = base_url().'index.php/usuarios/gerenciar/';
        $config['total_rows'] = $this->Usuarios_model->count('usuarios');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

		$data['results'] = $this->Usuarios_model->get($config['per_page'],$this->uri->segment(3));


        $data['menuUsuarios'] = 'Usuarios';
        $data['menuConfiguraciones'] = 'Configuraciones';

        $this->load->view('plantillas/front_end/header',$data);
        $this->load->view('usuarios/usuarios',$data);
        $this->load->view('plantillas/front_end/footer');



    }

    function adicionar(){

        $this->load->library('form_validation');
		$data['custom_error'] = '';
        $name = $this->input->post('nome');

        if( ! empty($name) || $name != '' || $name != NULL ){
            //$this->load->library('encrypt');
            $senha = sha1($this->input->post('senha'));
            $data = array(
                'nombre' => $this->input->post('nome'),
                'apellido' => $this->input->post('rg'),
                'cedula' => $this->input->post('cpf'),
                'calle' => $this->input->post('rua'),
                'numero' => $this->input->post('numero'),
                'barrio' => $this->input->post('bairro'),
                'ciudad' => $this->input->post('cidade'),
                'estado' => $this->input->post('estado'),
                'email' => $this->input->post('email'),
                'clave' => $senha,
                'telefono' => $this->input->post('telefone'),
                'celular' => $this->input->post('celular'),
                'status' => $this->input->post('situacao'),
                'permisos_id' => $this->input->post('permissoes_id'),
                'fechaingreso' => date('Y-m-d')
            );

            if ($this->Usuarios_model->add('usuarios',$data) == TRUE) {

                $this->session->set_flashdata('success','Usuario registrado con Exito!');
                redirect(base_url().'index.php/usuarios');
            
            } else {

                //$data['custom_error'] = '<div class="form_error"><p>Error registrando Usuario.</p></div>';
                $this->session->set_flashdata('error','Error registrando Usuario.!');
                redirect(base_url().'index.php/usuarios/adicionar');

            }

            $this->load->model('Permisos_model');
            $data['permisos'] = $this->Permisos_model->getActive('permisos','permisos.idPermiso,permisos.nombperm');

            $this->load->view('plantillas/front_end/header',$data);
            $this->load->view('usuarios/adicionarUsuario',$data);
            $this->load->view('plantillas/front_end/footer');

        } else {


            $this->load->model('Permisos_model');
            $data['permisos'] = $this->Permisos_model->getActive('permisos','permisos.idPermiso,permisos.nombperm');

            $this->load->view('plantillas/front_end/header',$data);
            $this->load->view('usuarios/adicionarUsuario',$data);
            $this->load->view('plantillas/front_end/footer');

        }

    }

    function editar(){

        $this->load->library('form_validation');
		$data['custom_error'] = '';
        $this->form_validation->set_rules('nome', 'Nombre', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rg', 'Apellido', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cpf', 'Cedula', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rua', 'Calle', 'trim|required|xss_clean');
        $this->form_validation->set_rules('numero', 'Número', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bairro', 'Sector', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cidade', 'Ciudad', 'trim|required|xss_clean');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('telefone', 'Telefono', 'trim|required|xss_clean');
        $this->form_validation->set_rules('situacao', 'Status', 'trim|required|xss_clean');
        $this->form_validation->set_rules('permissoes_id', 'Permisos', 'trim|required|xss_clean');

        if ($this->form_validation->run() == false) {

             $data['custom_error'] = (validation_errors() ? '<div class="form_error">'.validation_errors().'</div>' : false);

        } else {

            if ($this->input->post('idUsuarios') == 1 && $this->input->post('situacao') == 0) {

                $this->session->set_flashdata('error','El Superusuario no puede ser desactivado!');
                redirect(base_url().'index.php/usuarios/editar/'.$this->input->post('idUsuarios'));
            }

            $senha = $this->input->post('senha');
            if($senha != null){
                //$this->load->library('encryption');
                //$senha = $this->encryption->sha1($senha);
                $senha = sha1($this->input->post('senha'));

                $data = array(
                        'nombre' => $this->input->post('nome'),
                        'apellido' => $this->input->post('rg'),
                        'cedula' => $this->input->post('cpf'),
                        'calle' => $this->input->post('rua'),
                        'numero' => $this->input->post('numero'),
                        'barrio' => $this->input->post('bairro'),
                        'ciudad' => $this->input->post('cidade'),
                        'estado' => $this->input->post('estado'),
                        'email' => $this->input->post('email'),
                        'clave' => $senha,
                        'telefono' => $this->input->post('telefone'),
                        'celular' => $this->input->post('celular'),
                        'status' => $this->input->post('situacao'),
                        'permisos_id' => $this->input->post('permissoes_id')
                );
            } else{

                $data = array(
                        'nombre' => $this->input->post('nome'),
                        'apellido' => $this->input->post('rg'),
                        'cedula' => $this->input->post('cpf'),
                        'calle' => $this->input->post('rua'),
                        'numero' => $this->input->post('numero'),
                        'barrio' => $this->input->post('bairro'),
                        'ciudad' => $this->input->post('cidade'),
                        'estado' => $this->input->post('estado'),
                        'email' => $this->input->post('email'),
                        'telefono' => $this->input->post('telefone'),
                        'celular' => $this->input->post('celular'),
                        'status' => $this->input->post('situacao'),
                        'permisos_id' => $this->input->post('permissoes_id')
                );

            }


			if ($this->Usuarios_model->edit('usuarios',$data,'idUsuarios',$this->input->post('idUsuarios')) == TRUE) {

                $this->session->set_flashdata('success','Usuario editado con exito!');
				redirect(base_url().'index.php/usuarios');
			} else {

				$data['custom_error'] = '<div class="form_error"><p>Error editando usuario</p></div>';
                redirect(base_url().'index.php/usuarios/editar/'.$this->input->post('idUsuarios'));

			}
		}

        $data['result'] = $this->Usuarios_model->getById($this->uri->segment(3));
        $this->load->model('Permisos_model');
        $data['permisos'] = $this->Permisos_model->getActive('permisos','permisos.idPermiso,permisos.nombperm');
        $this->load->view('plantillas/front_end/header',$data);
        $this->load->view('usuarios/editarUsuario',$data);
        $this->load->view('plantillas/front_end/footer');


    }

    function excluir(){
            $ID =  $this->uri->segment(3);
            $this->Usuarios_model->delete('usuarios','idUsuarios',$ID);
            redirect(base_url().'index.php/usuarios/gerenciar/');
    }
}

