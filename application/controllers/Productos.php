<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productos extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if((!$this->session->userdata('is_logued_in')) || ($this->session->userdata('is_logued_in') != TRUE)){
			redirect('mapos/login');
		}

		$this->load->model('Productos_model', '', TRUE);

	}

	function index(){
		$this->gerenciar();
	}

	function gerenciar(){
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'vProduto')){
			 $this->session->set_flashdata('error','Ud. No tiene permiso para visualizar Productos.');
				 redirect(base_url());
		}

		$config['base_url'] = base_url().'index.php/productos/gerenciar/';
		$config['total_rows'] = $this->Productos_model->count('productos');
		$config['per_page'] = 10;
		$config['next_link'] = 'Próxima';
		$config['prev_link'] = 'Anterior';
		$config['full_tag_open'] = '<div class="pagination alternate"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
		$config['cur_tag_close'] = '</b></a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['first_link'] = 'Primeira';
		$config['last_link'] = 'Última';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$data['results'] = $this->Productos_model->get('productos','idProductos,codprod,descriProd,unidad,precioCompra,precioVenta,existencia,existMinimo','',$config['per_page'],$this->uri->segment(3));

		$data['menuProductos'] = 'Productos';
		$this->load->view('plantillas/front_end/header',  $data);
		$this->load->view('productos/productos',  $data);
		$this->load->view('plantillas/front_end/footer');

	}

	// para consultar si la cedula ya existe
	public function consultcod(){
		$codprod = $this->input->post('codprod');
		$consulta = $this->Productos_model->consultar_codprod($codprod);
			//echo $consulta;
			if ($consulta == 0 ) {
				// si el valor es menor a 1 o igual a cero valid valdrá "true" e indica que ya NO existe esa cedula
				$valid = json_encode(array('valid' => TRUE));
				echo $valid;
			} else if ($consulta >= 1 ){
				// si el valor es mayor a 1 valid valdrá "false" e indica que ya existe esa cedula
				$valid = json_encode(array('valid' => FALSE));
				echo $valid;
			}
	}

	function adicionar() {
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'aProduto')){
			 $this->session->set_flashdata('error','Ud. No tiene permiso para adicionar Productos.');
				 redirect(base_url());
		}

			//$this->load->library('form_validation');
			$this->data['custom_error'] = '';
			$this->form_validation->set_rules('codprod', 'Codigo Producto', 'required|trim|xss_clean');
			$this->form_validation->set_rules('descriProd', 'Descrip. Producto', 'required|trim|xss_clean');
			$this->form_validation->set_rules('unidad', 'Unidad', 'required|trim|xss_clean');
			$this->form_validation->set_rules('categoria', 'Categoria', 'required|trim|xss_clean');
			$this->form_validation->set_rules('estilo', 'Estilo', 'required|trim|xss_clean');
			$this->form_validation->set_rules('tipo', 'Tipo', 'required|trim|xss_clean');
			$this->form_validation->set_rules('talla', 'Talla', 'required|trim|xss_clean');
			$this->form_validation->set_rules('color', 'Color', 'required|trim|xss_clean');
			$this->form_validation->set_rules('caractprod', 'Caracteristicas', 'required|trim|xss_clean');
			$this->form_validation->set_rules('precioCompra', 'Precio Compra', 'required|trim|xss_clean');
			$this->form_validation->set_rules('precioVenta', 'Precio Venta', 'required|trim|xss_clean');
			$this->form_validation->set_rules('precioVentaMay', 'Precio Venta Mayor', 'required|trim|xss_clean');
			$this->form_validation->set_rules('existencia', 'Existencia', 'required|trim|xss_clean');
			$this->form_validation->set_rules('existMinimo', 'Existencia Minima', 'required|trim|xss_clean');

			if ($this->form_validation->run() == FALSE) {

				$this->data['custom_error'] = validation_errors();
				$this->load->view('plantillas/front_end/header');
				$this->load->view('productos/adicionarProducto',  $this->data);
				$this->load->view('plantillas/front_end/footer');

			 } else {

				$valq = array('.',',','Bs ');
				$valp = array('','.','');
				$precioCompra = str_replace($valq,$valp,$this->input->post('precioCompra'));

				$precioVenta  = str_replace($valq,$valp,$this->input->post('precioVenta'));

				$precioVentaM = str_replace($valq,$valp,$this->input->post('precioVentaMay'));


				$data = array(
					'codprod' 		=> set_value('codprod'),
					'descriProd' 	=> set_value('descriProd'),
					'unidad' 			=> set_value('unidad'),
					'categoria'		=> set_value('categoria'),
					'estilo' 			=> set_value('estilo'),
					'tipo' 				=> set_value('tipo'),
					'talla' 			=> set_value('talla'),
					'color' 			=> set_value('color'),
					'caractprod'	=> set_value('caractprod'),
					'precioCompra'=> $precioCompra,
					'precioVenta' => $precioVenta,
					'precioVentaMay' => $precioVentaM,
					'existencia' 	=> set_value('existencia'),
					'existMinimo' => set_value('existMinimo')
				);

					$insercion = $this->Productos_model->add('productos', $data);

				if ($insercion == 'error') {

					$this->session->set_flashdata('mensaje', '
						<div id="saveAlert" class="alert alert-danger" >
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							<p>No se puede registrar 2 veces un Codigo de Producto..!</p>
						</div>
						<script> $("#saveAlert").delay(14000).fadeOut(7000);</script>
					');
					redirect(base_url() . 'index.php/productos/adicionar');

				 } elseif ($insercion == 'correcto') {

					$this->session->set_flashdata('mensaje', '
						<div id="saveAlert" class="alert alert-success" >
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							<p>Producto adicionado con Exito.!</p>
						</div>
						<script> $("#saveAlert").delay(14000).fadeOut(7000);</script>
					');
					redirect(base_url() . 'index.php/productos');

				 } elseif ($insercion == 'error2') {

					$this->session->set_flashdata('mensaje', '
						<div id="saveAlert" class="alert alert-success" >
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							<p>Ocurrio un Error..!</p>
						</div>
						<script> $("#saveAlert").delay(14000).fadeOut(7000);</script>
					');
					redirect(base_url() . 'index.php/productos/adicionar');

				}
			}
			//$this->data['view'] = 'productos/adicionarProducto';
			//$this->load->view('tema/topo', $this->data);
			//$this->load->view('plantillas/front_end/header');
			//$this->load->view('clientes/adicionarCliente',  $this->data);
			//$this->load->view('plantillas/front_end/footer');

	}

	function editar() {
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'eProduto')){
			$this->session->set_flashdata('error','Ud. no tiene permiso para editar productos.');
			redirect(base_url());
		}

		//$this->load->library('form_validation');
		$this->data['custom_error'] = '';
		$this->form_validation->set_rules('codprod', 'Codigo Producto', 'required|trim|xss_clean');
		$this->form_validation->set_rules('descriProd', 'Descrip. Producto', 'required|trim|xss_clean');
		$this->form_validation->set_rules('unidad', 'Unidad', 'required|trim|xss_clean');
		$this->form_validation->set_rules('categoria', 'Categoria', 'required|trim|xss_clean');
		$this->form_validation->set_rules('estilo', 'Estilo', 'required|trim|xss_clean');
		$this->form_validation->set_rules('tipo', 'Tipo', 'required|trim|xss_clean');
		$this->form_validation->set_rules('talla', 'Talla', 'required|trim|xss_clean');
		$this->form_validation->set_rules('color', 'Color', 'required|trim|xss_clean');
		$this->form_validation->set_rules('caractprod', 'Caracteristicas', 'required|trim|xss_clean');
		$this->form_validation->set_rules('precioCompra', 'Precio Compra', 'required|trim|xss_clean');
		$this->form_validation->set_rules('precioVenta', 'Precio Venta', 'required|trim|xss_clean');
		$this->form_validation->set_rules('precioVentaMay', 'Precio Venta Mayor', 'required|trim|xss_clean');
		$this->form_validation->set_rules('existencia', 'Existencia', 'required|trim|xss_clean');
		$this->form_validation->set_rules('existMinimo', 'Existencia Minima', 'required|trim|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$this->data['custom_error'] = validation_errors();
			$this->data['result'] = $this->Productos_model->getById($this->uri->segment(3));
			$this->load->view('plantillas/front_end/header');
			$this->load->view('productos/editarProducto',  $this->data);
			$this->load->view('plantillas/front_end/footer');
		} else {
			$valq = array('.',',','Bs ');
			$valp = array('','.','');
			$precioCompra = str_replace($valq,$valp,$this->input->post('precioCompra'));

			$precioVenta  = str_replace($valq,$valp,$this->input->post('precioVenta'));

			$precioVentaM = str_replace($valq,$valp,$this->input->post('precioVentaMay'));


			$data = array(
				'codprod' 		=> set_value('codprod'),
				'descriProd' 	=> set_value('descriProd'),
				'unidad' 			=> set_value('unidad'),
				'categoria'		=> set_value('categoria'),
				'estilo' 			=> set_value('estilo'),
				'tipo' 				=> set_value('tipo'),
				'talla' 			=> set_value('talla'),
				'color' 			=> set_value('color'),
				'caractprod'	=> set_value('caractprod'),
				'precioCompra'=> $precioCompra,
				'precioVenta' => $precioVenta,
				'precioVentaMay' => $precioVentaM,
				'existencia' 	=> set_value('existencia'),
				'existMinimo' => set_value('existMinimo')
			);

			$edicion = $this->Productos_model->edit('productos', $data, 'idProductos', $this->input->post('idProductos')) == TRUE;

			if ($edicion == 'correcto') {

				$this->session->set_flashdata('mensaje', '
					<div id="saveAlert" class="alert alert-success" >
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<p>Producto adicionado con Exito.!</p>
					</div>
					<script> $("#saveAlert").delay(14000).fadeOut(7000);</script>
				');
				redirect(base_url() . 'index.php/productos');

			 } elseif ($edicion == 'error') {

				$this->session->set_flashdata('mensaje', '
					<div id="saveAlert" class="alert alert-success" >
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<p>Ocurrio un Error..!</p>
					</div>
					<script> $("#saveAlert").delay(14000).fadeOut(7000);</script>
				');
				redirect(base_url() . 'index.php/productos/editar/'.$this->input->post('idProductos'));
			}
		}

		//$this->data['result'] = $this->Productos_model->getById($this->uri->segment(3));
		//$this->data['view'] = 'produtos/editarProduto';
		//$this->load->view('tema/topo', $this->data);
		//$this->load->view('plantillas/front_end/header');
		//$this->load->view('productos/editarProducto',  $this->data);
		//$this->load->view('plantillas/front_end/footer');

	}

	function visualizar() {
		if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'vProduto')){
			 $this->session->set_flashdata('error','Ud. no tiene permiso para visualizar productos.');
			 redirect(base_url());
		}

			$this->data['result'] = $this->Productos_model->getById($this->uri->segment(3));

			if($this->data['result'] == null){
				$this->session->set_flashdata('error','Producto no encontrado.');
				redirect(base_url() . 'index.php/produtos/editar/'.$this->input->post('idProductos'));
			}

			//$this->data['view'] = 'produtos/visualizarProduto';
			//$this->load->view('tema/topo', $this->data);
			$this->load->view('plantillas/front_end/header');
			$this->load->view('productos/visualizarProducto',  $this->data);
			$this->load->view('plantillas/front_end/footer');

	}

	function excluir(){

			if(!$this->permission->checkPermission($this->session->userdata('permisos_id'),'dProduto')){
				 $this->session->set_flashdata('error','ud no tiene permiso para borrar productos.');
				 redirect(base_url());
			}


			$id =  $this->input->post('id');
			if ($id == null){
				$this->session->set_flashdata('error','Error al intentar excluir producto.');
				redirect(base_url().'index.php/productos/gerenciar/');
			}

			$this->db->where('idProductos_os', $id);
			$this->db->delete('productos_os');


			$this->db->where('idProductos', $id);
			$this->db->delete('productos');

			$this->Productos_model->delete('productos','idProductos',$id);


			$this->session->set_flashdata('success','Producto excluido con exito!');
			redirect(base_url().'index.php/productos/gerenciar/');

	}
}

