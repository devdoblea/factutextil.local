<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	// Incluimos el archivo fpdf
	require_once APPPATH."/third_party/fpdf/fpdf.php";
 
//Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
class Pdf extends FPDF {
	
	public function __construct() {
		parent::__construct();
	}

	//funciones para cambiar los encabezados de los reportes
	function SetDatosHeader($datosH){
		$this->datosHeader = $datosH;
	}

	//funciones para cambiar los encabezados de los reportes
	function GetDatosHeader(){
		return $this->datosHeader;
	}
	// El encabezado del PDF
	function Header() {
		if ($this->header == 1) {
			$this->Image('assets/img/logo.png',10,8,22);
			$this->SetFont('Arial','B',13);
			$datosHeader = $this->GetDatosHeader();
			$titu = $datosHeader['Titulo'];
			$stit = $datosHeader['Subtitulo'];
			$this->Cell(140,10,"$titu",0,0,'C');
			$this->Ln('5');
			$this->SetFont('Arial','B',8);
			$this->Cell(140,10,"$stit",0,0,'C');
			$this->Ln(20);
		} elseif ($this->header == 2) {
			$this->Image('assets/img/logo-matricula-escolar.png',2,3,120);
			$this->SetFont('Arial','B',9);
			$datosHeader = $this->GetDatosHeader();
			$titu = $datosHeader['Titulo'];
			$sti1 = $datosHeader['Subtit1'];
			$sti2 = $datosHeader['Subtit2'];
			$anioes = $datosHeader['anioesc'];
			$mesane = $datosHeader['msaneva'];
			$codcol = $datosHeader['codcole'];
			$nomcol = $datosHeader['nomcole'];
			$dircol = $datosHeader['dircole'];
			$tlfcol = $datosHeader['tlfcole'];
			$muncol = $datosHeader['muncole'];
			$entcol = $datosHeader['entcole'];
			$zedcol = $datosHeader['zedcole'];
			$dtrcol = $datosHeader['dtrcole'];
			$cdtcol = $datosHeader['cdtcole'];

			$this->Cell(150);
			$this->SetFont('','BU');
			$this->Cell(8,8,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'C');
			$this->Ln(4);
			$this->Cell(150);
			$this->SetFont('','B');
			$this->Cell(8,8,iconv("UTF-8","ISO-8859-1","$sti1"),0,0,'C');
			$this->Ln(4);
			$this->Cell(150);
			$this->SetFont('Arial','B',7);
			$this->Cell(8,8,iconv("UTF-8","ISO-8859-1","$sti2"),0,0,'C');
			$this->Ln(4);
			$this->Cell(90);
			$this->SetFont('Arial','B',8);
			$this->Cell(8,8,"I. TIPO DE MATRICULA:",0,0,'C');
			$this->SetFont('Arial','',8);
			$this->Cell(55,8,"CONVENCIONAL:",0,0,'R');
			$this->Cell(1,8,"______",0,0,'L');
			$this->Cell(40,8,"NO CONVENCIONAL:",0,0,'R');
			$this->Cell(1,8,"______",0,0,'L');
			$this->Ln(4);
			$this->SetFont('Arial','',8);
			$this->Cell(82);
			$this->Cell(55,8,iconv("UTF-8","ISO-8859-1","Año Escolar:"),0,0,'R');
			$this->SetFont('','BU');
			$this->Cell(1,8,$anioes,0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(52,8,iconv("UTF-8","ISO-8859-1","Mes y Año de la Evaluación:"),0,0,'R');
			$this->SetFont('','BU');
			$this->Cell(1,8,"Julio $mesane",0,0,'L');
			$this->Ln(4);

			$this->SetFont('Arial','B',10);
			$this->Cell(8,8,"II. Datos del Plantel:",0,0,'L');
			$this->Ln(7);

			$this->SetFont('Arial','B',8);
			$this->Cell(12,3,iconv("UTF-8","ISO-8859-1","Cód. Plantel:"),0,0,'L');
			$this->Cell(10);
			$this->SetFont('Arial','',8);
			$this->Cell(30,3,$codcol,"B",0,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(16,3,iconv("UTF-8","ISO-8859-1","Nombre:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(90,3,$nomcol,"B",0,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(17,3,iconv("UTF-8","ISO-8859-1","Dtto Esc.:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,3,"13","B",0,'C');
			$this->Ln(4);

			$this->SetFont('Arial','B',8);
			$this->Cell(18,3,iconv("UTF-8","ISO-8859-1","Dirección:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(140,3,$dircol,"B",0,'C');
			$this->SetFont('Arial','B',9);
			$this->Cell(17,3,iconv("UTF-8","ISO-8859-1","Teléfono:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,3,$tlfcol,"B",0,'C');
			$this->Ln(4);

			$this->SetFont('Arial','B',8);
			$this->Cell(18,3,iconv("UTF-8","ISO-8859-1","Municipio:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(62,3,$muncol,"B",0,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(3);
			$this->Cell(22,3,iconv("UTF-8","ISO-8859-1","Ent. Federal:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(35,3,$entcol,"B",0,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(3);
			$this->Cell(28,3,iconv("UTF-8","ISO-8859-1","Zona Educativa:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(34,3,$zedcol,"B",0,'C');
			$this->Ln(4);

			$this->SetFont('Arial','B',8);
			$this->Cell(19,3,iconv("UTF-8","ISO-8859-1","Director(a):"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(146,3,$dtrcol,"B",0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(8,3,iconv("UTF-8","ISO-8859-1","C.I.:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(32,3,number_format($cdtcol,0,',','.'),"B",0,'C');
			$this->Ln(3);

			$this->SetFont('Arial','B',10);
			$this->Cell(8,8,iconv("UTF-8","ISO-8859-1","III. Identificación del Curso:"),0,0,'L');
			$this->Ln(7);
		} elseif ($this->header == 3) {
			$this->Image('assets/img/fichaacum.png',25,8,22);
			$this->SetFont('Arial','B',11);
			$datosHeader = $this->GetDatosHeader();
			$titu = $datosHeader['Titulo'];
			$sti1 = $datosHeader['Subtit1'];
			$sti2 = $datosHeader['Subtit2'];
			$sti3 = $datosHeader['Subtit3'];
			$sti4 = $datosHeader['Subtit4'];
			$sti5 = $datosHeader['Subtit5'];
			$sti6 = $datosHeader['Subtit6'];
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'C');
			$this->Ln(6);
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$sti1"),0,0,'C');
			$this->Ln(6);
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$sti2"),0,0,'C');
			$this->Ln(6);
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$sti3 . EDO. $sti4"),0,0,'C');
			$this->Ln(6);
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$sti5"),0,0,'C');
			$this->Ln(6);
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","M.E.C.D. $sti6 - DEA"),0,0,'C');
			$this->Ln(20);
		} elseif ($this->header == 4) {
			$this->Image('assets/img/logo-mineduc.png',10,5,40);
			$this->Image('assets/img/logo-lhomez-300x384.png',170,5,25);
			$this->SetFont('Arial','B',11);
			$datosHeader = $this->GetDatosHeader();
			$titu = $datosHeader['Titulo'];
			$sti1 = $datosHeader['Subtit1'];
			$sti2 = $datosHeader['Subtit2'];
			$sti3 = $datosHeader['Subtit3'];
			$sti4 = $datosHeader['Subtit4'];
			$sti5 = $datosHeader['Subtit5'];
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'C');
			$this->Ln(6);
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$sti1"),0,0,'C');
			$this->Ln(6);
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$sti2"),0,0,'C');
			$this->Ln(6);
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$sti3"),0,0,'C');
			$this->Ln(6);
			$this->Cell(184,10,iconv("UTF-8","ISO-8859-1","$sti4. EDO. $sti5"),0,0,'C');
			$this->Ln(12);
		} elseif ($this->header == 5) {
			$this->Image('assets/img/logo.png',10,8,22);
			$this->SetFont('Arial','B',10);
			$this->Cell(193,7,'FECHA: '.date('d-M-Y'),'0',1,'R','0');
			$this->SetFont('Arial','B',13);
			$datosHeader = $this->GetDatosHeader();
			$titu = $datosHeader['Titulo'];
			$sti1 = $datosHeader['Subtit1'];
			$sti2 = $datosHeader['Subtit2'];
			$this->Cell(200,10,"$titu",0,0,'C');
			$this->Ln(5);
			$this->SetFont('Arial','B',10);
			$this->Cell(200,10,"$sti1",0,0,'C');
			$this->Ln(5);
			$this->SetFont('Arial','B',9);
			$this->Cell(200,10,"$sti2",0,0,'C');
			$this->Ln(10);
		} elseif ($this->header == 6) {
			$this->Image('assets/img/logo.png',10,8,22);
			$this->SetFont('Arial','B',10);
			$this->Cell(193,7,'FECHA: '.date('d-M-Y'),'0',1,'R','0');
			$this->SetFont('Arial','B',13);
			$datosHeader = $this->GetDatosHeader();
			$titu = $datosHeader['Titulo'];
			$sti1 = $datosHeader['Subtit1'];
			$sti2 = $datosHeader['Subtit2'];
			$sti3 = $datosHeader['Subtit3'];
			$this->Cell(200,10,"$titu",0,0,'C');
			$this->Ln(5);
			$this->SetFont('Arial','B',10);
			$this->Cell(200,10,"$sti1",0,0,'C');
			$this->Ln(5);
			$this->SetFont('Arial','B',9);
			$this->Cell(200,10,"$sti2",0,0,'C');
			$this->Ln(5);
			$this->SetFont('Arial','B',9);
			$this->Cell(200,10,"$sti3",0,0,'C');
			$this->Ln(10);
		}
	}

	// El pie del pdf
	function Footer(){
		if ($this->footer == 1) {
			$this->SetY(-15);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		} elseif ($this->footer == 2) {
			$this->SetY(-39);
			$this->SetFont('Arial','B',7);
			$this->Cell(100,5,iconv("UTF-8","ISO-8859-1","VI. Fecha de Remisión:"),'TLR',0,'C','0');
			$this->Cell(4);
			$this->Cell(100,5,iconv("UTF-8","ISO-8859-1","VII. Fecha de Recepción:"),'TLR',0,'C','0');
			$this->Ln(5);
			$this->Cell(45,5,iconv("UTF-8","ISO-8859-1","Director(a)"),'TLR',0,'C','0');
			$this->Cell(55,5,iconv("UTF-8","ISO-8859-1",""),'TLR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,5,iconv("UTF-8","ISO-8859-1","Funcionario(a) Receptor(a)"),'TLR',0,'C','0');
			$this->Cell(55,5,iconv("UTF-8","ISO-8859-1",""),'TLR',0,'C','0');
			$this->Ln(5);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Apellidos y Nombres:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Apellidos y Nombres:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Número de C.I.:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1","SELLO DEL PLANTEL"),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Número de C.I.:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1","SELLO DE LA ZONA EDUCATIVA"),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Firma:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Firma"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
		} elseif ($this->footer == 3) {
			$this->SetY(-55);
			$this->SetFont('Arial','I',8);
			$this->Cell(184,8,iconv("UTF-8","ISO-8859-1","NOTA: Todos los Representantes están en la obligación de leer y manejar el contenido de los Acuerdos de"),0,0,'C');
			$this->Ln(4);
			$this->Cell(184,8,iconv("UTF-8","ISO-8859-1","Convivencia Escolar y Comunitarios de la Institución, el cual será enviado por correo electrónico."),0,0,'C');
		}
	}

}