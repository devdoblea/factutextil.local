<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Compras_model extends CI_Model {


	public function __construct() {
		parent::__construct();
	}

	//obtenemos el total de filas para hacer la paginación del buscador
	function busqueda($buscador) {
		$this->db->like('cedsolicit', $buscador);
		$this->db->or_like('nombsolicit', $buscador);
		$this->db->or_like('codserv', $buscador);
		$this->db->order_by('idsgen', 'DESC');
		$consulta = $this->db->get('tbservgen');
		return $consulta->num_rows();
	}

	/* obtenemos todos los posts a paginar con la función total_posts_paginados pasando lo que buscamos,
	la cantidad por página y el segmento como parámetros de la misma */
	function total_posts_paginados($buscador, $por_pagina, $segmento) {
		$this->db->like('cedsolicit', $buscador);
		$this->db->or_like('nombsolicit', $buscador);
		$this->db->or_like('codserv', $buscador);
		$this->db->order_by('idsgen', 'DESC');
		$consulta = $this->db->get('tbservgen', $por_pagina, $segmento);
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			return $datos;
		}
	}

	function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){

		$this->db->select($fields.', proveedores.nomProv, proveedores.idProv');
		$this->db->from($table);
		$this->db->limit($perpage,$start);
		$this->db->join('proveedores', 'proveedores.idProv = '.$table.'.proveedor_id');
		$this->db->order_by('idCompras','desc');
		if($where){
			$this->db->where($where);
		}

		$query = $this->db->get();

		$result =  !$one  ? $query->result() : $query->row();
		return $result;
	}

	function getById($id){

		$this->db->select('compras.*, proveedores.*, usuarios.*');
		$this->db->from('compras');
		$this->db->join('proveedores','proveedores.idProv = compras.proveedor_id');
		$this->db->join('usuarios','usuarios.idUsuarios = compras.usuarios_id');
		$this->db->where('compras.idCompras',$id);
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	function count($table){

		return $this->db->count_all($table);
	}

	public function nro_temporal() {
		$this->db->select('nrotemp');
		$consulta = $this->db->get('empresa');

		if ($consulta->num_rows() > 0) {
				foreach ($consulta->result() as $fila) {
						$nrotemp = $fila->nrotemp;
		}

			// retorno el dato que se saco de la bd antes de aumentarle uno mas
			return $nrotemp;
		}
	}

	function datos() {
		$consulta = $this->db->get('empresa');
		if ($consulta->num_rows() > 0) {
				foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
		}
				return $datos;
		}
	}

		// para traer los datos de la empresa empresa_model
	public function getEmpresa() {

		return $this->db->get('empresa')->result();
	}

	// para buscar los datos del cliente
	function consultar_prov($nome) {

		// para acomodar el resultado de los telefonos
		function camelize($word) {
			$vowels = array("/", "-", "(", ")");
			return str_replace($vowels, '', $word);
		}
		$this->db->like('nomProv',$nome);
		//$this->db->where('stat =!','INACTIVO');
		$this->db->order_by('nomProv');
		$query = $this->db->get('proveedores');
		if($query->num_rows() > 0){
				foreach ($query->result_array() as $row){
						$new_row['value']   = trim($row['nomProv']);
						$new_row['id']      = $row['idProv'];
						$new_row['tlfn']    = camelize($row['telefono']);
						$new_row['celu']    = camelize($row['celular']);
						$row_set[] = $new_row; //build an array
				}
			echo json_encode($row_set); //format the array into json data
		}
	}

	// para buscar los datos de los servicios tabulados
	function consultar_prod($descrip) {
		$this->db->like('descriProd',$descrip);
		$this->db->order_by('descriProd');
		$query = $this->db->get('productos');
		if($query->num_rows() > 0){
				foreach ($query->result_array() as $row){
						$new_row['value']           = trim($row['descriProd'].' - '.$row['unidad'].' - Quedan: '.$row['existencia']);
						$new_row['codprod']     = trim($row['codprod']);
						$new_row['preciov']     = trim($row['precioVenta']);
						$new_row['precioc']     = trim($row['precioCompra']);
						$new_row['tipo']        = trim($row['tipo']);
						$new_row['unidad']      = trim($row['unidad']);
						$row_set[] = $new_row; //build an array
				}
			echo json_encode($row_set); //format the array into json data
		}
	}

	// para sacar los articulos registrados en la factura del servicio
	public function listar_articulos($nrotemp) {
		$this->db->select('compras_det.idFactcomp, compras_det.nrotemp, compras_det.codprod, compras_det.cant, compras_det.unidad, compras_det.precioComp, compras_det.precioVent, compras_det.fecha, productos.descriProd');
		$this->db->join('productos','productos.codprod = compras_det.codprod');
		$this->db->where('nrotemp',$nrotemp);
		$this->db->order_by('idFactcomp');
		$consulta = $this->db->get('compras_det');
				return $consulta;
	}

	/* para sacar TODOS los datos de la orden de servicio para imprimir
			pero esta vez es cuando la solicitud venga directo de la creacion de orden de servicio*/
	public function listar_encabezado_nrotemp($nrotemp) {
			$this->db->select('compras.idCompras, compras.fechaCompra, compras.valorTotal, compras.nrotemp_id,compras.proveedor_id, proveedores.nomProv, proveedores.documento, proveedores.direccion, proveedores.telefono, proveedores.celular, proveedores.email, proveedores.calle, proveedores.numero, proveedores.barrio, proveedores.ciudad, proveedores.estado');
			$this->db->join('proveedores','compras.proveedor_id = proveedores.idProv');
			$this->db->where('compras.nrotemp_id',$nrotemp);
			$consulta = $this->db->get('compras');
					return $consulta->result();
	}

	/* para sacar TODOS los datos de la orden de servicio para imprimir
			pero esta vez es cuando la solicitud venga directo de la creacion de orden de servicio*/
	public function listar_art_nrotemp($nrotemp) {
			$this->db->select('compras_det.fecha, compras_det.cant, compras_det.precio, compras_det.codprod, compras_det.unidad, productos.descriProd');
			$this->db->join('compras','compras.nrotemp_id = compras_det.nrotemp');
			$this->db->join('productos','compras_det.codprod = productos.codprod');
			$this->db->where('compras_det.nrotemp',$nrotemp);
			$this->db->order_by('compras_det.nrotemp');
			$consulta = $this->db->get('compras_det');
					return $consulta->result();
	}

	public function listar_encabezado_id($idcomp) {
			$this->db->select('compras.idCompras, compras.fechaCompra, compras.valorTotal, compras.nrofactcompra, compras.nrotemp_id,compras.proveedor_id,  proveedores.idProv, proveedores.nomProv, proveedores.documento, proveedores.direccion, proveedores.telefono, proveedores.celular, proveedores.email, proveedores.calle, proveedores.numero, proveedores.barrio, proveedores.ciudad, proveedores.estado');
			$this->db->join('proveedores','compras.proveedor_id = proveedores.idProv');
			$this->db->where('compras.idCompras',$idcomp);
			$consulta = $this->db->get('compras');
					return $consulta->result();
	}

	// para grabar los articulos en el servicio
	public function agregar_art($idtmp,$codpr,$cantp,$prcic,$prciv,$unidd,$fec,$ope) {

			// descuento las existencias
			$this->db->where('codprod',$codpr);
			$desc = $this->db->get('productos');
			foreach ($desc->result() as $r) {
					$ex_anteri = $r->existencia;
			}
					// calculo la existencia nueva
					$ex_actual = (int) $ex_anteri + (int) $cantp;
					// actualizo la base de datos
					$data2 = array('existencia' => $ex_actual, 'precioCompra' => $prcic, 'precioVenta' => $prciv);
					$this->db->where('codprod',$codpr);
					$this->db->update('productos',$data2);

					$data = array(
							'nrotemp'       => $idtmp,
							'codprod'       => $codpr,
							'cant'          => $cantp,
							'unidad'        => $unidd,
							'precioVent'    => $prciv,
							'precioComp'    => $prcic,
							'fecha'         => $fec
					);
					// verificamos si se insertaron los registros
					if (!$this->db->insert('compras_det', $data)){

							return 'error';

					 } else {

							return 'correcto';
					}
	}

	// borra articulo uno por uno de la orden de servicio
	public function borra_art($idart){
			// borro los registros alla en la prenom general
			$this->db->where('idFactcomp',$idart);
			if ($this->db->delete('compras_det')) {

					return 'error';

			} else {

					return 'correcto';
			}
	}

	// para insertar una orden de servicio
	public function insertaRegistro($fcom,$nrot,$nfac,$nprv,$prvi,$rifp,$celu){

			// funcion para invertir las fechas
			function cambfecha($fecha) {
				$fech = explode('-',$fecha);
				$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];
				return $fnac;
			}

			// consulto si el proveedor esta en la proveedores y si no esta lo grabo
			$this->db->select('idProv');
			$this->db->where('idProv',$prvi);
			$qcli = $this->db->get('proveedores');
			if ($qcli->num_rows() == 0) {
					$data4 = array(
							'nomProv'		=> $nprv,
							'celular'   => $celu,
							'fechaprov'  => date('Y-m-d'),
							'documento' => $rifp,
							'email'     => 'falta@falta.com',
							'direccion' => 'falta',
							'telefono'	=> '0000 000 00 00',
							'calle'     => 'falta',
							'numero'    => 'falta',
							'barrio'    => 'falta',
							'ciudad'    => 'falta',
							'estado'    => 'falta'
							);
					$this->db->insert('proveedores',$data4);
			}

			// consulto el ultimo nro temporal creado para generar un nro nuevo
			$this->db->select('nrotemp');
			$qnrosrv = $this->db->get('empresa');
			foreach ($qnrosrv->result() as $r) {
					$ultimotmp = $r->nrotemp;
			}
				$ultnrotmp = $ultimotmp + 1;
				/*$siguiente_id = sprintf("%03d",$ultimonro);
				$nroserv = 'SERV-'.$siguiente_id;*/
				// saco el monto total sin iva de compras_det
				$consulta = $this->db->query("SELECT SUM(precioComp*cant) as total FROM compras_det WHERE nrotemp='$nrot'");
				foreach ($consulta->result() as $r) {
						$vtotal = $r->total;
				}
				// guardo en la tabla de servicio
				$data = array(
						'fechaCompra'   => cambfecha($fcom),
						'valorTotal'    => $vtotal,
						'descuento'     => '0',
						'nrofactcompra' => $nfac,
						'proveedor_id'  => $prvi,
						'usuarios_id'   => '',
						'nrotemp_id'    => $nrot
				);

				// grabo los datos a la tabla correspondiente y comienzo una cascada de eventos donde verificamos si se insertaron los registros
				if (!$this->db->insert('compras', $data)) {

						return 'error';

				 } else {

						// actualizo los articulos con el nro de orden creado
						$data2 = array('nrofactcompra' => $nfac);
						$this->db->where('nrotemp', $nrot);
						// de una vez verificamos si se actualizaron los registros, sino lanza el error
						if (!$this->db->update('compras_det',$data2)) {

								return 'error';

						 } else {

								// luego actualizo el nro de orden de servicio
								$data3 = array('nrotemp' =>$ultnrotmp);
								$this->db->where('id','1');
								// de una vez verificamos si realmente se actualizaron los registros sino lanza error
								if (!$this->db->update('empresa',$data3)) {

										return 'error';

								 } else {

										return 'correcto';
								}
						}
				}
	}

	public function editaRegistro($fcom,$nrot,$nfac,$nprv,$prvi,$rifp,$celu) {
		// funcion para invertir las fechas
		function cambfecha($fecha) {
			$fech = explode('-',$fecha);
			$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];
			return $fnac;
		}

		// consulto si el proveedor esta en la proveedores y si no esta lo grabo
		$this->db->select('idProv');
		$this->db->where('idProv',$prvi);
		$qcli = $this->db->get('proveedores');
		if ($qcli->num_rows() == 0) {
				$data4 = array(
						'nomProv'		=> $nprv,
						'celular'   => $celu,
						'fechaprov'  => date('Y-m-d'),
						'documento' => $rifp,
						'email'     => 'falta@falta.com',
						'direccion' => 'falta',
						'telefono'	=> '0000 000 00 00',
						'calle'     => 'falta',
						'numero'    => 'falta',
						'barrio'    => 'falta',
						'ciudad'    => 'falta',
						'estado'    => 'falta'
						);
				$this->db->insert('proveedores',$data4);
		}

		// consulto el ultimo nro temporal creado para generar un nro nuevo
		$this->db->select('nrotemp');
		$qnrosrv = $this->db->get('empresa');
		foreach ($qnrosrv->result() as $r) {
				$ultimotmp = $r->nrotemp;
		}
			$ultnrotmp = $ultimotmp + 1;
			/*$siguiente_id = sprintf("%03d",$ultimonro);
			$nroserv = 'SERV-'.$siguiente_id;*/
			// saco el monto total sin iva de compras_det
			$consulta = $this->db->query("SELECT SUM(precioComp*cant) as total FROM compras_det WHERE nrotemp='$nrot'");
			foreach ($consulta->result() as $r) {
					$vtotal = $r->total;
			}
			// guardo en la tabla de servicio
			$data = array(
					'fechaCompra'   => cambfecha($fcom),
					'valorTotal'    => $vtotal,
					'descuento'     => '0',
					'nrofactcompra' => $nfac,
					'proveedor_id'  => $prvi,
					'usuarios_id'   => '',
					'nrotemp_id'    => $nrot
			);

			// grabo los datos a la tabla correspondiente y comienzo una cascada de eventos donde verificamos si se insertaron los registros
			$this->db->where('nrofactcompra',$nfac);
			if (!$this->db->update('compras', $data)) {

					return 'error';

			 } else {

					// actualizo los articulos con el nro de orden creado
					$data2 = array('nrofactcompra' => $nfac);
					$this->db->where('nrotemp', $nrot);
					// de una vez verificamos si se actualizaron los registros, sino lanza el error
					if (!$this->db->update('compras_det',$data2)) {

							return 'error';

					 } else {

							// luego actualizo el nro de orden de servicio
							$data3 = array('nrotemp' =>$ultnrotmp);
							$this->db->where('id','1');
							// de una vez verificamos si realmente se actualizaron los registros sino lanza error
							if (!$this->db->update('empresa',$data3)) {

									return 'error';

							 } else {

									return 'correcto';
							}
					}
			}
	}

	//para cancelar la orden de servicio, si se han insertado articulos esta funcion las elimina
	public function cancelarOrden($nrot) {
			// revierto la existencia que se desconto en la factura activa
			// primero busco cuales articulos se incluyeron en la factura
			$this->db->where('nrotemp',$nrot);
			$articulos = $this->db->get('compras_det');
			foreach ($articulos->result() as $r) {
					$codprod = $r->codprod;
					$cantida = $r->cant;
					// consulto cada articulo generado dentro del foreach para ubicar la existencia actual
					$this->db->where('codprod',$codprod);
					$desc = $this->db->get('productos');
					foreach ($desc->result() as $r) {
							$ex_anteri = $r->existencia;
					}

					// calculo la cantidad de existencia a revertir
					$ex_actual = $ex_anteri - $cantida;

					// actualizo el producto con la nueva existencia
					$data2 = array('existencia' => $ex_actual);
					$this->db->where('codprod',$codprod);
					$this->db->update('productos',$data2);
			}

			// borro de la tabla compras_det todo lo que tenga el numero temporal que envie
			$this->db->where('nrotemp',$nrot);
			if (!$this->db->delete('compras_det')) {
					return 'error';
			 } else {
					return 'correcto';
			}
	}

}

/* End of file compras_model.php */
/* Location: ./application/models/compras_model.php */