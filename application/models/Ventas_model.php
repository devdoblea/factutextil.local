<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ventas_model extends CI_Model {

	public function __construct() {
				parent::__construct();
		}


		function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){

			$this->db->select($fields.', clientes.nomCliente, clientes.idClientes');
			$this->db->from($table);
			$this->db->limit($perpage,$start);
			$this->db->join('clientes', 'clientes.idClientes = '.$table.'.clientes_id');
			$this->db->order_by('idVentas','desc');
			if($where){
					$this->db->where($where);
			}

			$query = $this->db->get();

			$result =  !$one  ? $query->result() : $query->row();
			return $result;
		}

		function getById($id){

			$this->db->select('ventas.*, clientes.*, usuarios.*');
			$this->db->from('ventas');
			$this->db->join('clientes','clientes.idClientes = ventas.clientes_id');
			$this->db->join('usuarios','usuarios.idUsuarios = ventas.usuarios_id');
			$this->db->where('ventas.idVentas',$id);
			$this->db->limit(1);
			return $this->db->get()->row();
		}

		public function getProductos($id = null){
				$this->db->select('items_de_ventas.*, productos.*');
				$this->db->from('items_de_ventas');
				$this->db->join('productos','productos.idProductos = items_de_ventas.productos_id');
				$this->db->where('ventas_id',$id);
				return $this->db->get()->result();
		}

		function add($table,$data,$returnId = false){
				$this->db->insert($table, $data);
				if ($this->db->affected_rows() == '1'){
					if($returnId == true){
						return $this->db->insert_id($table);
					}
					return TRUE;
				}
				return FALSE;
		}

		function edit($table,$data,$fieldID,$ID){
				$this->db->where($fieldID,$ID);
				$this->db->update($table, $data);

				if ($this->db->affected_rows() >= 0){
					return TRUE;
				}
					return FALSE;
		}

		function delete($table,$fieldID,$ID){
				$this->db->where($fieldID,$ID);
				$this->db->delete($table);
				if ($this->db->affected_rows() == '1'){
					return TRUE;
				}
				return FALSE;
		}

		function count($table){

		 return $this->db->count_all($table);
		}

		public function autoCompleteProducto($q){

				$this->db->select('*');
				$this->db->limit(5);
				$this->db->like('descriProd', $q);
				$query = $this->db->get('produtos');
				if($query->num_rows > 0){
						foreach ($query->result_array() as $row){
								$row_set[] = array('label'=>$row['descriProd'].' | Precio: Bs '.$row['precioVenta'].' | Existencia: '.$row['existencia'],'existencia'=>$row['existencia'],'id'=>$row['idProdutos'],'precio'=>$row['precioVenta']);
						}
						echo json_encode($row_set);
				}
		}

		public function autoCompleteCliente($q){

				//$this->db->select('*');
				$this->db->like('nomCliente', $q);
				$this->db->limit(5);
				$query = $this->db->get('clientes');
				if($query->num_rows > 0){
						foreach ($query->result_array() as $row){
								$row_set[] = array('label'=>$row['nomCliente'].' | Telefono: '.$row['telefono'],'id'=>$row['idClientes']);
						}
						echo json_encode($row_set);
				}
		}

		public function autoCompleteUsuario($q){

				$this->db->select('*');
				$this->db->limit(5);
				$this->db->like('nombre', $q);
				$this->db->where('status',1);
				$query = $this->db->get('usuarios');
				if($query->num_rows > 0){
						foreach ($query->result_array() as $row){
								$row_set[] = array('label'=>$row['nombre'].' | Telefono: '.$row['telefono'],'id'=>$row['idUsuarios']);
						}
						echo json_encode($row_set);
				}
		}



}

/* End of file vendas_model.php */
/* Location: ./application/models/vendas_model.php */