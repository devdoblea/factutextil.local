<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mapos_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function login_user($username,$password) {
		$this->db->where('email',$username);
		$this->db->where('clave',$password);
		$query = $this->db->get('usuarios');
		if($query->num_rows() == 1) {
			return $query->row();
		}else{
			return 'FALSE';
		}
	}

	public function actualiza_ultsession($idUs) {

		// configuro la hora en formato Unix
		$ses = time();
		//Consulto si tiene dato de untima session en la tabla de usuarios
		$this->db->select('ultsession');
		$this->db->where('idUsuarios',$idUs);
		$consultses  = $this->db->get('usuarios');
		foreach ($consultses->result() as $k) {
			$us = $k->ultsession;
		}
		// si no hay datos de la ultima session se los actualizo
		if (!$us) {
			$datos = array('ultsession' => $ses);
			
			$this->db->where('idUsuarios',$idUs);
			$this->db->update('usuarios',$datos);

			return 'TRUE';
		} else {
			$datos = array('ultsession' => $ses);
			
			$this->db->where('idUsuarios',$idUs);
			$this->db->update('usuarios',$datos);
			return 'FALSE';
		}


	}

		function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){

				$this->db->select($fields);
				$this->db->from($table);
				$this->db->limit($perpage,$start);
				if($where){
						$this->db->where($where);
				}

				$query = $this->db->get();

				$result =  !$one  ? $query->result() : $query->row();
				return $result;
		}

		function getById($id){
				$this->db->from('usuarios');
				$this->db->select('usuarios.*, permisos.nombperm as permissao');
				$this->db->join('permisos', 'permisos.idPermiso = usuarios.permisos_id', 'left');
				$this->db->where('idUsuarios',$id);
				$this->db->limit(1);
				return $this->db->get()->row();
		}

		function getByEmail($email){
				$this->db->from('usuarios');
				$this->db->select('usuarios.*, permisos.nombperm as permissao');
				$this->db->join('permisos', 'permisos.idPermiso = usuarios.permisos_id', 'left');
				$this->db->where('email',$email);
				$this->db->limit(1);
				return $this->db->get()->row();
		}

		public function alterarSenha($senha,$oldSenha,$id){

				$this->db->where('idUsuarios', $id);
				$this->db->limit(1);
				$usuario = $this->db->get('usuarios')->row();

				if($usuario->senha != $oldSenha){
						return false;
				}
				else{
						$this->db->set('senha',$senha);
						$this->db->where('idUsuarios',$id);
						return $this->db->update('usuarios');
				}
		}

		function pesquisar($termo){

				 $data = array();
				 // buscando clientes
				 $this->db->like('nomeCliente',$termo);
				 $this->db->limit(5);
				 $data['clientes'] = $this->db->get('clientes')->result();

				 // buscando os
				 $this->db->like('idOs',$termo);
				 $this->db->limit(5);
				 $data['os'] = $this->db->get('os')->result();

				 // buscando produtos
				 $this->db->like('descricao',$termo);
				 $this->db->limit(5);
				 $data['produtos'] = $this->db->get('produtos')->result();

				 //buscando serviços
				 $this->db->like('nome',$termo);
				 $this->db->limit(5);
				 $data['servicos'] = $this->db->get('servicos')->result();

				 return $data;
		}


		function add($table,$data){

				$this->db->insert($table, $data);
				if ($this->db->affected_rows() == '1') {
					return TRUE;
				}
				return FALSE;
		}

		function edit($table,$data,$fieldID,$ID){

				$this->db->where($fieldID,$ID);
				$this->db->update($table, $data);

				if ($this->db->affected_rows() >= 0) {
					return TRUE;
				}
				return FALSE;
		}

		function delete($table,$fieldID,$ID){

				$this->db->where($fieldID,$ID);
				$this->db->delete($table);
				if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}

		return FALSE;
		}

	function count($table){

		return $this->db->count_all($table);
	}

		function getOsAbiertas(){

				$this->db->select('os.*, clientes.nomCliente');
				$this->db->from('os');
				$this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
				$this->db->where('os.status','Abierto');
				$this->db->limit(10);
				return $this->db->get()->result();
		}

		function getProdutosMinimo(){

			$query = $this->db->query(' SELECT * FROM productos WHERE existencia <= existMinimo LIMIT 10  ');
				return $query->result();
		}

		function getOsEstatisticas(){

				$sql = "SELECT status, COUNT(status) as total FROM os GROUP BY status ORDER BY status";
				return $this->db->query($sql)->result();
		}

		public function getEstatisticasFinanciero() {

				$sql = "SELECT SUM(CASE WHEN bajado = 1 AND tipo = 'receita' THEN valor END) as total_receita,
											 SUM(CASE WHEN bajado = 1 AND tipo = 'despesa' THEN valor END) as total_despesa,
											 SUM(CASE WHEN bajado = 0 AND tipo = 'receita' THEN valor END) as total_receita_pendente,
											 SUM(CASE WHEN bajado = 0 AND tipo = 'despesa' THEN valor END) as total_despesa_pendente FROM facturacion";
				return $this->db->query($sql)->row();
		}

		public function getEmpresa() {

			return $this->db->get('empresa')->result();
		}

		public function addEmpresa($nome, $rif, $ramo, $iva, $dir, $numero, $sector, $ciudad, $tlfno, $email, $logo) {

			$data = array(
					'nombrempresa'=> $nome,
					'rif'		  => $rif,
					'ramo'		  => $ramo,
					'iva'		  => $iva,
					'direccion'	  => $dir,
					'numero'	  => $numero,
					'sector'	  => $sector,
					'ciudad'	  => $ciudad,
					'telefonoemp' => $tlfno,
					'email'		  => $email,
					'url_logo'	  => $logo,
					'nrotemp'	  => '0',
					'nrofact'	  => '0'
			);

			if (!$this->db->insert('empresa', $data)) {
				return 'error';
			} else {
				return 'correcto';
			}
		}

		public function editEmpresa($id, $nome, $rif, $ramo, $iva, $dir, $numero, $sector, $ciudad, $tlfno, $email){

		 $data = array(
				'nombrempresa'=> $nome,
				'rif'		  => $rif,
				'ramo'		  => $ramo,
				'iva'		  => $iva,
				'direccion'	  => $dir,
				'numero'	  => $numero,
				'sector'	  => $sector,
				'ciudad'	  => $ciudad,
				'telefonoemp' => $tlfno,
				'email'		  => $email,
				'nrotemp'	  => '0'
			);

			$this->db->where('id', $id);
			if (!$this->db->update('empresa', $data)) {

				return 'error';

			} else {

				return 'correcto';
			}
		}

		public function editLogo($id, $logo){

			$data = array('url_logo' => $logo );
			$this->db->where('id', $id);
			if (!$this->db->update('empresa', $data)) {

				return 'error';

			} else {

				return 'correcto';
			}
		}
}