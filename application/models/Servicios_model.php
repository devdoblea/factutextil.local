<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicios_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	//obtenemos el total de filas para hacer la paginación del buscador
	function busqueda($buscador) {
		$this->db->like('cedsolicit', $buscador);
		$this->db->or_like('nombsolicit', $buscador);
		$this->db->or_like('codserv', $buscador);
		$this->db->order_by('idsgen', 'DESC');
		$consulta = $this->db->get('tbservgen');
		return $consulta->num_rows();
	}

	//obtenemos todos los posts a paginar con la función
	//total_posts_paginados pasando lo que buscamos, la cantidad por página y el segmento
	//como parámetros de la misma
	function total_posts_paginados($buscador, $por_pagina, $segmento) {
		$this->db->like('cedsolicit', $buscador);
		$this->db->or_like('nombsolicit', $buscador);
		$this->db->or_like('codserv', $buscador);
		$this->db->order_by('idsgen', 'DESC');
		$consulta = $this->db->get('tbservgen', $por_pagina, $segmento);
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			return $datos;
		}
	}

	function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){

		$this->db->select($fields.', clientes.nomCliente, clientes.idClientes');
		$this->db->from($table);
		$this->db->limit($perpage,$start);
		$this->db->join('clientes', 'clientes.idClientes = '.$table.'.clientes_id');
		$this->db->order_by('idVentas','desc');
		if($where){
				$this->db->where($where);
		}

		$query = $this->db->get();

		$result =  !$one  ? $query->result() : $query->row();
		return $result;
	}

	function getById($id){

		$this->db->select('ventas.*, clientes.*, usuarios.*');
		$this->db->from('ventas');
		$this->db->join('clientes','clientes.idClientes = vendas.clientes_id');
		$this->db->join('usuarios','usuarios.idUsuarios = vendas.usuarios_id');
		$this->db->where('ventas.idVentas',$id);
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	function count($table){

		 return $this->db->count_all($table);
	}

	public function nro_temporal() {
		$this->db->select('nrotemp');
		$consulta = $this->db->get('empresa');

		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$nrotemp = $fila->nrotemp;
		}

		/* sumo un numero mas al que esta en la base de datos para que no se dupliquen
		los contenidos de las ordenes de servicio cuando hay mas de un usuario cargando*/
		/*$incremento = array ( 'nrotemp' => $nrotemp + 1) ;
		$this->db->where('id', 1);
		$this->db->update('empresa',$incremento);*/

			// retorno el dato que se saco de la bd antes de aumentarle uno mas
			return $nrotemp;
		}
	}

	function datos() {
		$consulta = $this->db->get('empresa');
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
			$datos[] = $fila;
		}
			return $datos;
		}
	}

	// para traer los datos de la empresa empresa_model
  public function getEmpresa() {

    return $this->db->get('empresa')->result();
  }

	// para buscar los datos del cliente
	function consultar_cliente($nome) {

		// para acomodar el resultado de los telefonos
		function camelize($word) {
			$vowels = array("/", "-", "(", ")");
		   return str_replace($vowels, '', $word);
		}
		$this->db->like('nomCliente',$nome);
		//$this->db->where('stat =!','INACTIVO');
		$this->db->order_by('nomCliente');
		$query = $this->db->get('clientes');
		if($query->num_rows() > 0){
			foreach ($query->result_array() as $row){
				$new_row['value']	= trim($row['nomCliente']);
				$new_row['id']		= $row['idClientes'];
				$new_row['tlfn']	= camelize($row['telefono']);
				$new_row['celu']	= camelize($row['celular']);
				$row_set[] = $new_row; //build an array
			}
		  echo json_encode($row_set); //format the array into json data
		}
	}

	// para buscar los datos de los servicios tabulados
	function consultar_prod($descrip) {
		$this->db->like('descriProd',$descrip);
		$this->db->order_by('descriProd');
		$query = $this->db->get('productos');
		if($query->num_rows() > 0){
			foreach ($query->result_array() as $row){
				$new_row['value']			= trim($row['descriProd'].' - '.$row['unidad'].' - Quedan: '.$row['existencia']);
				$new_row['codprod']		= trim($row['codprod']);
				$new_row['preciov']		= trim($row['precioVenta']);
				$new_row['tipo']			= trim($row['tipo']);
				$new_row['unidad']		= trim($row['unidad']);
				$row_set[] = $new_row; //build an array
			}
		  echo json_encode($row_set); //format the array into json data
		}
	}

	// para sacar los articulos registrados en la factura del servicio
	public function listar_articulos($nrotemp) {
		$this->db->select('ventas_det.idFacturas, ventas_det.nrotemp, ventas_det.codprod, ventas_det.cant, ventas_det.unidad, ventas_det.precio, ventas_det.fecha, productos.descriProd');
		$this->db->join('productos','productos.codprod = ventas_det.codprod');
		$this->db->where('nrotemp',$nrotemp);
		$this->db->order_by('idFacturas');
		$consulta = $this->db->get('ventas_det');
			return $consulta;
	}

	/* para sacar TODOS los datos de la orden de servicio para imprimir
	    pero esta vez es cuando la solicitud venga directo de la creacion de orden de servicio*/
	public function listar_encabezado_nrotemp($nrotemp) {
		$this->db->select('ventas.idVentas, ventas.fechaVenta, ventas.valorTotal, ventas.nrotemp_id,ventas.clientes_id, clientes.nomCliente, clientes.documento, clientes.direccion, clientes.telefono, clientes.celular, clientes.email, clientes.calle, clientes.numero, clientes.barrio, clientes.ciudad, clientes.estado');
		$this->db->join('clientes','ventas.clientes_id = clientes.idClientes');
		$this->db->where('ventas.nrotemp_id',$nrotemp);
		$consulta = $this->db->get('ventas');
			return $consulta->result();
	}

	/* para sacar TODOS los datos de la orden de servicio para imprimir
	    pero esta vez es cuando la solicitud venga directo de la creacion de orden de servicio*/
	public function listar_art_nrotemp($nrotemp) {
		$this->db->select('ventas_det.fecha, ventas_det.cant, ventas_det.precio, ventas_det.codprod, ventas_det.unidad, productos.descriProd');
		$this->db->join('ventas','ventas.nrotemp_id = ventas_det.nrotemp');
		$this->db->join('productos','ventas_det.codprod = productos.codprod');
		$this->db->where('ventas_det.nrotemp',$nrotemp);
		$this->db->order_by('ventas_det.nrotemp');
		$consulta = $this->db->get('ventas_det');
			return $consulta->result();
	}


	// para grabar los articulos en el servicio
	public function agregar_art($idtmp,$codpr,$cantp,$prcio,$unidd,$fec,$ope) {

		// descuento las existencias
		$this->db->where('codprod',$codpr);
		$desc = $this->db->get('productos');
		foreach ($desc->result() as $r) {
			$ex_anteri = $r->existencia;
			$ex_minima = $r->existMinimo;
		}
			// calculo la existencia nueva
			$ex_actual = (int) $ex_anteri - (int) $cantp;
			// actualizo la base de datos
			$data2 = array('existencia' => $ex_actual);
			$this->db->where('codprod',$codpr);
			$this->db->update('productos',$data2);

		 	$data = array(
				'nrotemp'		=> $idtmp,
				'codprod'		=> $codpr,
				'cant'		 	=> $cantp,
				'unidad'		=> $unidd,
				'precio' 		=> $prcio,
				'fecha'			=> $fec
			);
			// verificamos si se insertaron los registros
			if (!$this->db->insert('ventas_det', $data)){

				return 'error';

			 } else {

				return 'correcto';
			}
	}

	// borra articulo uno por uno de la orden de servicio
	public function borra_art($idart){
		// borro los registros alla en la prenom general
		$this->db->where('idFacturas',$idart);
		if ($this->db->delete('ventas_det')) {

			return 'error';

		} else {

			return 'correcto';
		}
	}

	// para insertar una orden de servicio
	public function insertaRegistro($nrot,$ncli,$ccli,$tlfn,$celu,$fven){

		// funcion para invertir las fechas
		function cambfecha($fecha) {
			$fech = explode('-',$fecha);
			$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];
			return $fnac;
		}

		// consulto si el cliente esta en la tbclientes y si no esta lo grabo
		$this->db->select('idClientes');
		$this->db->where('idClientes',$ccli);
		$qcli = $this->db->get('clientes');
		if ($qcli->num_rows() == 0) {
			$data4 = array(
				'nomCliente'=> $ncli,
				'telefono'	=> $tlfn,
				'celular'		=> $celu,
				'fechacli'	=> date('Y-m-d'),
				'documento' => '0',
				'email'			=> 'falta@falta.com',
				'direccion' => 'falta',
				'calle'			=> 'falta',
				'numero'		=> 'falta',
				'barrio'		=> 'falta',
				'ciudad'		=> 'falta',
				'estado'		=> 'falta'
				);
			$this->db->insert('clientes',$data4);
		}

		// consulto el ultimo nro de servicio creado para generar un nro nuevo
		$this->db->select('nrofact,nrotemp');
		$qnrosrv = $this->db->get('empresa');
		foreach ($qnrosrv->result() as $r) {
			$ultimonro = $r->nrofact;
			$ultimotmp = $r->nrotemp;
		}
			$ultnrotmp = $ultimotmp + 1;
			$ultnrosrv = $ultimonro + 1;
			/*$siguiente_id = sprintf("%03d",$ultimonro);
			$nroserv = 'SERV-'.$siguiente_id;*/
			// saco el monto total sin iva de ventas_det
			$consulta = $this->db->query("SELECT SUM(precio*cant) as total FROM ventas_det WHERE nrotemp='$nrot'");
			foreach ($consulta->result() as $r) {
				$vtotal = $r->total;
			}
			// guardo en la tabla de servicio
			$data = array(
				'fechaVenta'	=> cambfecha($fven),
				'valortotal'	=> $vtotal,
				'descuento'		=> '0',
				'facturado'		=> 1,
				'clientes_id'	=> $ccli,
				'usuarios_id'	=> $this->session->userdata('permisos_id'),
				'nrotemp_id'	=> $nrot
			);

			// grabo los datos a la tabla correspondiente y comienzo una cascada de eventos donde verificamos si se insertaron los registros
			if (!$this->db->insert('ventas', $data)) {

				return 'error';

			 } else {

				// actualizo los articulos con el nro de orden creado
				$data2 = array('nrofactura' => $ultnrosrv);
				$this->db->where('nrotemp', $nrot);
				// de una vez verificamos si se actualizaron los registros, sino lanza el error
				if (!$this->db->update('ventas_det',$data2)) {

					return 'error';

				 } else {

					// luego actualizo el nro de orden de servicio
					$data3 = array('nrofact' => $ultnrosrv,'nrotemp' =>$ultnrotmp);
					$this->db->where('id','1');
					// de una vez verificamos si realmente se actualizaron los registros sino lanza error
					if (!$this->db->update('empresa',$data3)) {

						return 'error';

					 } else {

						return 'correcto';
					}
				}
			}
	}

	//para cancelar la orden de servicio, si se han insertado articulos esta funcion las elimina
	public function cancelarOrden($nrot) {
		// revierto la existencia que se desconto en la factura activa
		// primero busco cuales articulos se incluyeron en la factura
		$this->db->where('nrotemp',$nrot);
		$articulos = $this->db->get('ventas_det');
		foreach ($articulos->result() as $r) {
			$codprod = $r->codprod;
			$cantida = $r->cant;
			// consulto cada articulo generado dentro del foreach para ubicar la existencia actual
			$this->db->where('codprod',$codprod);
			$desc = $this->db->get('productos');
			foreach ($desc->result() as $r) {
				$ex_anteri = $r->existencia;
			}

			// calculo la cantidad de existencia a revertir
			$ex_actual = $ex_anteri + $cantida;

			// actualizo el producto con la nueva existencia
			$data2 = array('existencia' => $ex_actual);
			$this->db->where('codprod',$codprod);
			$this->db->update('productos',$data2);
		}

		// borro de la tabla ventas_det todo lo que tenga el numero temporal que envie
		$this->db->where('nrotemp',$nrot);
		if (!$this->db->delete('ventas_det')) {
			return 'error';
		 } else {
			return 'correcto';
		}
	}
}