<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Productos_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}


	function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
		$this->db->select($fields);
		$this->db->from($table);
		$this->db->order_by('idProductos','desc');
		$this->db->limit($perpage,$start);
		if($where){
			$this->db->where($where);
		}
		$query = $this->db->get();

		$result =  !$one  ? $query->result() : $query->row();
		return $result;
	}

	function getById($id){
		$this->db->where('idProductos',$id);
		$this->db->limit(1);
			return $this->db->get('productos')->row();
	}

	// consultar para la busqueda de cedula en caliente
	function consultar_codprod($codprod) {
		$this->db->where('codprod',$codprod);
		$query = $this->db->get('productos');
			$q = $query->num_rows();
			return $q;
	}

	function add($table,$data){

			// consulto si el codigo esta repetido
			$cod = $data['codprod'];
			$this->db->where('codprod',$cod);
			$consulta = $this->db->get('productos');
			if ($consulta->num_rows() >= 1) {

				return 'error';

			} else {

				// si no esta repetido el codigo, entonces inserto
				if (!$this->db->insert($table, $data)) {

					return 'error2';

				 } else {

					return 'correcto';

				}
			}
	}

	function edit($table,$data,$fieldID,$ID){

		$this->db->where($fieldID,$ID);
		if (!$this->db->update($table, $data)) {
				return 'error';
		} else {
			return 'correcto';
		}

	}

	function delete($table,$fieldID,$ID){
		$this->db->where($fieldID,$ID);
		$this->db->delete($table);
			if ($this->db->affected_rows() == '1') {
				return TRUE;
			}
		return FALSE;
	}

	function count($table){
		return $this->db->count_all($table);
	}
}